program VRCOSC10Key;

uses
  System.Sysutils,
  FMX.Forms,
  FMX.Types,
  Unit1 in 'Unit1.pas' {VTKForm},
  Unit2 in 'Unit2.pas' {ExecutionCell: TFrame},
  Unit3 in 'Unit3.pas' {VRC10KEYRECEIVER},
  AppUtils in 'AppUtils.pas',
  Unit4 in 'Unit4.pas' {FormOSCLogger},
  PK.Net.WebSocket in 'PK.Net.WebSocket.pas',
  AppInclude in 'AppInclude.pas',
  Lime.Util.SendKeys in 'Lime.Util.SendKeys.pas';

{$R *.res}

begin

  if not FileExists(ChangeFileExt(ParamStr(0),'.gpu.full')) then
  begin
      FMX.Types.GlobalUseDXSoftware:=True;
      FMX.Types.GlobalUseDXInDX9Mode := False;
  end;

  Application.Initialize;
  Application.CreateForm(TVTKForm, VTKForm);
  Application.Run;
end.
