unit Unit3;

interface

uses
  {$IFDEF MSWINDOWS} Winapi.Windows, Winapi.Messages, FMX.Platform.Win, Winapi.MMSystem, {$ENDIF}
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Diagnostics, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, PK.Net.WebSocket, AppInclude, Apputils,
  IdContext, IdCustomHTTPServer, IdServerIOHandler, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdHTTPServer;


const
  URL_NERV = 'wss://unnerv.jp/api/v1/streaming?stream=public';
{$IFDEF MSWINDOWS}
  WM_VRC10KEY = WM_APP + $100;
  WM_VRC10KEYLOCAL =  WM_VRC10KEY + $1;
  WM_VRC10KEYLOCALLEGACY =  WM_VRC10KEY + $2;
  RI_BUF = 16;
{$ENDIF}

type
  TVRC10KEYRECEIVER = class(TForm)
    Label1: TLabel;
    IdHTTPServer1: TIdHTTPServer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure IdHTTPServer1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure IdHTTPServer1ParseAuthentication(AContext: TIdContext;
      const AAuthType, AAuthData: string; var VUsername, VPassword: string;
      var VHandled: Boolean);
  private
    { private �錾 }
    FLocalMessageLoop : TThread;
    FWebSocket : TWebSocket;
    FEnableNerv : Boolean;
    FEnableHTTPD : Boolean;
    FEnableKeyCode : Boolean;
    FEnableSendMsg : Boolean;
    FEnableMIDI : Boolean;
    FEnableGamePad : Boolean;
    FFormReady : Boolean;
    {$IFDEF MSWINDOWS}
    FMIDIDevices : TStringList;
    FMIDISelected : Integer;
    FMIDIHandle : HMIDIIN;
    FGamePadThread : TThread;
    //FGamePadInfo : Array [0..512] of TJoyInfoEx;
    RawTimeLimit, RawTimeLimit_KB, RawTimeLimit_HID  : TStopWatch;
    procedure WmInput(var Msg: TMessage); message WM_INPUT;
    procedure WmAppCommand(var Msg: TMessage); message WM_APPCOMMAND;
    {$ENDIF}
    procedure NervReceiver(Sender: TObject; const iText: String);
    procedure SetEnableNerv(bEnable:Boolean);
    procedure SetEnableHTTPD(bEnable:Boolean);
    procedure SetEnableMIDI(bEnable:Boolean);
    procedure SetMIDISelected(idx:Integer);
    procedure SetEnableGamePad(bEnable:Boolean);
    procedure PostMessageLocal(MsgCode:Cardinal; wParam:WPARAM; lParam:LPARAM);
    procedure NervConnected(Sender: TObject);
    procedure GamePadThreadTerminate(Sender: TObject);
    procedure LocalMessageLoopTerminate(Sender: TObject);
  public
    { public �錾 }
    {$IFDEF MSWINDOWS}
    WindowsHandle : HWND;
    property MIDIDevices : TStringList read FMIDIDevices;
    property MIDISelected : Integer read FMIDISelected write SetMIDISelected;
    property EnableSendMessage:Boolean read FEnableSendMsg write FEnableSendMsg;
    {$ENDIF}
    property EnableKeyCode:Boolean read FEnableKeyCode write FEnableKeyCode;
    property EnableNERV:Boolean read FEnableNerv write SetEnableNerv;
    property EnableHTTPD:Boolean read FEnableHTTPD write SetEnableHTTPD;
    property EnableMIDI:Boolean read FEnableMIDI write SetEnableMIDI;
    property FormReady:Boolean read FFormReady;
    procedure ReceiveMessageLocal(var Msg:TMessage);
    procedure ReceiveMessageLocalLegacy(var Msg:TMessage);
    {$IFDEF MSWINDOWS}
    procedure ReceiveMessage(var Msg:TMessage);
    procedure ReceiveMessageExternal(var Msg:TMessage); message WM_VRC10KEY;
    {$ENDIF}
  end;

  TMastodonWS = record
      event : String;
      payload : String;
  end;

var
  VRC10KEYRECEIVER: TVRC10KEYRECEIVER;

implementation

{$R *.fmx}

uses Unit1,
      System.JSON.Serializers, System.JSON.Converters, System.Json, Unit4,
      System.Generics.Collections, System.DateUtils;

{$IFDEF MSWINDOWS}

var
  hHooks : Array [0..1] of HHOOK;

procedure LogWrap(const Source, Verb, Parameter:String); inline;
begin
      if (FormOSCLogger <> nil) and (not FormOSCLogger.LogTerminate) then
      begin
            if (System.MainThreadID = TThread.CurrentThread.ThreadID) then
            begin
                  FormOSCLogger.ReceiveDataINPUTAdd(Source, Verb, Parameter);
            end else
            begin
                  TThread.Queue(nil, procedure()
                  begin
                        FormOSCLogger.ReceiveDataINPUTAdd(Source, Verb, Parameter);
                  end);
            end;
      end;
end;

procedure MidiInProc(hMIDIIn: HMIDIIN; wMsg:UINT; dwInstance:DWORD_PTR; dwParam1:DWORD_PTR; dwParam2:DWORD_PTR); stdcall;
var
      stat, ch, m : Byte;
      data : Array [1..3] of Byte;
      cc, val, note, vel : Byte;
      hpval : Word;
begin
      cc := 0; val := 0; note := 0; vel := 0;

	case wMsg of
		MM_MIM_DATA:
            begin
                  stat := dwParam1 and $FF;

                  // MIDI_STATUS_REALTIME, MIDI_STATUS_SYSEX
                  if (stat in [$F8,$F0]) then exit;

                  ch := stat and $F;
                  m := (stat shr 4) and $F;

                  data[1] := (dwParam1 shr 8) and $FF;
                  data[2] :=(dwParam1 shr 16) and $FF;
                  data[3] :=(dwParam1 shr 24) and $FF;

                  LogWrap('MIDI', 'MM_MIM_DATA', Format('CH=%d, Message=%d, data=(%d, %d, %d)', [ ch+1, m, data[1], data[2], data[3] ]));

                  case m of
                        MIDICODE_NOTEOFF : LogWrap('MIDI', 'MM_MIM_DATA', 'NOTEOFF');//; Note Off  data[1]:NoteNumber, data[2]:Velocity
                        MIDICODE_NOTEON: //; Note On  data[1]:NoteNumber, data[2]:Velocity
                        begin
                              LogWrap('MIDI', 'MM_MIM_DATA', 'NOTEON');

                              note := data[1]; vel := data[2];
                              VRC10KEYRECEIVER.PostMessageLocal( WM_VRC10KEYLOCAL, NativeUINT(MakeLong(Word(APPSEND_MIDIIN), MakeWord(note,vel))), NativeUINT(MakeLong(MakeWord(ch, m), 0)) );
                        end;
                        MIDICODE_POLYPHONIC_AFTERTOUCH: LogWrap('MIDI', 'MM_MIM_DATA', 'POLYPHONIC AFTERTOUCH'); //; Polyphonic aftertouch  data[1]:NoteNumber, data[2]:Poly Pressure
                        MIDICODE_CONTROLCHANGE: //; Control Change data[1]:Controller, data[2]:Value
                        begin
                              LogWrap('MIDI', 'MM_MIM_DATA', 'CONTROL CHANGE');

                              cc := data[1]; val := data[2];
                              VRC10KEYRECEIVER.PostMessageLocal( WM_VRC10KEYLOCAL, NativeUINT(MakeLong(Word(APPSEND_MIDIIN), MakeWord(cc,val))), NativeUINT(MakeLong(MakeWord(ch, m), 0 )));
                        end;

                        MIDICODE_PROGRAMCHANGE: LogWrap('MIDI', 'MM_MIM_DATA', 'PROGRAM CHANGE'); //; Program Change  data[1]:Program
                        MIDICODE_CHANNEL_AFTERTOUCH: LogWrap('MIDI', 'MM_MIM_DATA', 'CHANNEL AFTERTOUCH'); //; Channel aftertouch  data[1]:Aftertouch
                        MIDICODE_PITCHBEND: //; Pitch Bend data[1]:LSB, data[2]:MSB
                        begin
                              hpval := MakeWord(data[1], data[2]);
                              LogWrap('MIDI', 'MM_MIM_DATA', 'PITCHBEND [LSB/MSB] ' + InttoStr(hpval));
                              VRC10KEYRECEIVER.PostMessageLocal( WM_VRC10KEYLOCAL, NativeUINT(MakeLong(Word(APPSEND_MIDIIN), MakeWord(cc,val))), NativeUINT(MakeLong(MakeWord(ch, m), hpval)));
                        end;
                  end;

            end;

		MM_MIM_OPEN: LogWrap('MIDI', 'MM_MIM_OPEN', ''); //OPENED
		MM_MIM_CLOSE: LogWrap('MIDI', 'MM_MIM_CLOSE', ''); //CLOSING
		MM_MIM_LONGDATA: LogWrap('MIDI', 'MM_MIM_LONGDATA', ''); //LONGDATA
		MM_MIM_ERROR: LogWrap('MIDI', 'MM_MIM_ERROR', ''); //ERROR
		MM_MIM_LONGERROR: LogWrap('MIDI', 'MM_MIM_LONGERROR', ''); //LONGERROR
      end;
end;

function Messaging0(nCode: Integer; wParam: WPARAM; lParam: LPARAM):LResult; stdcall;
var
    Msg: TMessage;
begin
      try
            if (nCode > -1) then
            begin
                  if (PMSG(lParam)^.hwnd = VRC10KEYRECEIVER.WindowsHandle) then
                  begin
                        Msg.Msg := PMSG(lparam)^.message; Msg.wParam := PMSG(lparam)^.wParam; Msg.lParam := PMSG(lparam)^.lParam;

                        case Msg.Msg of
                              WM_VRC10KEY: if (VRC10KEYRECEIVER.FEnableSendMsg) then VRC10KEYRECEIVER.ReceiveMessageExternal(Msg);
                              WM_INPUT, WM_APPCOMMAND: VRC10KEYRECEIVER.Dispatch(Msg);
                        end;
                  end;
            end;
      finally
            Result := CallNextHookEx(hHooks[0], nCode, wParam, lParam);
      end;
end;

function Messaging1(nCode: Integer; wParam: WPARAM; lParam: LPARAM):LResult; stdcall;
var
      Msg: TMessage;
begin
      try
            if (nCode > -1) and (wParam = PM_REMOVE) then
            begin
                  if (PMSG(lParam)^.hwnd = VRC10KEYRECEIVER.WindowsHandle) then
                  begin
                        Msg.Msg := PMSG(lparam)^.message; Msg.wParam := PMSG(lparam)^.wParam; Msg.lParam := PMSG(lparam)^.lParam;

                        case Msg.Msg of
                              WM_VRC10KEY: if (VRC10KEYRECEIVER.FEnableSendMsg) then VRC10KEYRECEIVER.ReceiveMessageExternal(Msg);
                              WM_INPUT, WM_APPCOMMAND: VRC10KEYRECEIVER.Dispatch(Msg);
                        end;
                  end;
            end;
      finally
            Result := CallNextHookEx(hHooks[1], nCode, wParam, lParam);
      end;
end;

procedure TVRC10KEYRECEIVER.PostMessageLocal(MsgCode:Cardinal; wParam:WPARAM; lParam:LPARAM);
begin
      PostThreadMessage( FLocalMessageLoop.ThreadID, MsgCode, wParam, lParam );
end;

procedure TVRC10KEYRECEIVER.LocalMessageLoopTerminate(Sender: TObject);
begin
      FLocalMessageLoop := nil;
end;

procedure TVRC10KEYRECEIVER.FormCreate(Sender: TObject);
var
      //i : Cardinal;
      //Rid: packed array [0..RI_BUF-1] of TRAWInputDevice;
      pRid_f, pRid : PRAWInputDevice;
begin
      FMIDIDevices := TStringList.Create;

      FLocalMessageLoop := TThread.CreateAnonymousThread(
      procedure
      var
            oMsg: tagMsg;
      begin
            PeekMessage( oMsg, 0, 0, 0, PM_NOREMOVE );

            while( GetMessage( oMsg, 0, 0, 0 ) )  do
            begin
                  if oMsg.message = WM_QUIT then break;

                  TThread.Queue(nil, procedure()
                  var
                        Msg: TMessage;
                  begin
                        Msg.Msg := oMsg.message;
                        Msg.wParam := oMsg.wParam;
                        Msg.lParam := oMsg.lParam;

                        case oMsg.message of
                              WM_VRC10KEYLOCAL: VRC10KEYRECEIVER.ReceiveMessageLocal(Msg);
                              WM_VRC10KEYLOCALLEGACY: VRC10KEYRECEIVER.ReceiveMessageLocalLegacy(Msg);
                        else
                              VRC10KEYRECEIVER.Dispatch(Msg);
                        end;
                  end);
            end;

      end);

      FLocalMessageLoop.OnTerminate := LocalMessageLoopTerminate;
      FLocalMessageLoop.Start;

      WindowsHandle := FMX.Platform.Win.WindowHandleToPlatform(Handle).Wnd;

      hHooks[0] := SetWindowsHookEx(WH_CALLWNDPROC, @Messaging0, hInstance , GetCurrentThreadId);
      hHooks[1] := SetWindowsHookEx(WH_GETMESSAGE, @Messaging1, hInstance , GetCurrentThreadId);
      //i := 0;

      GetMem(PByte(pRid), sizeof(TRAWInputDevice) * RI_BUF);
      pRid_f := pRid;
      try

            // gamepad
            pRid^.usUsagePage := $0001;
            pRid^.usUsage     := $05;
            pRid^.dwFlags     := RIDEV_INPUTSINK;
            pRid^.hwndTarget  := WindowsHandle;
            Inc(pRid);

            // joystick
            pRid^.usUsagePage := $0001;
            pRid^.usUsage     := $04;
            pRid^.dwFlags     := RIDEV_INPUTSINK;
            pRid^.hwndTarget  := WindowsHandle;
            Inc(pRid);

            // keyboard
            pRid^.usUsagePage := $0001;
            pRid^.usUsage     := $06;
            pRid^.dwFlags     := RIDEV_INPUTSINK;
            pRid^.hwndTarget  := WindowsHandle;
            Inc(pRid);

            // remote commander
            pRid^.usUsagePage := $FFBC;
            pRid^.usUsage     := $88;
            pRid^.dwFlags     := RIDEV_INPUTSINK;
            pRid^.hwndTarget  := WindowsHandle;
            Inc(pRid);

            pRid^.usUsagePage := $000C;
            pRid^.usUsage     := $01;
            pRid^.dwFlags     := RIDEV_INPUTSINK;
            pRid^.hwndTarget  := WindowsHandle;
            Inc(pRid);

            //if RegisterRawInputDevices(Pointer(@Rid), i, SizeOf(TRAWInputDevice)) = 0 then
            if not RegisterRawInputDevices(pRid_f, (NativeUINT(pRid) - NativeUINT(pRid_f)) div SizeOf(TRAWInputDevice),  SizeOf(TRAWInputDevice)) then
            begin
                  ShowMessage('Error' + InttoStr(GetLastError));
            end;
      finally
            FreeMem(PByte(pRid_f));
      end;

      RawTimeLimit := TStopWatch.StartNew;
      RawTimeLimit_KB := TStopWatch.StartNew;
      RawTimeLimit_HID := TStopWatch.StartNew;

      //-------

      FWebSocket := TWebSocket.Create(Self);
      //FWebSocket.OnError := WebSocketError;
      FWebSocket.OnText := NervReceiver;

      FFormReady := True;
end;

procedure TVRC10KEYRECEIVER.WmAppCommand(var Msg: TMessage);
begin
      //PostMessage(WindowHandleToPlatform(Self.Handle).Wnd, WM_VRC10KEY, GlobalPassCode, MakeLong(-2, Msg.LParamHi));
end;

procedure TVRC10KEYRECEIVER.WmInput(var Msg: TMessage);
const
  ARR_BUF = 1024;
  INIT_BYTES = 1024;
var
  i,j: Integer;
  RI: PRAWINPUT;
  dwSize: Cardinal;
  lpb: PByte;
  //DataSize: DWORD;
  HidPacket : Array [0..15] of Cardinal;
  P:PByte;
  //Data: Array [0..INIT_BYTES-1] of Byte;
  pDevName: PByte;
  DevName : String;
  DevNameSize: Integer;
  WindowName : Array [0..INIT_BYTES-1] of Char;
  a,b, dump : String;
  DeviceInfo: TRID_DEVICE_INFO;
begin
      //if (RawTimeLimit.ElapsedMilliseconds < 10) then exit;
      //RawTimeLimit.Stop; RawTimeLimit.Reset; RawTimeLimit.Start;

      //if not FEnableInput then exit;

      ZeroMemory(@WindowName[0], sizeof(WindowName));
      GetWindowText(GetForegroundWindow, @WindowName[0], Length(WindowName)-1);

      a := StrPas(PWideChar(@WindowName[0]));
      b := ExtractFileName(GetWindowModuleName(GetForegroundWindow));

      //if (CompareText(a, 'VRChat') <> 0) or (CompareText(b, 'VRChat.exe') <> 0) then exit;

      // Query header size
      GetRawInputData(PRAWINPUT(Msg.LParam), RID_INPUT, nil, @dwSize,SizeOf(TRAWINPUTHEADER));
      if dwSize = 0 then Exit;

      lpb := AllocMem(dwSize);
      pDevName := AllocMem(INIT_BYTES);
      try
            // Get header
            GetRawInputData(PRAWINPUT(Msg.LParam), RID_INPUT, lpb, @dwSize, SizeOf(TRAWINPUTHEADER));
            RI := PRAWINPUT(lpb);

            // Query devicename size
            GetRawInputDeviceInfoW(RI.header.hDevice, RIDI_DEVICENAME, nil, @dwSize);
            DevNameSize := dwSize;

            // Get devicename
            GetRawInputDeviceInfoW(RI.header.hDevice, RIDI_DEVICENAME, Pointer(pDevName), @dwSize);
            DevName := StrPas(PWideChar(pDevName));

            // Get device information
            DeviceInfo.cbSize := SizeOf(TRID_DEVICE_INFO);
            dwSize := SizeOf(TRID_DEVICE_INFO);
            GetRawInputDeviceInfoW(RI.header.hDevice, RIDI_DEVICEINFO, @DeviceInfo, @dwSize);

            case RI.header.dwType of
                  // keybooard
                  RIM_TYPEKEYBOARD:
                        begin
                              // keycode
                              if FEnableKeyCode then
                              begin
                                    if RawTimeLimit_KB.ElapsedMilliseconds > 250 then
                                    begin
                                          LogWrap('HID', 'WM_INPUT', Format('Device=%s, HIDType=%d, data=%d', [ DevName, RI.header.dwType, RI.Data.keyboard.VKey ]));
                                          PostMessageLocal(WM_VRC10KEYLOCAL, MakeLong(Word(APPSEND_Keycode), RI.Data.keyboard.VKey), 0);

                                          RawTimeLimit_KB.Stop; RawTimeLimit_KB.Reset; RawTimeLimit_KB.Start;
                                    end;
                              end;


                        end;

                  // HID
                  RIM_TYPEHID:
                        begin
                              if RawTimeLimit_HID.ElapsedMilliseconds > 250 then
                              begin
                                    // hid data
                                    p := @RI.Data.hid.bRawData; dump := '';

                                    for i := 0 to RI.Data.hid.dwCount-1 do
                                    begin
                                          HidPacket[i] := 0;
                                          for j := 1 to RI.Data.hid.dwSizeHid do
                                          begin
                                                HidPacket[i] := (HidPacket[i] shl 8) + p^;
                                                Inc(P);
                                          end;

                                          dump := dump + '0x' + InttoHex(HidPacket[i], 8) + ' ';
                                    end;

                                    LogWrap('HID', 'WM_INPUT', Format('Device=%s, HIDType=%d, data=%s', [ DevName, RI.header.dwType, Trim(dump).Replace(' ',',') ]));

                                    RawTimeLimit_HID.Stop; RawTimeLimit_HID.Reset; RawTimeLimit_HID.Start;
                              end;

                        end;

            end;
      finally
            FreeMem(lpb);
            FreeMem(pDevName);
      end;

end;

procedure TVRC10KEYRECEIVER.FormDestroy(Sender: TObject);
begin
      PostThreadMessage( FLocalMessageLoop.ThreadID, WM_QUIT, 0, 0);
      PostThreadMessage( FGamePadThread.ThreadID, WM_QUIT, 0, 0);

      UnhookWindowsHookEx(hHooks[0]);
      UnhookWindowsHookEx(hHooks[1]);

      if Assigned(FWebSocket) then FWebSocket.DisposeOf;

      FMIDIDevices.DisposeOf;

      while Assigned(FGamePadThread) or Assigned(FLocalMessageLoop) do
      begin
            Application.ProcessMessages;
            Sleep(50);
      end;
end;

procedure TVRC10KEYRECEIVER.NervConnected(Sender: TObject);
begin
      if FWebSocket.Connected then
            FWebSocket.Send('{ type: "subscribe", stream: "public" }');
end;

procedure TVRC10KEYRECEIVER.SetEnableNerv(bEnable:Boolean);
begin
      FEnableNerv := bEnable;

      if FEnableNerv then
      begin
            FWebSocket.OnEstablished := NervConnected;
            FWebSocket.Connect(URL_NERV);
      end else
      begin
            //if FWebSocket.Connected then
            //      FWebSocket.Send('{ type: "unsubscribe", stream: "public" }');

            FWebSocket.Close;
      end;
end;

procedure TVRC10KEYRECEIVER.SetEnableHTTPD(bEnable:Boolean);
begin
      FEnableHTTPD := bEnable;
      IdHTTPServer1.Active := FEnableHTTPD;
end;

procedure TVRC10KEYRECEIVER.SetMIDISelected(idx:Integer);
begin
      if (idx >= 0) and (midiInOpen(@FMIDIHandle, idx, DWORD_PTR(@MidiInProc), 0, CALLBACK_FUNCTION) = 0) then
      begin
            FMIDISelected := idx;
            midiInStart(FMIDIHandle);
      end else
      begin
            FMIDISelected := -1;

            if FMIDIHandle > 0 then
            begin
                  midiInStop(FMIDIHandle);
                  midiInReset(FMIDIHandle);
                  midiInClose(FMIDIHandle);
                  FMIDIHandle := 0;
            end;
      end;
end;

procedure TVRC10KEYRECEIVER.SetEnableMIDI(bEnable:Boolean);
var
      i,c : Integer;
      mic: TMidiInCaps;
begin
      FEnableMIDI := bEnable;

      c := midiInGetNumDevs();
      FMIDIDevices.Clear;

	for i := 0 to c-1 do
      begin
		if (midiInGetDevCaps(i, @mic, sizeof(MIDIINCAPS)) = 0) then
            begin
                  FMIDIDevices.AddObject(mic.szPname, TObject(NativeInt(i)));

			//fprintf(stderr,"Device ID #%u: %s\r\n", i, mic.szPname);
		end;
	end;
end;

procedure TVRC10KEYRECEIVER.GamePadThreadTerminate(Sender: TObject);
begin
      FGamePadThread := nil;
end;

procedure TVRC10KEYRECEIVER.SetEnableGamePad(bEnable:Boolean);
begin
      FEnableGamePad := bEnable;

      if ( FEnableGamePad and Assigned(FEnableGamePad) ) or (not FEnableGamePad) then
      begin
            if Assigned(FEnableGamePad) then
            begin
                  PostThreadMessage( FGamePadThread.ThreadID, WM_QUIT, 0, 0);

                  while Assigned(FGamePadThread) do
                  begin
                        Application.ProcessMessages;
                        Sleep(50);
                  end;
            end;
      end;

      if FEnableGamePad then
      begin
            FGamePadThread := TThread.CreateAnonymousThread(procedure()
            var
                  JoyInfoEx: TJoyInfoEx;
                  i,q : Integer;
                  n : UINT64;
                  oMsg: tagMsg;
            begin
                  n := 0; q := 0;

                  PeekMessage( oMsg, 0, 0, 0, PM_NOREMOVE );

                  while True do
                  begin
                        try
                              for i := JOYSTICKID1 to JOYSTICKID2 do
                              begin
                                    ZeroMemory(@JoyInfoEx, sizeof(TJoyInfoEx));
                                    JoyInfoEx.dwSize  := SizeOf(JoyInfoEx);
                                    JoyInfoEx.dwFlags := JOY_RETURNALL;

                                    // GamePad Handler
                                    if joyGetPosEx(i, @JoyInfoEx) = JOYERR_NOERROR then
                                    begin
                                          if (n mod 10) = 0 then LogWrap('HID', 'joyGetPos', Format('Device=%d, wButtons=%x', [ i, JoyInfoEx.wButtons ]));

                                          //FGamePadInfo[q] := JoyInfoEx; Inc(q);
                                          PostMessageLocal(WM_VRC10KEYLOCAL, MakeLong(Word(APPSEND_GamePad), GP_BUTTONS), JoyInfoEx.wButtons);
                                    end;

                                    // Rewind index (Ring buffer)
                                    //if q >= High(FGamePadInfo) then q := 0;
                              end;

                              ZeroMemory(@oMsg, sizeof(oMsg));
                              if PeekMessage( oMsg, 0, 0, 0, PM_NOREMOVE ) then
                              begin
                                    if oMsg.message = WM_QUIT then exit;
                              end;

                        finally
                              if oMsg.message <> WM_QUIT then
                              begin
                                    Sleep(100);
                                    Inc(n);
                              end;
                        end;
                  end;
            end);

            FGamePadThread.FreeOnTerminate := True;
            FGamePadThread.OnTerminate := GamePadThreadTerminate;
            FGamePadThread.Start;
      end;
end;

procedure TVRC10KEYRECEIVER.NervReceiver(Sender: TObject; const iText: String);
var
      DeS : TJsonSerializer;
      JsonRec : TMastodonWS;
      JR : TJSONObject;
      JC : TJSONArray;
      i : Integer;
      CheckString : String;
begin
      DeS := TJsonSerializer.Create;
      try
            CheckString := '';
            JsonRec := DeS.Deserialize<TMastodonWS>(iText);

            if CompareText(JsonRec.event,'update') = 0 then
            begin
                  JR := TJSONObject.ParseJSONValue(JsonRec.payload) as TJSONObject;

                  JC := JR.Values['tags'] as TJsonArray;
                  for i := 0 to JC.Count-1 do
                  begin
                        CheckString := CheckString + (JC.Items[i] as TJsonObject).Values['name'].Value + #09;
                  end;

                  if (Pos('�ً}', CheckString) > 0) or (Pos('�L�^�I�Z���ԑ�J���', CheckString) > 0) then
                  begin
                        PostMessageLocal( WM_VRC10KEYLOCAL, MakeLong(Word(APPSEND_MastodonNerv),0), 0);
                  end;

                  //CheckString := CheckString + JR.Values['content'] + #09;
            end;
      finally
            Des.Free;
      end;
end;

function HTTPDIPFilter(AContext: TIdContext):Boolean;
var
      ip : String;
begin
      Result := True;
      ip := LowerCase(AContext.Binding.PeerIP);

      if not ( ip.StartsWith('127.0.0.1') or ip.StartsWith('192.168.') or ip.StartsWith('fe80:') or ip.StartsWith('::1')  ) then
      begin
            Result := False;
      end;
end;

procedure TVRC10KEYRECEIVER.IdHTTPServer1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
      code : Integer;
      keynum : SmallInt;
      nonce, a1, a2, verify, passcode : String;
      strarr : TArray<String>;
      authreq : TStringList;
      i : Integer;
      html : TStringList;
begin
      if not HTTPDIPFilter(AContext) then exit;

      LogWrap('HTTPD', 'GET', ARequestInfo.RawHTTPCommand );

      if ARequestInfo.Document = '/vrcosc' then
      begin
            code := StrtoIntDef( ARequestInfo.Params.Values['code'] , 0 );
            keynum := StrtoIntDef( ARequestInfo.Params.Values['run'] , -1 );

            PostMessageLocal( WM_VRC10KEYLOCALLEGACY, code, MakeLong(Word(APPSEND_httpdevent), keynum) );

            with AResponseInfo do
            begin
                  ResponseNo := 200;
                  ContentType := 'text/plain; charset=utf-8';
                  CacheControl := 'no-cache';
                  ContentText := InttoStr(DateTimeToUnix(Now));
                  WriteHeader;
                  WriteContent;
                  exit;
            end;

      end else if ARequestInfo.Document = '/vrcoscpad' then
      begin
            if Pos('Digest', ARequestInfo.RawHeaders.Values['Authorization']) > 0 then
            begin
                  authreq := TStringList.Create;
                  try
                        authreq.Text := ARequestInfo.RawHeaders.Values['Authorization'].Replace(',',#13#10);

                        for i := 0 to authreq.Count-1 do
                        begin
                              strarr := [authreq.Names[i], authreq.ValueFromIndex[i]];
                              authreq[i] := Trim(strarr[0]) + '=' + Trim(strarr[1]).DeQuotedString('"');
                              //authreq.Names[] := Trim(authreq.Names[i]);
                              //authreq.ValueFromIndex[i] := Trim(authreq.ValueFromIndex[i]).DeQuotedString('"');
                        end;

                        a1 := MD5('VRCOSC' + ':' + authreq.Values['realm'] + ':' + Format('%.06d', [GlobalPassCodeRaw]));
                        a2 := MD5('GET' + ':'+ authreq.Values['uri']);
                        verify := MD5(a1 + ':' + authreq.Values['nonce'] + ':' + authreq.Values['nc'] + ':' + authreq.Values['cnonce'] + ':' + authreq.Values['qop'] + ':' + a2);

                        if CompareText(verify, authreq.Values['response']) = 0 then
                        begin
                              with AResponseInfo do
                              begin
                                    ResponseNo := 200;

                                    html := TStringList.Create;
                                    try
                                          passcode := InttoStr(GlobalPassCode());

                                          html.Add('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
                                          html.Add('<html xmlns="http://www.w3.org/1999/xhtml">');
                                          html.Add('<head>');
                                          html.Add('<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />');
                                          html.Add('<meta name="viewport" content="width=device-width" />');
                                          html.Add('<title>VRCOSC HTTP Interface</title>');
                                          html.Add('<script>');
                                          html.Add('function send(num) {');
                                          html.Add('var oReq = new XMLHttpRequest(); var code = "'+passcode+'";');
                                          html.Add('oReq.open("GET", "/vrcosc?code=" + code + "&run=" + String(num)); oReq.send();');
                                          html.Add('}');
                                          html.Add('</script>');
                                          html.Add('</head>');
                                          html.Add('<body>');

                                          html.Add('<table style="width: 100%" border="0">');

                                          for i := 0 to 15 do
                                          begin
                                                if (i mod 4) = 0 then html.Add('<tr>');

                                                try
                                                      html.Add('<td><button style="width:100%; height:120px;" onclick="send('+i.ToString+')">'+GlobalCellLabel[i]+'</button></td>');
                                                except
                                                      html.Add('<td><button style="width:100%; height:120px;" onclick="send('+i.ToString+')">---</button></td>');
                                                end;

                                                if ((i+1) mod 4) = 0 then html.Add('</tr>');
                                          end;

                                          html.Add('</table></body></html>');

                                          ContentText := html.Text;
                                    finally
                                          html.Free;
                                    end;

                                    ContentType := 'text/html; charset=utf-8';
                                    CacheControl := 'no-cache';
                                    WriteHeader;
                                    WriteContent;
                              end;

                              exit;
                        end;

                  finally
                        authreq.Free;
                  end;
            end;

            with AResponseInfo do
            begin
                  ResponseNo := 401;
                  nonce := RandomNonce();

                  CustomHeaders.Values['WWW-Authenticate'] := 'Digest realm="UserID: VRCOSC", nonce="'+nonce+'", algorithm=MD5, qop="auth"';
                  //ContentType := 'text/plain; charset=utf-8';
                  CacheControl := 'no-cache';

                  //ContentText := InttoStr(DateTimeToUnix(Now));
                  WriteHeader;
                  //WriteContent;

                  exit;
            end;

      end else
      begin
            with AResponseInfo do
            begin
                  ResponseNo := 403;
                  ContentType := 'text/plain; charset=utf-8';
                  CacheControl := 'no-cache';
                  ContentText := 'Forbidden';
                  WriteHeader;
                  WriteContent;
                  exit;
            end;
      end;
end;

procedure TVRC10KEYRECEIVER.IdHTTPServer1ParseAuthentication(
  AContext: TIdContext; const AAuthType, AAuthData: string; var VUsername,
  VPassword: string; var VHandled: Boolean);
begin
      if CompareText(AAuthType, 'Digest') = 0 then
      begin
            VUsername := 'Dummy';
            VPassword := 'Dummy';
            VHandled := True;
      end;
end;

procedure TVRC10KEYRECEIVER.ReceiveMessageLocal(var Msg:TMessage);
begin
      VTKForm.RunExeCell(GlobalPassCode, Msg.WParam, Msg.LParam);
end;

procedure TVRC10KEYRECEIVER.ReceiveMessageLocalLegacy(var Msg:TMessage);
begin
      VTKForm.RunExeCell(Msg.WParam, Msg.LParam);
end;

procedure TVRC10KEYRECEIVER.ReceiveMessageExternal(var Msg:TMessage);
begin
      // Sender WParam -> Code [PassCode]
      // Sender (Opr)  -> Sender type [CellNumber LoWord]
      // Sender LParam (LoWord) -> Parameter1 [CellNumber HiWord]
      // Sender LParam (HiWord) -> Parameter2 [InternalValue]

      LogWrap('HWND', 'WM_VRC10KEY', Format('Code=hidden, data=(%d,%d)', [ Msg.WParam, LowerWord(Msg.LParam), UpperWord(Msg.LParam)]));

      VTKForm.RunExeCell(
            Msg.WParam,
            Integer(MakeLong(
                  Word(APPSEND_WindowMessage),
                  UpperWord(Integer(Msg.LParam)))
            ),
            LowerWord(Integer(Msg.LParam))
      );
end;

procedure TVRC10KEYRECEIVER.ReceiveMessage(var Msg:TMessage);
begin
      VTKForm.RunExeCell(Msg.WParam, Msg.LParam);
end;
{$ENDIF}

end.
