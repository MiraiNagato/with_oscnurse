unit Unit4;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.Controls.Presentation, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Layouts, AppInclude, AppUtils, FMX.TabControl, FMX.Memo;

type
  TFormOSCLogger = class(TForm)
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Label1: TLabel;
    StringGrid2: TStringGrid;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    PopupColumn1: TPopupColumn;
    Layout1: TLayout;
    TabControl1: TTabControl;
    TabItemOsc: TTabItem;
    TabItemInput: TTabItem;
    Memo1: TMemo;
    Layout2: TLayout;
    CheckBoxFreeze: TCheckBox;
    CheckBoxStopLog: TCheckBox;
    Lang1: TLang;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StringGrid2SelChanged(Sender: TObject);
    procedure CheckBoxStopLogChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private 宣言 }
    FFormReady : Boolean;
    RecEvent : TArray<TReceivedEvent>;
    LastRecTrigger : TDateTime;
    FRecSettingOnChange : TNotifyEvent;
    FLogCopyOSC : TStringList;
    FLogCopyINPUT : TStringList;
    FLogTerminate : Boolean;
    procedure SetRecEvent(Input : TArray<TReceivedEvent>);
    procedure MakeViewOSC();
    procedure MakeViewOSCInternal();
    procedure MakeViewInput();
  public
    { public 宣言 }
    procedure ReceiveDataOSC(const LogData:TStrings);
    procedure ReceiveDataOSCEvent(const ForceExecution:Boolean);
    procedure ReceiveDataINPUTAdd(const Source, Verb, Parameter:String);
    property LogTerminate:Boolean read FLogTerminate;
    property FormReady:Boolean read FFormReady;
    property RecSettingOnChange: TNotifyEvent read FRecSettingOnChange write FRecSettingOnChange;
    property ReceivedEvent:TArray<TReceivedEvent> read RecEvent write SetRecEvent;
  end;

var
  FormOSCLogger: TFormOSCLogger;

implementation

uses
  {$IFDEF MSWINDOWS} FMX.Platform.Win, Winapi.ShellAPI, Winapi.Windows, Winapi.MMSystem, {$ENDIF}
  Unit1, System.StrUtils, System.Math, System.RegularExpressions, System.DateUtils;

{$R *.fmx}

procedure TFormOSCLogger.CheckBoxStopLogChange(Sender: TObject);
begin
      FLogTerminate := (Sender as TCheckBox).IsChecked;
end;

procedure TFormOSCLogger.FormCreate(Sender: TObject);
begin
      FLogCopyOSC := TStringList.Create;
      FFormReady := True;
      FLogTerminate := False;

      FLogCopyINPUT := TStringList.Create;
      LastRecTrigger := Now();

      CheckBoxStopLogChange(CheckBoxStopLog);
end;

procedure TFormOSCLogger.FormDestroy(Sender: TObject);
begin
      FLogCopyOSC.DisposeOf;
      FLogCopyINPUT.DisposeOf;
end;

procedure TFormOSCLogger.FormShow(Sender: TObject);
begin
      MakeViewOSC(); MakeViewInput();
end;

procedure TFormOSCLogger.MakeViewOSC();
begin
      if Assigned(FormOSCLogger) and Self.Visible and (TabControl1.TabIndex = 0) then
      begin
            if ( (timeGettime() mod 10) = 0 ) then
            begin
                  MakeViewOSCInternal();
            end;
      end;
end;

procedure TFormOSCLogger.MakeViewOSCInternal();
var
      i : Integer;
begin
      StringGrid1.BeginUpdate;
      try
            StringGrid1.RowCount := FLogCopyOSC.Count;

            for i := 0 to FLogCopyOSC.Count-1 do
            begin
                  StringGrid1.Cells[0, i] := FLogCopyOSC.Names[i];
                  StringGrid1.Cells[1, i] := FLogCopyOSC.ValueFromIndex[i];
            end;
      finally
            StringGrid1.EndUpdate;
            StringGrid1.Repaint;
      end;
end;

procedure TFormOSCLogger.MakeViewInput();
begin
      if Self.Visible and (TabControl1.TabIndex = 1) then
      begin
            Memo1.BeginUpdate;
            try
                  Memo1.Lines.Assign(FLogCopyInput);

                  if Memo1.Lines.Count > 0 then
                  begin
                        Memo1.SelStart := Length(Memo1.Lines.Text) - Length(Memo1.Lines[Memo1.Lines.Count-1]);
                  end else
                  begin
                        Memo1.SelStart := 0;
                  end;

                  //Memo1.ScrollBy(0, Memo1.ContentBounds.Bottom, False);
            finally
                  Memo1.EndUpdate;
                  Memo1.Repaint;
            end;
      end;
end;

procedure TFormOSCLogger.SetRecEvent(Input : TArray<TReceivedEvent>);
var
      i : Integer;
begin
      for I := 0 to High(Input) do
      begin
            with StringGrid2 do
            begin
                  Cells[0, i] := Input[i].Address;
                  Cells[1, i] := PopupColumn1.Items[ Ord(Input[i].Operation) ];
                  Cells[2, i] := Input[i].Value;
                  Cells[3, i] := Trim(Trim(Input[i].Command) + ' ' + Input[i].CommandParam);
            end;
      end;

      RecEvent := Input;
end;

procedure TFormOSCLogger.ReceiveDataINPUTAdd(const Source, Verb, Parameter:String);
const
      LINE_LIMIT = 1000;
begin
      FLogCopyInput.Add( DateTimeToStr(Now) + ' : [' + Source + '] ' + Verb + #09 + Parameter );
      if FLogCopyInput.Count > LINE_LIMIT then FLogCopyInput.Clear;
      if not CheckBoxFreeze.IsChecked then MakeViewInput();
end;

procedure TFormOSCLogger.ReceiveDataOSC(const LogData:TStrings);
begin
      if Assigned(LogData) then
      begin
            FLogCopyOSC.Assign(LogData);
            FLogCopyOSC.Sort;
      end;

      MakeViewOSC();
      Application.ProcessMessages;

      ReceiveDataOSCEvent(false);
end;

procedure TFormOSCLogger.ReceiveDataOSCEvent(const ForceExecution:Boolean);
const
      EXE_LIMIT_MS_A = 100;
      EXE_LIMIT_MS_B = 700;
var
      i,j,k : Integer;
      v1, v2  : Single;
      s1, s2, Com  : String;
      Res : Boolean;
      mt : TMatch;
      Parameters : TArray<String>;
      ShellAPIProcess : TThreadProcedure;
      ValueChanged : Boolean;
begin
      // REC Event
      for i := 0 to High(RecEvent) do
      begin
            // 値の変化を検出、ロックを外す
            ValueChanged := CompareStr(FLogCopyOSC.Values[RecEvent[i].Address], RecEvent[i].LastProcessValue) <> 0;
            if (ValueChanged and RecEvent[i].ExecutionLock) then RecEvent[i].ExecutionLock := False;

            if (RecEvent[i].Operation <> otNull) and (not RecEvent[i].ExecutionLock) then
            begin
                  j := FLogCopyOSC.IndexOfName(RecEvent[i].Address);
                  if j >= 0 then
                  begin
                        s1 := FLogCopyOSC.ValueFromIndex[j];
                        s2 := RecEvent[i].Value;

                        SetLength(Parameters, 1); Parameters[0] := '';

                        if RecEvent[i].Operation in [TRecOperationType.otContain, TRecOperationType.otNotContain, TRecOperationType.otRegExp] then
                        begin
                              Res := False;

                              case RecEvent[i].Operation of
                                    TRecOperationType.otContain: Res := Pos(s2, s1) > 0;
                                    TRecOperationType.otNotContain: Res := Pos(s2, s1) = 0;
                                    TRecOperationType.otRegExp:
                                    begin
                                          mt := TRegEx.Match(s1, s2);
                                          Res := mt.Success;
                                          SetLength(Parameters, mt.Groups.Count+1);
                                          for k := 1 to mt.Groups.Count-1 do Parameters[k] := mt.Groups.Item[k].Value;
                                    end;
                              end;

                        end else if IsNumeric(RecEvent[i].Value) then
                        begin
                              v1 := StrtoFloatDef(s1,0.0);
                              v2 := StrtoFloatDef(s2,0.0);

                              Res := False;
                              case RecEvent[i].Operation of
                                    TRecOperationType.otLess: Res := (v1 > v2);
                                    TRecOperationType.otGreater: Res := (v1 < v2);
                                    TRecOperationType.otLessEq: Res := (v1 >= v2);
                                    TRecOperationType.otGreaterEq: Res := (v1 <= v2);
                                    TRecOperationType.otEqual: Res := CompareValue(v1,v2) = 0;
                                    TRecOperationType.otNotEqual: Res := CompareValue(v1,v2) <> 0;
                              end;
                        end else
                        begin
                              Res := False;
                              case RecEvent[i].Operation of
                                    TRecOperationType.otLess: Res := CompareStr(s1,s2) > 0;
                                    TRecOperationType.otGreater: Res := CompareStr(s1,s2) < 0;
                                    TRecOperationType.otLessEq: Res := CompareStr(s1,s2) >= 0;
                                    TRecOperationType.otGreaterEq: Res := CompareStr(s1,s2) <= 0;
                                    TRecOperationType.otEqual: Res := CompareStr(s1,s2) = 0;
                                    TRecOperationType.otNotEqual: Res := CompareStr(s1,s2) <> 0;
                              end;
                        end;

                        // 条件を満たした
                        if Res then
                        begin
                              // 前回からの経過時間が EXE_LIMIT_MS_B ミリ秒以内は実行しない
                              if ValueChanged and (System.DateUtils.MilliSecondSpan(RecEvent[i].LastExecution, Now()) > EXE_LIMIT_MS_B) then
                              begin
                                    // 立ち上がりロック
                                    RecEvent[i].ExecutionLock := True;

                                    Parameters[0] := s1;
                                    Com := ReplaceByArray( RecEvent[i].Command, Parameters, '%');

                                    ShellAPIProcess := procedure()
                                    var
                                          SI : TShellExecuteInfo;
                                          LRet  : Cardinal;
                                    begin
                                          FillChar(SI, SizeOf(SI), 0);
                                          SI.cbSize := SizeOf(SI);
                                          //SI.fMask  := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI;
                                          SI.fMask := SEE_MASK_NOASYNC or SEE_MASK_FLAG_NO_UI;
                                          SI.lpFile       := PChar(RecEvent[i].Command);
                                          SI.lpParameters := PChar(RecEvent[i].CommandParam);
                                          SI.Wnd    := WindowHandleToPlatform(Self.Handle).Wnd;
                                          SI.lpVerb := 'open';
                                          SI.nShow  := SW_SHOWNOACTIVATE;

                                          ShellExecuteEx(@SI);

                                          (*ShellExecute(
                                                WindowHandleToPlatform(Self.Handle).Wnd,
                                                'open', PChar(RecEvent[i].Command), PChar(RecEvent[i].CommandParam),nil, SW_SHOWNOACTIVATE
                                          );
                                          *)
                                    end;

                                    if TThread.CurrentThread.ThreadID = System.MainThreadID then
                                          ShellAPIProcess()
                                    else
                                          TThread.Queue(nil, ShellAPIProcess);

                                    RecEvent[i].LastExecutionValue := s1;
                                    RecEvent[i].LastExecution := Now();
                              end;
                        end;

                  end;

                  Application.ProcessMessages;
            end;

            RecEvent[i].LastProcessValue := s1;
      end;

      LastRecTrigger := Now();
end;

procedure TFormOSCLogger.StringGrid2SelChanged(Sender: TObject);
var
      i,j : Integer;
      s,op,val : String;
      Cmd : String;
      //sa : TStringList;
begin
      //sa := TStringList.Create;
      try
            SetLength(RecEvent, StringGrid2.RowCount);

            j := 0;
            for i := 0 to StringGrid2.RowCount-1 do
            begin
                  s := Trim(StringGrid2.Cells[0, i]);
                  op :=  Trim(StringGrid2.Cells[1, i]);
                  val := Trim(StringGrid2.Cells[2, i]);
                  Cmd := StringGrid2.Cells[3, i];

                  if (Length(s) > 0) and (Length(cmd) > 0) then
                  begin
                        RecEvent[j].Address := s;
                        //RecEvent[j].Operation := op;
                        RecEvent[j].Value := Val;

                        RecEvent[j].Command := '';
                        RecEvent[j].CommandParam := '';
                        RecEvent[j].LastExecutionValue := '';
                        RecEvent[j].LastExecution := (Now() - 0.041);

                        ParseCommandLine(Cmd, RecEvent[j].Command, RecEvent[j].CommandParam);

                        //RecEvent[j].Command := Cmd;

                        case IndexStr(op, ['>','<','>=','<=','=','!=',LC('含む'),LC('含まない'),LC('正規表現')]) of
                              0: RecEvent[j].Operation := TRecOperationType.otLess;
                              1: RecEvent[j].Operation := TRecOperationType.otGreater;
                              2: RecEvent[j].Operation := TRecOperationType.otLessEq;
                              3: RecEvent[j].Operation := TRecOperationType.otGreaterEq;
                              4: RecEvent[j].Operation := TRecOperationType.otEqual;
                              5: RecEvent[j].Operation := TRecOperationType.otNotEqual;
                              6: RecEvent[j].Operation := TRecOperationType.otContain;
                              7: RecEvent[j].Operation := TRecOperationType.otNotContain;
                              8: RecEvent[j].Operation := TRecOperationType.otRegExp;
                        else
                              RecEvent[j].Operation := TRecOperationType.otNull;
                        end;

                        Inc(j);
                  end;

            end;

            SetLength(RecEvent, j);

      finally

      end;

      if Assigned(FRecSettingOnChange) then FRecSettingOnChange(Self);
end;

procedure TFormOSCLogger.Timer1Timer(Sender: TObject);
begin
      if Assigned(FormOSCLogger) and Self.Visible and (TabControl1.TabIndex = 0) then
      begin
            MakeViewOSCInternal();
      end;
end;

end.
