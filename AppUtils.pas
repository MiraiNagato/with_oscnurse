unit AppUtils;

interface

uses
  {$IFDEF MSWINDOWS} Winapi.Windows, Winapi.Messages, FMX.Platform.Win, Winapi.Nb30, {$ENDIF}
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.TypInfo, System.Hash, AppInclude, System.Generics.Collections,
  System.Diagnostics;

const
  PASSKEY_INIT_VALUE = 63021912;

(* Platform Utilities *)

{$IFDEF MSWINDOWS}
function GetWindowModuleName(const hHandle: HWND): String;
function GetLocalMacAddress(const Addresses:TStrings; const ExceptVendor:Array of String):Integer;
function GetRemoteMacAdress(var address: AnsiString): Boolean;
function GetSteamUserIdent():UInt64;
function GetSteamPath():String;
function ParseCommandLine(const Cmd:String; var FilePath:String; var Parameters:String):Integer;
{$ENDIF}

(* Generic Utilities *)
procedure nop();
procedure EndianConv(const Any32Bit:Pointer);
function IsEmptyOrNull(const Value: Variant): Boolean;
function IsDecimal(const s:String):Boolean;
function IsNumeric(const s:String):Boolean;

(* String Utilities *)
function EzMatch(const pattern:string; const s:string):string;
function ReplaceByArray(const s:String; const Param:Array of String; const MARK:Char = '%'):String;
function NextChar(p:PWideChar):PWideChar;

(* OSC Utilities *)
procedure MemoryAlignment(const AStream:TStream; const AlignSize: Integer = 4; const WritePadding: Boolean = True);
function BuildOSCPacket(const Buffer:TMemoryStream; const InputAddress, InputType:String; const InputValue:Single; var WrittenValue:Variant):Integer;

(* Local Utilities *)
function LC(const BasisString:String):String;
function GetKeycodeName(keycode:Byte):String;
function CellRoll(iRoll:Integer; iParameter:Variant):TCellRoll;
function GenerateAccessToken(const SHA1_UserIDHash, SHA1_PassCodeText:String; const DateFormat:String = SALTDATE_DEFAULT; const LowPrecision:Boolean = False):Cardinal;
function MIDIGetNoteName(code:Byte):String; overload;
function MIDIGetNoteName(code:TMIDINote):String; overload;
function MIDIGetCCName(code:TMIDICtrlChange):String;
function MIDIGetMCULocalCodeName(code:TMIDIProcCode):String;
function KeyCodeNameMod(const KeycodeName:String):String;
function GlobalPassCode():Cardinal;

(* Bit Utilities *)
function LowerByte(Value:Word):Byte;
function UpperByte(Value:Word):Byte;
function LowerWord(Value:Cardinal):Word;
function UpperWord(Value:Cardinal):Word;
function LowerLong(Value:UINT64):Cardinal;
function UpperLong(Value:UINT64):Cardinal;
function MakeUINT64(LoLong,HiLong: Cardinal):UINT64;

(* Security Utilities *)
function RandomNonce(const Len:Integer = 50):String;
function MD5(const s:String):String;
function SHA1(const s:String):String;

implementation

uses
  {$IFDEF MSWINDOWS} System.Win.Registry, Winapi.ShellAPI, Winapi.PsApi, Winapi.Winsock, {$ENDIF}
  System.StrUtils, System.RegularExpressions, System.Math;

procedure nop();
asm
      nop;
end;

procedure EndianConv(const Any32Bit:Pointer);
var
      Data : P32BitArray;
      Src : T32bitArray;
begin
      Src := P32BitArray(Any32Bit)^;
      Data := Any32Bit;

      Data^.b1 := Src.b4;
      Data^.b2 := Src.b3;
      Data^.b3 := Src.b2;
      Data^.b4 := Src.b1;
end;

procedure MemoryAlignment(const AStream:TStream; const AlignSize: Integer = 4; const WritePadding: Boolean = True);
var
      AddSize : Integer;
      Empty : Byte;
      i : Integer;
begin
      AddSize := AlignSize - AStream.Position mod AlignSize;

      if AddSize <> AlignSize then
      begin
            if WritePadding then
            begin
                  Empty := 0;
                  for i := 1 to AddSize do AStream.Write(Empty, 1);
            end else
            begin
                  AStream.Seek(soFromCurrent, AddSize);
            end;
      end;
end;

function BuildOSCPacket(const Buffer:TMemoryStream; const InputAddress, InputType:String; const InputValue:Single; var WrittenValue:Variant):Integer;
const
      MEMORY_INITCAPACITY = 1000;
var
      Addr, Com : AnsiString;
      DataInt : Integer;
      DataFloat : Single;
begin
      Buffer.SetSize(MEMORY_INITCAPACITY);

      Result := 0; WrittenValue := Unassigned;
      Addr := AnsiString(InputAddress) + #0;
      Buffer.Write(PAnsiChar(Addr)^, Length(Addr));
      MemoryAlignment(Buffer);

      case IndexText(InputType, [VRCOSCType_Int, VRCOSCType_Bool, VRCOSCType_Float, VRCOSCType_None]) of
            0:
            begin
                  Com := ',i' + #0;
                  Buffer.Write(PAnsiChar(Com)^, Length(Com));
                  MemoryAlignment(Buffer);

                  DataInt := Round(InputValue);
                  WrittenValue := DataInt;
                  EndianConv(PInteger(@DataInt));

                  Buffer.Write(DataInt,sizeof(DataInt));
                  MemoryAlignment(Buffer);
            end;
            1:
            begin
                  Com := ',i' + #0;
                  Buffer.Write(PAnsiChar(Com)^, Length(Com));
                  MemoryAlignment(Buffer);

                  DataInt := Round(InputValue);
                  WrittenValue := DataInt;
                  EndianConv(PInteger(@DataInt));

                  Buffer.Write(DataInt,sizeof(DataInt));
                  MemoryAlignment(Buffer);
            end;
            2:
            begin
                  Com := ',f' + #0;
                  Buffer.Write(PAnsiChar(Com)^, Length(Com));
                  MemoryAlignment(Buffer);

                  DataFloat:= InputValue;
                  WrittenValue := DataFloat;
                  EndianConv(PSingle(@DataFloat));

                  Buffer.Write(DataFloat,sizeof(DataFloat));
                  MemoryAlignment(Buffer);
            end;
            3:
            begin
                  WrittenValue := Null;
            end
      else
            // no argument
      end;

      Result := Buffer.Position;

end;

function ParseCommandLine(const Cmd:String; var FilePath:String; var Parameters:String):Integer;
var
      ArgCount : Integer;
      Args : PPWideChar;
      i : Integer;
begin
      Args := Pointer(CommandLineToArgvW(PWideChar(Cmd), ArgCount));
      Result := ArgCount-1;

      FilePath := '';
      Parameters := '';

      if ArgCount > 0 then
      begin
            for i := 0 to ArgCount-1 do
            begin
                  if i = 0 then
                        FilePath := String(StrPas(Args^)).DeQuotedString('"')
                  else
                        Parameters := Parameters + ' "' + String(StrPas(Args^)).DeQuotedString + '"';

                  Inc(Args)
            end;
      end;
end;

function IsNumeric(const s:String):Boolean;
begin
      Result := not IsNaN( StrtoFloatDef(s, NaN) );
end;

function IsDecimal(const s:String):Boolean;
var
      i : Integer;
      p,pf : PWideChar;
begin
      if Length(Trim(s)) = 0 then
      begin
            Result := False;
            exit;
      end;

      Result := True;
      p := PWideChar(s);
      pf := p;

      while (p^ <> #0) do
      begin
            try
                  if (pf = p) then
                  begin
                        if not (p^ in ['+','-','0'..'9']) then
                        begin
                              Result := False;
                              break;
                        end;
                  end else
                  begin
                        if not (p^ in ['0'..'9']) then
                        begin
                              Result := False;
                              break;
                        end;
                  end;

            finally
                  Inc(p);
            end;
      end;
end;


function IsEmptyOrNull(const Value: Variant): Boolean;
begin
  Result := VarIsClear(Value) or VarIsEmpty(Value) or VarIsNull(Value) or (VarCompareValue(Value, Unassigned) = vrEqual);
  if (not Result) and VarIsStr(Value) then
    Result := Value = '';
end;

function EzMatch(const pattern:string; const s:string):string;
var
      ss : String;
      Mt : TMatch;
begin
      ss := TRegEx.Escape(pattern, true).Replace('%d','((?:\+|\-)?[0-9]+)').Replace('%f','((?:\+|\-)?[\.0-9eE]+)').Replace('%x','((?:\+|\-)?(?:0x)?[0-9a-fA-F]+)');
      Mt := TRegEx.Match(s, ss);

      Result := '';
      if Mt.Success then Result := Mt.Groups.Item[Mt.Groups.Count-1].Value;
end;

function ReplaceByArray(const s:String; const Param:Array of String; const MARK:Char = '%'):String;
var
      p : PWideChar;
      i : Integer;
begin
      p := PWideChar(@s[1]);

      Result := '';
      while (p^ <> #0) do
      begin
            if (p^ = MARK) and ( IsDecimal(NextChar(p)^)  ) then
            begin
                  i := StrtoIntDef(p^,0);
                  if i <= High(Param) then Result := Result + Param[i];
                  Inc(p,2);
                  continue;
            end;

            Result := Result + p^;
            Inc(p);
      end;
end;

function NextChar(p:PWideChar):PWideChar; inline;
begin
      Inc(p);
      Result := p;
end;

function CellRoll(iRoll:Integer; iParameter:Variant):TCellRoll;
begin
      Result.iRoll := iRoll;

      Result.iParameter := '';
      if not IsEmptyOrNull(iParameter) then Result.iParameter := String(iParameter);
end;

(*function GetKeycodeName(keycode:Byte):String;
begin
      Result := Copy(GetEnumName(TypeInfo(TVirtualKeys),keycode),3, 100);
end;
*)

function GenerateAccessToken(const SHA1_UserIDHash, SHA1_PassCodeText:String; const DateFormat:String = SALTDATE_DEFAULT; const LowPrecision:Boolean = False):Cardinal;
var
      SHA1: THashSHA1;
      Hash : TBytes;
      PHash : PCardinal;
      i : Integer;
      DateString : String;
begin
      SHA1 := THashSHA1.Create;

      DateString := FormatDateTime(DateFormat, Now);
      if LowPrecision then DateString := Copy(DateString, 1, Length(DateString)-1);

      SHA1.Update( DateString + ':' + SHA1_UserIDHash + ':' + SHA1_PassCodeText );

      Hash := SHA1.HashAsBytes;
      PHash := PCardinal(@Hash[0]);

      Result := PASSKEY_INIT_VALUE;
      for i := 1 to 5 do
      begin
            Result := Result xor PHash^;
            Inc(PHash);
      end;
end;

function LowerByte(Value:Word):Byte; inline;
begin
      Result := Value and $FF;
end;

function UpperByte(Value:Word):Byte; inline;
begin
      Result := (Value shr 8) and $FF;
end;

function LowerWord(Value:Cardinal):Word; inline;
begin
      Result := Value and $FFFF;
end;

function UpperWord(Value:Cardinal):Word; inline;
begin
      Result := (Value shr 16) and $FFFF;
end;

function LowerLong(Value:UINT64):Cardinal; inline;
begin
      Result := Value and $FFFFFFFF;
end;

function UpperLong(Value:UINT64):Cardinal; inline;
begin
      Result := (Value shr 32) and $FFFFFFFF;
end;

function MakeUINT64(LoLong, HiLong: Cardinal):UINT64;
begin
      Result := UInt64(LoLong) or (UInt64(HiLong) shl 32);
end;

function RandomNonce(const Len:Integer = 50):String;
var
      i : Integer;
begin
      Result := '';
      for i := 0 to Len-1 do
      begin
            Result := Result + SHA1(FloatToStr(Now) + InttoStr(RandomRange(Low(Integer), High(Integer))));

            if Length(Result) > Len then
            begin
                  Result := Copy(Result, 1, Len);
                  exit;
            end;
      end;
end;

function MD5(const s:String):String; inline;
var
      MD5: THashMD5;
begin
      MD5 := THashMD5.Create;
      MD5.Update(s);
      Result := LowerCase(MD5.HashAsString);
end;

function SHA1(const s:String):String; inline;
var
      SHA1: THashSHA1;
begin
      SHA1 := THashSHA1.Create;
      SHA1.Update(s);
      Result := LowerCase(SHA1.HashAsString);
end;

function GlobalPassCode():Cardinal;
begin
      Result := GenerateAccessToken(GlobalUserIDHash, GlobalPassCodeHash);
end;

function GenericEnumName(TypeInfo: PTypeInfo; Value: Integer; LeftTrim:Integer = 2):String; inline;
const
      CP_LIMIT = 100;
begin
      Result := Copy(GetEnumName(TypeInfo,Value), LeftTrim+1, CP_LIMIT);
end;

function MIDIGetNoteName(code:Byte):String; inline;
begin
      Result := GenericEnumName( TypeInfo(TMIDINote),code, 2);
end;

function MIDIGetNoteName(code:TMIDINote):String; inline;
begin
      Result := GenericEnumName( TypeInfo(TMIDINote), Byte(code), 2);
end;

function MIDIGetCCName(code:TMIDICtrlChange):String; inline;
begin
      Result := GenericEnumName( TypeInfo(TMIDICtrlChange), Byte(code), 2);
end;

function MIDIGetMCULocalCodeName(code:TMIDIProcCode):String; inline;
begin
      Result := GenericEnumName( TypeInfo(TMIDIProcCode), Byte(code), 2);
end;

function LC(const BasisString:String):String;
begin
      if not TranslateDictionary.TryGetValue(GlobalLocaleID+':'+BasisString, Result) then Result := BasisString;
end;

function GetKeycodeName(keycode:Byte):String; inline;
begin
      Result := GenericEnumName( TypeInfo(TVirtualKeys),keycode, 2);
end;

function KeyCodeNameMod(const KeycodeName:String):String;
const
      JPOnly = ' [JP]';
var
      s : String;
begin
      s := KeycodeName.Replace('Num','');

      if s = 'Multiply' then s := 'pad ��'
      else if s = 'Add' then s := 'pad �{'
      else if s = 'Subtract' then s := 'pad �|'
      else if s = 'Decimal' then s := 'pad �D'
      else if s = 'Separator' then s := 'pad �C'
      else if s = 'Divide' then s := 'pad �^'
      else if s = 'Colon' then s := '�F' + JPOnly
      else if s = 'Semicolon' then s := '�G' + JPOnly
      else if s = 'Comma' then s := '�C' + JPOnly
      else if s = 'Minus' then s := '�|' + JPOnly
      else if s = 'Period' then s := '�D' + JPOnly
      else if s = 'Slash' then s := '�^' + JPOnly
      else if s = 'OEMAt' then s := '��' + JPOnly;

      Result := s;
end;
{$IFDEF MSWINDOWS}

function GetWindowModuleName(const hHandle: HWND): String;
var
  ProcessId : DWORD;
  hProcess : THandle;
  data : Array [0..4096] of Char;
begin
  Result := '';
  GetWindowThreadProcessId(hHandle, ProcessId);

  hProcess := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, False, ProcessId);
  try
    ZeroMemory(@data[0], sizeof(data));
    if (GetModuleFileNameExW(hProcess, 0, @data[0], Length(data)-1 ) <> 0) then Result := StrPas(PWideChar(@data[0]));
  finally
    CloseHandle(hProcess);
  end;

end;

function GetLocalMacAddress(const Addresses:TStrings; const ExceptVendor:Array of String):Integer;
var
      Ncb: TNCB;
      RetCode: Integer;
      lenum: TLanaEnum;
      s : String;
      i,j,c: integer;
      Adapter: TAstat;
      bl : Boolean;
begin
      ZeroMemory(@Ncb, SizeOf(NCB));
      Ncb.ncb_command := Char(NCBENUM);
      Ncb.ncb_buffer := @lenum;
      Ncb.ncb_length := SizeOf(lenum);
      Netbios(@Ncb);

      c := 0;

      for i := 0 to integer(lenum.length) - 1 do
      begin
            ZeroMemory(@Ncb, SizeOf(NCB));
            Ncb.ncb_command := Char(NCBRESET);
            Ncb.ncb_lana_num := lenum.lana[i];
            Netbios(@Ncb);

            ZeroMemory(@Ncb, SizeOf(NCB));
            Ncb.ncb_command := Char(NCBASTAT);
            Ncb.ncb_lana_num := lenum.lana[i];
            Ncb.ncb_callname := '* ';
            Ncb.ncb_buffer := @Adapter;
            Ncb.ncb_length := SizeOf(Adapter);
            RetCode := Byte(Netbios(@Ncb));

            if RetCode = 0 then
            begin
                  s := UpperCase(Format('%.02X-%.02X-%.02X-%.02X-%.02X-%.02X', [
                        Byte(Adapter.adapt.adapter_address[0]),
                        Byte(Adapter.adapt.adapter_address[1]),
                        Byte(Adapter.adapt.adapter_address[2]),
                        Byte(Adapter.adapt.adapter_address[3]),
                        Byte(Adapter.adapt.adapter_address[4]),
                        Byte(Adapter.adapt.adapter_address[5])]));


                  bl := False;
                  for j := 0 to High(ExceptVendor) do
                  begin
                        if s.StartsWith(UpperCase(ExceptVendor[j])) then
                        begin
                              bl := True;
                              break;
                        end;
                  end;

                  if not bl then Addresses.Add(s);

                  Inc(c);
            end;

      end;

      Result := c;
end;

function GetRemoteMacAdress(var address: AnsiString): Boolean;
var
  dwRemoteIP: DWORD;
  PhyAddrLen: Longword;
  pMacAddr : array [0..1] of Longword;
  temp: array [0..5] of byte;
  I: Byte;
begin
  Result := false;
  dwremoteIP := inet_addr(PAnsiChar(AnsiString(address)));
  if dwremoteIP <> 0 then begin
    PhyAddrLen := 6;
    if SendARP (dwremoteIP, 0, @pMacAddr, @PhyAddrLen) = NO_ERROR then begin
      if (PhyAddrLen <> 0) and (pMacAddr[0] <> 0) then begin
         Move (pMacAddr, temp, 6);
         address := '';
         For I := 0 to 5 do address := address + AnsiString(inttohex (temp[i], 2))+'-';
         Delete (address, Length (address), 1);
         Result := true;
      end;
    end;
  end;
end;

function GetSteamUserIdent():UInt64;
var
      s : TStringList;
      Path : String;
      i : Integer;
      RM : TMatch;
begin
      Result := 0;

      s := TStringList.Create;
      try
            Path := IncludeTrailingPathDelimiter(GetSteamPath()) + 'config\loginusers.vdf';

            if FileExists(Path) then
            begin
                  s.LoadFromFile(Path);

                  for i := 0 to s.Count-1 do
                  begin
                         RM := TRegEx.Match(s[i],'^\t+"([0-9]+)"');

                         if RM.Success then
                         begin
                              Result := StrtoUInt64Def( RM.Groups.Item[1].Value, 0 );
                              break;
                         end;

                  end;
            end;

      finally
            s.Free;
      end;
end;

function GetSteamPath():String;
const
      AppKey='\steam\Shell\Open\Command';
var
      Reg:TRegistry;
      Arg : PWideChar;
      Args : PPWideChar;
      ArgCount: Integer;
begin
      Result := '';
      Reg:=TRegistry.Create;
      try
            with Reg do
            begin
                  RootKey := HKEY_CLASSES_ROOT;
                  if OpenKeyReadOnly(AppKey) then
                  begin
                        Arg := PWideChar(ReadString(''));
                        try
                              Args := Pointer(CommandLineToArgvW(Arg, ArgCount));

                              if ArgCount > 0 then
                              begin
                                    Result := ExtractFileDir(String(StrPas(Args^))).DeQuotedString('"');
                              end;
                        except
                        end;
                  end;
            end;
      finally
            Reg.Free;
      end;
end;

{$ENDIF}

initialization
      TranslateDictionary := TDictionary<string, string>.Create;
      GlobalTimer := TStopWatch.StartNew;

finalization
      TranslateDictionary.DisposeOf;
      GlobalTimer.Stop;

end.
