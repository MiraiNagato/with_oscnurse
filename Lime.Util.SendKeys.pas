unit Lime.Util.SendKeys;

interface

uses
      Winapi.Windows, Winapi.Messages, System.Math, System.Sysutils,
      System.StrUtils, System.Generics.Collections;

(*
      Translated / Refactoring SendKeys copyright info (MIT)
      -------------------------------------------------------
      SendKeys (SendkeyExtended.pas) routine for Delphi 10.x.
      Copyright (c) 2022 NAGATO Mirai <hayate@zuh.jp>
      Permission is hereby granted, free of charge, to any person obtaining
      a copy of this software and associated documentation files (the "Software"),
      to deal in the Software without restriction, including without limitation
      the rights to use, copy, modify, merge, publish, distribute, sublicense,
      and/or sell copies of the Software, and to permit persons to whom the
      Software is furnished to do so, subject to the following conditions:

      The above copyright notice and this permission notice shall be included
      in all copies or substantial portions of the Software.

      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
      DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
      THE USE OR OTHER DEALINGS IN THE SOFTWARE.

      Translated SendKeys copyright info (2-caused BSD)
      --------------------------------------------------
      SendKeys routine (SendKeys.cpp / SendKeys.h) for C++
      Copyright (c) 2004 lallous <lallousx86@yahoo.com>
      All rights reserved.

      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions
      are met:
      1. Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
      IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
      ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
      FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
      DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
      OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
      HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
      LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
      OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
      SUCH DAMAGE.

      The Original SendKeys copyright info
      ---------------------------------------
      SendKeys (sndkeys32.pas) routine for 32-bit Delphi.
      Written by Ken Henderson
      Copyright (c) 1995 Ken Henderson <khen@compuserve.com>
*)

type
      TSKCEnumWindowCallback = record
            str: PChar;
            ihwnd: HWND;
      end;

      PSKCEnumWindowCallback = ^TSKCEnumWindowCallback;

      TSKCustomCallBack = reference to procedure (const Key:String; const Params:TArray<String>);
      TSKCustomCallBackMethod = procedure (Sender:TObject; const Key:String; const Params:TArray<String>) of object;

      TSKCKeyDesc = record
            VKey: Char;
            normalkey: Boolean; (* a normal character or a VKEY ? *)
            CustomCallBack : TSKCustomCallBack;
            CustomCallBackMethod : TSKCustomCallBackMethod;
      end;

      TSendKeys = class
      private
            FWait: Boolean;
            FUsingParens: Boolean;
            FShiftDown: Boolean;
            FAltDown: Boolean;
            FControlDown: Boolean;
            FWinDown: Boolean;
            FQueue: Boolean;
            FVkey: Boolean;
            FnDelayAlways: Cardinal;
            FnDelayNow: Cardinal;
            FInputBuffer: TArray<TINPUT>;
            FInputPtr: integer;
            FKeyNames: TDictionary<string, TSKCKeyDesc>;
            FSleepTime : Integer;
            FCaseSensitive : Boolean;
            function IncChar(const P: PChar; const c: integer = 1): PChar; inline;
            function BitSet(const BitTable: Byte; const BitMask: UINT): Boolean; inline;
            function StrCmp(const A:String; const B:String):Integer;inline;
            function KD(const VKey: WORD; const normalkey: Boolean = False; const CustomCallBack:TSKCustomCallBack = nil; const CustomCallBackMethod:TSKCustomCallBackMethod = nil): TSKCKeyDesc; overload;
            function KD(const VKey: Char; const normalkey: Boolean = False; const CustomCallBack:TSKCustomCallBack = nil; const CustomCallBackMethod:TSKCustomCallBackMethod = nil): TSKCKeyDesc; overload;
            function GetInput(index:Integer):PINPUT;
            procedure SetInput(index:Integer; Element:PINPUT);
            function CS(A:String):String;inline;
      protected
            procedure StatesClear();
            procedure CarryDelay();
            function IsVkExtended(const VKey: Byte): Boolean;
            procedure SendKeyUp(const VKey: Byte);
            procedure SendKeyDown(const VKey: Byte; const NumTimes: WORD; const GenUpMsg: Boolean; const bDelay: Boolean = False);
            function StringToVKey(const KeyString: String): TSKCKeyDesc;
            procedure PopUpShiftKeys();
      public
            constructor Create(const ACaseSensitive:Boolean = False);
            destructor Destroy; override;
            function RegisterCustomCallBack(const KeyString:String; const CallBackFunction:TSKCustomCallBack):Boolean; overload;
            function RegisterCustomCallBack(const KeyString:String; const CallBackMethod:TSKCustomCallBackMethod):Boolean; overload;
            function UnRegisterCustomCallBack(const KeyString:String):Boolean;
            procedure SendKey(const MKey: WORD; const NumTimes: WORD; const GenDownMsg: Boolean);
            function SendKeys(const KeysString: String; const Wait: Boolean = False): Boolean;
            procedure KeyboardEvent(const VKey: Byte; const ScanCode: Byte; const Flags: longint);
            procedure Flush;
            function AppActivate(const wnd: HWND): Boolean; overload;
            function AppActivate(const WindowTitle: PChar; const WindowClass: PChar = nil): Boolean; overload;
            property SleepTime:Integer read FSleepTime write FSleepTime;
            property InputBuffer[index:Integer]:PINPUT read GetInput write SetInput;
            property CaseSensitive:Boolean read FCaseSensitive;
      end;

implementation

const
      SKC_MaxExtendedVKeys = 10;
      SKC_VKKEYSCANSHIFTON = $01;
      SKC_VKKEYSCANCTRLON = $02;
      SKC_VKKEYSCANALTON = $04;
      SKC_INVALIDKEY = $FFFF;

      SKC_ExtendedVKeys: Array [0 .. Pred(SKC_MaxExtendedVKeys)] of Byte = (
            VK_UP,
            VK_DOWN,
            VK_LEFT,
            VK_RIGHT,
            VK_HOME,
            VK_END,
            VK_PRIOR, // PgUp
            VK_NEXT, // PgDown
            VK_INSERT,
            VK_DELETE
      );

{$EXTERNALSYM SendInputAPI}
function SendInputAPI(cInputs: UINT; pInputs: PInput; cbSize: integer): UINT;
  stdcall; external user32 name 'SendInput';

constructor TSendKeys.Create(const ACaseSensitive:Boolean);
begin
      inherited Create;
      FnDelayNow := 0;
      FnDelayAlways := 0;
      FCaseSensitive := ACaseSensitive;
      StatesClear();
      SetLength(FInputBuffer, 64);

      FKeyNames := TDictionary<string, TSKCKeyDesc>.Create;
      with FKeyNames do
      begin
            Add(CS('ADD'), KD(VK_ADD));
            Add(CS('APPS'), KD(VK_APPS));
            Add(CS('AT'), KD('@', true));
            Add(CS('BACKSPACE'), KD(VK_BACK));
            Add(CS('BKSP'), KD(VK_BACK));
            Add(CS('BQT'), KD('`', true));
            Add(CS('BREAK'), KD(VK_CANCEL));
            Add(CS('BS'), KD(VK_BACK));
            Add(CS('CAPSLOCK'), KD(VK_CAPITAL));
            Add(CS('CARET'), KD('^', true));
            Add(CS('CLEAR'), KD(VK_CLEAR));
            Add(CS('DECIMAL'), KD(VK_DECIMAL));
            Add(CS('DEL'), KD(VK_DELETE));
            Add(CS('DELETE'), KD(VK_DELETE));
            Add(CS('DIVIDE'), KD(VK_DIVIDE));
            Add(CS('DOWN'), KD(VK_DOWN));
            Add(CS('END'), KD(VK_END));
            Add(CS('ENTER'), KD(VK_RETURN));
            Add(CS('ESC'), KD(VK_ESCAPE));
            Add(CS('ESCAPE'), KD(VK_ESCAPE));
            Add(CS('F1'), KD(VK_F1));
            Add(CS('F10'), KD(VK_F10));
            Add(CS('F11'), KD(VK_F11));
            Add(CS('F12'), KD(VK_F12));
            Add(CS('F13'), KD(VK_F13));
            Add(CS('F14'), KD(VK_F14));
            Add(CS('F15'), KD(VK_F15));
            Add(CS('F16'), KD(VK_F16));
            Add(CS('F2'), KD(VK_F2));
            Add(CS('F3'), KD(VK_F3));
            Add(CS('F4'), KD(VK_F4));
            Add(CS('F5'), KD(VK_F5));
            Add(CS('F6'), KD(VK_F6));
            Add(CS('F7'), KD(VK_F7));
            Add(CS('F8'), KD(VK_F8));
            Add(CS('F9'), KD(VK_F9));
            Add(CS('GREATER'), KD('>', true));
            Add(CS('HELP'), KD(VK_HELP));
            Add(CS('HOME'), KD(VK_HOME));
            Add(CS('INS'), KD(VK_INSERT));
            Add(CS('LEFT'), KD(VK_LEFT));
            Add(CS('LEFTBRACE'), KD('{', true));
            Add(CS('LEFTPAREN'), KD('(', true));
            Add(CS('LESS'), KD('<', true));
            Add(CS('LWIN'), KD(VK_LWIN));
            Add(CS('MULTIPLY'), KD(VK_MULTIPLY));
            Add(CS('NUMLOCK'), KD(VK_NUMLOCK));
            Add(CS('NUMPAD0'), KD(VK_NUMPAD0));
            Add(CS('NUMPAD1'), KD(VK_NUMPAD1));
            Add(CS('NUMPAD2'), KD(VK_NUMPAD2));
            Add(CS('NUMPAD3'), KD(VK_NUMPAD3));
            Add(CS('NUMPAD4'), KD(VK_NUMPAD4));
            Add(CS('NUMPAD5'), KD(VK_NUMPAD5));
            Add(CS('NUMPAD6'), KD(VK_NUMPAD6));
            Add(CS('NUMPAD7'), KD(VK_NUMPAD7));
            Add(CS('NUMPAD8'), KD(VK_NUMPAD8));
            Add(CS('NUMPAD9'), KD(VK_NUMPAD9));
            Add(CS('PERCENT'), KD('%', true));
            Add(CS('PGDN'), KD(VK_NEXT));
            Add(CS('PGUP'), KD(VK_PRIOR));
            Add(CS('PLUS'), KD('+', true));
            Add(CS('PRTSC'), KD(VK_PRINT));
            Add(CS('RIGHT'), KD(VK_RIGHT));
            Add(CS('RIGHTBRACE'), KD('}', true));
            Add(CS('RIGHTPAREN'), KD(')', true));
            Add(CS('RWIN'), KD(VK_RWIN));
            Add(CS('SCROLL'), KD(VK_SCROLL));
            Add(CS('SEPARATOR'), KD(VK_SEPARATOR));
            Add(CS('SNAPSHOT'), KD(VK_SNAPSHOT));
            Add(CS('SUBTRACT'), KD(VK_SUBTRACT));
            Add(CS('TAB'), KD(VK_TAB));
            Add(CS('TILDE'), KD('~', true));
            Add(CS('UP'), KD(VK_UP));
            Add(CS('WIN'), KD(VK_LWIN));
      end;
end;

destructor TSendKeys.Destroy;
begin
      SetLength(FInputBuffer, 0);
      FKeyNames.Free;
      inherited;
end;

procedure TSendKeys.StatesClear();
begin
      FWait := False;
      FUsingParens := False;
      FShiftDown := False;
      FAltDown := False;
      FControlDown := False;
      FWinDown := False;
      FQueue := False;
end;

function TSendKeys.RegisterCustomCallBack(const KeyString:String; const CallBackFunction:TSKCustomCallBack):Boolean;
begin
      Result := False;

      if not FKeyNames.ContainsKey(CS(KeyString)) then
      begin
            FKeyNames.Add(CS(KeyString), KD(0, False, CallBackFunction, nil) );
            Result := True;
      end;
end;

function TSendKeys.RegisterCustomCallBack(const KeyString:String; const CallBackMethod:TSKCustomCallBackMethod):Boolean;
begin
      Result := False;

      if not FKeyNames.ContainsKey(CS(KeyString)) then
      begin
            FKeyNames.Add(CS(KeyString), KD(0, False, nil, CallBackMethod) );
            Result := True;
      end;
end;

function TSendKeys.UnRegisterCustomCallBack(const KeyString:String):Boolean;
begin
      Result := False;

      if not FKeyNames.ContainsKey(CS(KeyString)) then
      begin
            if (FKeyNames[CS(KeyString)].VKey = #0) then
            begin
                  FKeyNames.Remove(CS(KeyString));
                  Result := True;
            end;
      end;
end;

function TSendKeys.KD(const VKey: WORD; const normalkey: Boolean = False; const CustomCallBack:TSKCustomCallBack = nil;  const CustomCallBackMethod:TSKCustomCallBackMethod = nil)
  : TSKCKeyDesc;
begin
      Result := KD(WideChar(VKey), normalkey, CustomCallBack);
end;

function TSendKeys.KD(const VKey: Char; const normalkey: Boolean = False; const CustomCallBack:TSKCustomCallBack = nil;  const CustomCallBackMethod:TSKCustomCallBackMethod = nil)
  : TSKCKeyDesc;
begin
      Result.VKey := VKey;
      Result.normalkey := normalkey;
      Result.CustomCallBack := CustomCallBack;
end;

function TSendKeys.IncChar(const P: PChar; const c: integer = 1): PChar;
var
      P2: PChar;
begin
      P2 := P;
      Inc(P2, c);
      Result := P2;
end;

function TSendKeys.GetInput(index:Integer):PINPUT;
begin
      Result := nil;
      if High(FInputBuffer) >= index then Result := @FInputBuffer[index];
end;

procedure TSendKeys.SetInput(index:Integer; Element:PINPUT);
const
      margin = 32;
begin
      if High(FInputBuffer) >= index then FInputBuffer[index] := Element^
      else
      begin
            // dynamic allocate
            SetLength(FInputBuffer, index + margin);
            FInputBuffer[index] := Element^;
      end;
end;

function TSendKeys.StrCmp(const A:String; const B:String):Integer;
begin
      if FCaseSensitive then
            Result := CompareStr(A,B)
      else
            Result := CompareText(A,B);
end;

function TSendKeys.CS(A:String):String;
begin
      Result := IfThen( FCaseSensitive, A, UpperCase(A));
end;

procedure TSendKeys.KeyboardEvent(const VKey: Byte; const ScanCode: Byte; const Flags: longint);
var
      VKeyM: Byte;
      ScanCodeRaw: WORD;
      ScanCodeM: Byte;
      ExtendedFlag: WORD;
      FlagsM: Cardinal;
begin

      (* keybd_event(VKey, ScanCode, Flags, 0); *)

      VKeyM := VKey;
      case VKey of
            VK_SHIFT:
                  VKeyM := VK_LSHIFT;
            VK_CONTROL:
                  VKeyM := VK_LCONTROL;
            VK_MENU:
                  VKeyM := VK_LMENU;
      end;

      ScanCodeRaw := MapVirtualKeyExW(VKeyM, MAPVK_VK_TO_VSC_EX,
        GetKeyboardLayout(0));
      ScanCodeM := LOBYTE(ScanCodeRaw);
      ExtendedFlag := IfThen(HIBYTE(ScanCodeRaw) >= $E0,
        KEYEVENTF_EXTENDEDKEY, 0);
      FlagsM := ExtendedFlag or Flags;

      // RtlFillMemory(@t_input[i_input],sizeof(INPUT),$00);
      FInputBuffer[FInputPtr].Itype := INPUT_KEYBOARD;
      FInputBuffer[FInputPtr].ki.wVk := IfThen(FVkey, VKey, 0);
      FInputBuffer[FInputPtr].ki.wScan := ScanCodeM;
      FInputBuffer[FInputPtr].ki.dwFlags := FlagsM or Cardinal(IfThen(FVkey, 0, KEYEVENTF_SCANCODE));
      FInputBuffer[FInputPtr].ki.time := 0;
      FInputBuffer[FInputPtr].ki.dwExtraInfo := GetMessageExtraInfo();

      if not FQueue then
      begin
            SendInputAPI(FInputPtr + 1, @FInputBuffer[0], sizeof(TINPUT));
            Sleep(FSleepTime);
            FInputPtr := 0;

            if FWait then Flush;
      end
      else
      begin
            Inc(FInputPtr);
      end;

      FQueue := False;

end;

procedure TSendKeys.Flush;
var
      KeyboardMsg: TMSG;
begin
      while PeekMessage(KeyboardMsg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE) do
      begin
            TranslateMessage(KeyboardMsg);
            DispatchMessage(KeyboardMsg);
      end;
end;

function TSendKeys.IsVkExtended(const VKey: Byte): Boolean;
var
      i: integer;
begin

      Result := False;
      for i := 0 to Pred(SKC_MaxExtendedVKeys) do
      begin
            if SKC_ExtendedVKeys[i] = VKey then
            begin
                  Result := true;
                  break;
            end;
      end;

end;

(* Generates KEYUP *)
procedure TSendKeys.SendKeyUp(const VKey: Byte);
var
      ScanCode: Byte;
begin
      ScanCode := LOBYTE(MapVirtualKey(VKey, 0));
      KeyboardEvent(VKey, ScanCode, KEYEVENTF_KEYUP or
        IfThen(IsVkExtended(VKey), KEYEVENTF_EXTENDEDKEY, 0));
end;

procedure TSendKeys.SendKeyDown(const VKey: Byte; const NumTimes: WORD; const GenUpMsg: Boolean; const  bDelay: Boolean);
var
      Cnt: WORD;
      ScanCode: Byte;
      //NumState: Boolean;
      INPUT: array [0 .. Pred(2)] of TINPUT;
begin
      //ScanCode := 0;
      //NumState := False;
      if VKey = VK_NUMLOCK then
      begin
            for Cnt := 1 to NumTimes do
            begin
                  if bDelay then
                        CarryDelay();

                  INPUT[0].Itype := INPUT_KEYBOARD;
                  INPUT[0].ki.wVk := VKey;
                  INPUT[0].ki.dwFlags := 0;

                  INPUT[1].Itype := INPUT_KEYBOARD;
                  INPUT[1].ki.wVk := VKey;
                  INPUT[1].ki.dwFlags := KEYEVENTF_KEYUP;

                  SendInputAPI(sizeof(INPUT) div sizeof(INPUT), @INPUT[0],
                    sizeof(TINPUT));
            end;

            exit;
      end;

      ScanCode := LOBYTE(MapVirtualKey(VKey, 0));
      (* Get scancode *)

      (* Send keys *)
      for Cnt := 1 to NumTimes do
      begin
            (* Carry needed delay ? *)
            if bDelay then CarryDelay();

            KeyboardEvent(VKey, ScanCode, IfThen(IsVkExtended(VKey),
              KEYEVENTF_EXTENDEDKEY, 0));

            if GenUpMsg then
            begin
                  FQueue := False;
                  KeyboardEvent(VKey, ScanCode, IfThen(IsVkExtended(VKey),
                    KEYEVENTF_EXTENDEDKEY, 0) or KEYEVENTF_KEYUP);
            end;
      end;

      FQueue := False;

end;

(* Checks whether a bit is set *)
function TSendKeys.BitSet(const BitTable: Byte; const BitMask: UINT): Boolean;
begin
      Result := (BitTable and BitMask) > 0;
end;

(* Sends a single key *)
procedure TSendKeys.SendKey(const MKey: WORD; const NumTimes: WORD; const GenDownMsg: Boolean);
begin
      if (HIBYTE(MKey) and (SKC_VKKEYSCANSHIFTON or SKC_VKKEYSCANCTRLON or
        SKC_VKKEYSCANALTON)) > 0 then
            FQueue := true;

      (* Send appropriate shift keys associated with the given VKey *)
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANSHIFTON) then
            SendKeyDown(VK_SHIFT, 1, False);
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANCTRLON) then
            SendKeyDown(VK_CONTROL, 1, False);
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANALTON) then
            SendKeyDown(VK_MENU, 1, False);

      (* Send the actual VKey *)
      SendKeyDown(LOBYTE(MKey), NumTimes, GenDownMsg, true);

      (* toggle up shift keys *)
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANSHIFTON) then
            SendKeyUp(VK_SHIFT);
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANCTRLON) then
            SendKeyUp(VK_CONTROL);
      if BitSet(HIBYTE(MKey), SKC_VKKEYSCANALTON) then
            SendKeyUp(VK_MENU);
end;

(* Implements a simple binary search to locate special key name strings *)
function TSendKeys.StringToVKey(const KeyString: String): TSKCKeyDesc;
begin
      if not FKeyNames.TryGetValue(CS(KeyString), Result) then Result.VKey := #0;
end;

(* Releases all shift keys (keys that can be depressed while other keys are being pressed *)
(* If we are in a modifier group this function does nothing *)
procedure TSendKeys.PopUpShiftKeys();
begin
      if not FUsingParens then
      begin
            if FShiftDown then
                  SendKeyUp(VK_SHIFT);
            if FControlDown then
                  SendKeyUp(VK_CONTROL);
            if FAltDown then
                  SendKeyUp(VK_MENU);
            if FWinDown then
                  SendKeyUp(VK_LWIN);
            StatesClear();
      end;
end;

(* Sends a key string *)
function TSendKeys.SendKeys(const KeysString: String; const Wait: Boolean): Boolean;
var
      MKey: WORD;
      NumTimes: WORD;
      KeyString, KeyStringRaw: String;
      SP : Integer;
      pKey: PChar;
      ch, ch2: Char;
      P, P1 : PChar;
      t,t2 : size_t;
      KeyArray, KeyArray2: TArray<String>;
      frequency, delay: Cardinal;
      MkeyDesc: TSKCKeyDesc;
begin
      NumTimes := 0;
      KeyString := '';
      Result := False;
      pKey := PChar(KeysString);

      FWait := Wait;

      StatesClear();
      FInputPtr := 0;

      while pKey^ <> #0 do
      begin
            ch := pKey^;

            case WORD(ch) of
                  (* begin modifier group *)
                  WORD('('):
                        begin
                              FUsingParens := true;
                        end;
                  WORD(')'):

                        (* end modifier group *)
                        begin
                              FUsingParens := False;
                              PopUpShiftKeys();
                        end;
                  WORD('>'):
                        (* pop all shift keys when we finish a modifier group close *)

                        begin
                              FQueue := true;
                        end;

                  WORD('`'):
                        begin
                              FVkey := not FVkey;
                        end;
                  WORD('%'):

                        (* ALT key *)
                        begin
                              FAltDown := true;
                              FQueue := true;
                              SendKeyDown(VK_MENU, 1, False);
                        end;
                  WORD('+'):

                        (* SHIFT key *)
                        begin
                              FShiftDown := true;
                              FQueue := true;
                              SendKeyDown(VK_SHIFT, 1, False);
                        end;
                  WORD('^'):

                        (* CTRL key *)
                        begin
                              FControlDown := true;
                              FQueue := true;
                              SendKeyDown(VK_CONTROL, 1, False);
                        end;
                  WORD('@'):

                        (* WINKEY (Left-WinKey) *)
                        begin
                              FWinDown := true;
                              FQueue := true;
                              SendKeyDown(VK_LWIN, 1, False);
                        end;
                  WORD('~'):

                        (* enter *)
                        begin
                              SendKeyDown(VK_RETURN, 1, true);
                              PopUpShiftKeys();
                        end;

                  WORD('{'):
                        (* begin special keys *)
                        begin
                              (* find end of close *)
                              KeyStringRaw := StrPas( IncChar(pKey,1) );

                              t := Pos( '}', KeyStringRaw, 1); t2 := Pos( '}', KeyStringRaw, t+1);
                              t := Max(t, t2);

                              (* Invalidate key *)
                              KeyArray := [#0];
                              if t > 0 then
                              begin
                                    //Inc(P,t);
                                    KeyString := Copy(StrPas(pKey), 2, t-1);
                                    Inc(pKey, t);

                                    MKey := SKC_INVALIDKEY;
                                    KeyArray := Trim(KeyString).Split([' ', ','], TStringSplitOptions.ExcludeEmpty);
                              end;

                              (* sending arbitrary vkeys? *)
                              if StrCmp(KeyArray[0], 'VKEY') = 0 then
                              begin
                                    if Length(KeyArray) >= 2 then
                                    begin
                                        MKey := StrtoIntDef(KeyArray[1], 0);

                                        NumTimes := 1;
                                        if Length(KeyArray) >= 3 then NumTimes := StrtoIntDef(KeyArray[2],1);

                                        if (MKey > 0) then
                                        begin
                                          SendKey(MKey, NumTimes, true);
                                          PopUpShiftKeys();
                                        end;
                                    end;
                              end else if StrCmp(KeyArray[0], 'BEEP') = 0 then
                              begin
                                    if Length(KeyArray) >= 3 then
                                    begin
                                        frequency := StrtoIntDef(KeyArray[1], 0);
                                        delay := StrtoIntDef(KeyArray[2], 0);
                                        Winapi.Windows.Beep(frequency, delay);
                                    end;

                              (* Should activate a window? *)
                              end else if (CompareStr(KeyArray[0], 'APPACTIVATE') = 0) or (StrCmp(KeyArray[0], 'APP') = 0) then
                              begin
                                        if Length(KeyArray) >= 2 then
                                        AppActivate(PChar(KeyArray[1]));

                              (* want to send/set delay? *)
                              end else if StrCmp(KeyArray[0], 'DELAY') = 0 then
                              begin
                                        (* Advance to parameters *)
                                        (* set "sleep factor" *)
                                        if Length(KeyArray) >= 2 then
                                        begin
                                          if KeyArray[1][1] = '=' then
                                                FnDelayAlways := StrtoIntDef(Copy(KeyArray[1], 2, 64), 0)
                                          (* Take number after the '=' character *)
                                          else
                                                FnDelayNow := StrtoIntDef(KeyArray[1], 0);
                                        end;
                                        (* set "sleep now" *)

                              (* Lock WorkStation? *)
                              end else if StrCmp(KeyArray[0], 'LOCKWORKSTATION') = 0 then
                              begin
                                        LockWorkStation();

                              (* not command special keys, then process as keystring to VKey *)
                              end else
                              begin
                                        MkeyDesc := StringToVKey(KeyArray[0]);
                                        MKey := WORD(MkeyDesc.VKey);

                                        NumTimes := 1; (* Does the key string have also count specifier? *) (* Take the specified number of times *)
                                        if (Length(KeyArray) >= 2) then NumTimes := StrtoIntDef(KeyArray[1],1);

                                        (* Key found in table *)
                                        if WORD(MkeyDesc.VKey) > 0 then
                                        begin
                                          if MkeyDesc.normalkey then
                                                MKey := VkKeyScanEx(Char(WORD(MkeyDesc.VKey) and $FF),GetKeyboardLayout(0));
                                        end else if Assigned(MkeyDesc.CustomCallBack) then
                                        begin
                                          MkeyDesc.CustomCallBack(KeyArray[0], KeyArray);
                                        end else if Assigned(MkeyDesc.CustomCallBackMethod) then
                                        begin
                                          MkeyDesc.CustomCallBackMethod(Self, KeyArray[0], KeyArray);
                                        end else
                                        begin
                                          MKey := SKC_INVALIDKEY;

                                          case Length(KeyArray[0]) of
                                                0: MKey := 0;
                                                1: if Word(KeyArray[0][1]) > 0 then MKey := VkKeyScanEx(KeyArray[0][1], GetKeyboardLayout(0));
                                          else
                                                if (KeyArray[0].StartsWith('{')) and (KeyArray[0].EndsWith('}')) then
                                                begin
                                                      MkeyDesc := StringToVKey(Copy(KeyArray[0],2,Length(KeyArray[0])-2));

                                                      if ( WORD(MkeyDesc.VKey) > 0 ) then
                                                      begin
                                                            if MkeyDesc.normalkey then
                                                            MKey := VkKeyScanEx(Char(WORD(MkeyDesc.VKey) and $FF),GetKeyboardLayout(0));
                                                      end else if Assigned(MkeyDesc.CustomCallBack) then
                                                      begin
                                                            MkeyDesc.CustomCallBack(KeyArray[0], KeyArray);
                                                      end else if Assigned(MkeyDesc.CustomCallBackMethod) then
                                                      begin
                                                            MkeyDesc.CustomCallBackMethod(Self, KeyArray[0], KeyArray);
                                                      end;

                                                end;
                                          end;

                                        end;

                                        (* A valid key to send? *)
                                        if (MKey < SKC_INVALIDKEY) and (MKey > 0) then
                                        begin
                                          SendKey(MKey, NumTimes, true);
                                          PopUpShiftKeys();
                                        end;
                              end;

                        end;

            else
                  (* a normal key was pressed *)
                  (* Get the VKey from the key *)
                  MKey := VkKeyScanEx(ch, GetKeyboardLayout(0));
                  SendKey(MKey, 1, true);
                  PopUpShiftKeys();
            end; { case? }

            Inc(pKey);
      end;

      if (FInputPtr > 0) then
      begin
            SendInputAPI(FInputPtr + 1, @FInputBuffer[0], sizeof(TINPUT));
            Sleep(FSleepTime);
            FInputPtr := 0;
      end;

      FUsingParens := False;
      PopUpShiftKeys();

      Result := true;
end;

function TSendKeys.AppActivate(const wnd: HWND): Boolean;
begin
      if wnd = 0 then
      begin
            Result := False;
            exit;
      end;

      SendMessage(wnd, WM_SYSCOMMAND, SC_HOTKEY, NativeINT(wnd));
      SendMessage(wnd, WM_SYSCOMMAND, SC_RESTORE, NativeINT(wnd));
      ShowWindow(wnd, SW_SHOW);
      SetForegroundWindow(wnd);
      SetFocus(wnd);

      Result := true;
end;


function enumwindowsProc(wnd: HWND; lParam: LPARAM): Boolean; stdcall;
var
      t: PSKCEnumWindowCallback;
      str: PChar;
      bMatch: Boolean;
      szClass: array [0 .. Pred(300)] of Char;
      szTitle: array [0 .. Pred(300)] of Char;
      Query: TArray<String>;
begin
      t := Pointer(lParam);
      str := t.str;

      Query := String(str).Split([#09], TStringSplitOptions.ExcludeEmpty);

      bMatch := False;

      if (Length(Query) >= 1) then
      begin
            if GetWindowText(wnd, szTitle, sizeof(szTitle)) > 0 then
                  bMatch := bMatch or
                    (Pos(Query[0], StrPas(PWideChar(@szTitle[0]))) > 0);
      end;

      if (Length(Query) >= 2) then
      begin
            if GetClassName(wnd, szClass, sizeof(szClass)) > 0 then
                  bMatch := bMatch or
                    (Pos(Query[1], StrPas(PWideChar(@szClass[0]))) > 0);
      end;

      Result := true;

      if bMatch then
      begin
            t.ihwnd := wnd;
            Result := False;
      end;

end;

(* Searchs and activates a window given its title or class name *)
function TSendKeys.AppActivate(const WindowTitle: PChar; const WindowClass: PChar): Boolean;
var
      w: HWND;
      t: TSKCEnumWindowCallback;
begin
      w := FindWindow(WindowClass, WindowTitle);
      if w = 0 then
      begin

            (* Must specify at least a criteria *)
            if (WindowTitle = nil) and (WindowClass = nil) then
            begin
                  Result := False;
                  exit;
            end;

            t.ihwnd := 0;
            t.str := PWideChar(String(WindowTitle) + #09 + String(WindowClass));
            EnumWindows(@enumwindowsProc, LPARAM(@t));
            w := t.ihwnd;
      end;

      if w = 0 then
      begin
            Result := False;
            exit;
      end;

      Result := AppActivate(w);
end;

(* Carries the required delay and clears the m_nDelaynow value *)
procedure TSendKeys.CarryDelay();
begin
      (* Should we delay once? *)
      if FnDelayNow = 0 then
            FnDelayNow := FnDelayAlways; (* should we delay always? *)

      (* No delay specified? *)
      if (FnDelayNow > 0) then
            Sleep(FnDelayNow);

      FnDelayNow := 0;

end;

end.
