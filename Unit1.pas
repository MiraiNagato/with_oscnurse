﻿unit Unit1;

interface

uses
  {$IFDEF MSWINDOWS} Winapi.Windows, Winapi.ShellAPI, FMX.Platform.Win, {$ENDIF}
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient, Unit2, FMX.Menus,
  IdUDPServer, IdGlobal, IdSocketHandle, FMX.Controls.Presentation, System.Math,
  FMX.ScrollBox, FMX.Memo, FMX.StdCtrls, FMX.Edit, System.Generics.Collections,
  AppInclude, AppUtils, FMX.ComboEdit, FMX.Objects, FMX.TabControl, FMX.ExtCtrls,
  FMX.Effects, FMX.EditBox, FMX.SpinBox, FMX.ListBox, FMX.Grid.Style, FMX.Grid,
  System.JSON, FMX.Ani;

type
  TUDPProxySettings = record
      Enable : Boolean;
      Host : String;
      Port : Integer;
  end;

  TAppUseDevices = record
      MIDIIn : String;
      MIDIOut : String;
      Mouse : String;
      Digitizer : String;
      GamePad : String;
      JoyStick : String;
      KeyBoard : String;
      Others : TArray<String>;
  end;

  TAppSettings = record
      EnableFunction : TArray<Boolean>;
      UserLocale : String;
      CryptedPassCode: UInt64;
      UseDevices : TAppUseDevices;
      UDPProxy : TUDPProxySettings;
      CellRecords : TArray<TCellSettings>;
      RecRecords : TArray<TReceivedEvent>;
      Verify : String;
  end;

  TVTKForm = class(TForm)
    IdUDPClient1: TIdUDPClient;
    PopupMenu1: TPopupMenu;
    IdUDPServer1: TIdUDPServer;
    SaveTimer: TTimer;
    Layout2: TLayout;
    Layout3: TLayout;
    Splitter1: TSplitter;
    PlotGrid1: TPlotGrid;
    Rectangle1: TRectangle;
    CornerButton1: TCornerButton;
    TabControl1: TTabControl;
    GridPanelLayout1: TGridPanelLayout;
    TabItem1: TTabItem;
    GlowEffect1: TGlowEffect;
    Layout4: TLayout;
    Expander1: TExpander;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox3: TCheckBox;
    Memo1: TMemo;
    StyleBook1: TStyleBook;
    Expander2: TExpander;
    Panel1: TPanel;
    Label1: TLabel;
    PassCodeEdit: TEdit;
    PasswordEditButton1: TPasswordEditButton;
    CreatePassCodeButton: TEditButton;
    Memo2: TMemo;
    Panel3: TPanel;
    Layout5: TLayout;
    Panel4: TPanel;
    Button1: TButton;
    Button4: TButton;
    Button3: TButton;
    ComboEditPreset: TComboEdit;
    Panel2: TPanel;
    Label2: TLabel;
    IdUDPClient2: TIdUDPClient;
    Expander3: TExpander;
    Memo3: TMemo;
    EditUDPRHost: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    SpinUDPRPort: TSpinBox;
    CheckUDPREnable: TCheckBox;
    CheckBox5: TCheckBox;
    ComboMIDIInSelecter: TComboBox;
    Label5: TLabel;
    Layout1: TLayout;
    Image1: TImage;
    LabelVersion: TLabel;
    Lang1: TLang;
    Expander4: TExpander;
    ComboLang: TComboBox;
    PopupMenu2: TPopupMenu;
    MenuCellCopy: TMenuItem;
    MenuCellPaste: TMenuItem;
    Rectangle2: TRectangle;
    procedure FormCreate(Sender: TObject);
    procedure IdUDPServer1UDPRead(AThread: TIdUDPListenerThread;
      const AData: TIdBytes; ABinding: TIdSocketHandle);
    procedure FormDestroy(Sender: TObject);
    procedure CreatePassCodeButtonClick(Sender: TObject);
    procedure PassCodeEditKeyUp(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure PassCodeEditChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
    procedure MenuKeyCandClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure SaveTimerTimer(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ComboEditPresetEnter(Sender: TObject);
    procedure ComboEditPresetChange(Sender: TObject);
    procedure ComboEditPresetExit(Sender: TObject);
    procedure Splitter1Resized(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PlotGrid1Click(Sender: TObject);
    procedure CheckUDPREnableChange(Sender: TObject);
    procedure EditUDPRHostKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure EditUDPRHostExit(Sender: TObject);
    procedure SpinUDPRPortChange(Sender: TObject);
    procedure CheckBox5Change(Sender: TObject);
    procedure ComboMIDIInSelecterChange(Sender: TObject);
    procedure ComboLangChange(Sender: TObject);
    procedure MenuCellCopyClick(Sender: TObject);
    procedure MenuCellPasteClick(Sender: TObject);

  private
    { private 宣言 }
    FSaveDataCounter : UInt64;
    FIntialized : Boolean;
    FUDPPr : TUDPProxySettings;
    FMCUCode : Array [0..32] of TMIDIProcCode;
    FLangList : TStringList;
    FFormList : TList<TFmxObject>;
    procedure MenuItemClick(Sender:TObject);
    procedure CellChangeEvent(Sender:TObject);
    procedure CellExecuteAnimationEvent(Sender:TObject);
    procedure CellExecuteEvent(Sender:TObject; CellIndex:Integer);
    procedure RecChangeEvent(Sender:TObject);

    procedure ReleasePressAll(const ExceptBtn:TControl);
    procedure ClickInspecterClick(Sender:TObject);
    procedure ClickInspecterButton(Sender:TObject);
    procedure ClickInspecterMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure ReScanOSCMenuByDefine(Sender:TObject);
    procedure OpenOSCDirectoryShell(Sender:TObject);
  public
    { public 宣言 }
    UserIDHash : String;
    NicMacHash : String;
    KnownParameters : TStringList;
    CellList : TDictionary<Integer, TExecutionCell>;
    Translater : TJsonObject;

    property Intialized:Boolean read FIntialized write FIntialized;
    procedure ScanOSCDefines();
    procedure InitLocalize();
    procedure ApplyLocalize(const LocaleCode:String='ja-tokyo'; const ResStringOnly:Boolean=False; const ApplySelectMenu:Boolean = False);
    procedure ClearOSCMenuByDefines();
    procedure RunExeCell(const PassCode: Cardinal; const CellNumber: Integer; const InternalValue:Integer = 0); overload;
    procedure RunExeCell(const CellNumber: Integer; const RawValue:Single = NaN); overload;
    procedure LoadUserData(bForce:Boolean = False);
    procedure RestoreCell(const AData:TCellSettings);
    procedure RestoreCells(const ADataArray:TArray<TCellSettings>);
    procedure SaveUserData(bForce:Boolean = False);
    function UserDataMakeVerifyCode(const AppData:TAppSettings):String;
  end;

var
  VTKForm: TVTKForm;

implementation

{$R *.fmx}

uses Unit4, Unit3,
      System.JSON.Serializers, System.JSON.Converters, System.JSON.types,
      System.Hash, System.IOUtils, System.StrUtils, FMX.DialogService;

procedure TVTKForm.FormDestroy(Sender: TObject);
begin
      SaveUserData(True);

      if (IdUDPClient2.Connected) then IdUDPClient2.Disconnect;

      FormOSCLogger.DisposeOf;
      VRC10KEYRECEIVER.DisposeOf;

      FormOSCLogger := nil;
      VRC10KEYRECEIVER := nil;

      FLangList.DisposeOf;
      KnownParameters.DisposeOf;
      CellList.DisposeOf;
      Translater.DisposeOf;
end;

procedure TVTKForm.FormResize(Sender: TObject);
begin
      Splitter1Resized(Sender);
end;

procedure TVTKForm.IdUDPServer1UDPRead(AThread: TIdUDPListenerThread;
  const AData: TIdBytes; ABinding: TIdSocketHandle);
var
      p, pe : PByte;
      i,c : Integer;
      BufAddr, BufType : String;
      BufValue : T32bitArray;
      BufString : AnsiString;
      LBArr : TArray<String>;

      function ProcessLoopBack(var OSCAddress:String):Integer;
      begin
            Result := -1; // Is loopback, error

            // Loopback packet
            if OSCAddress.StartsWith(LOCALLOOPBACK_ROOT) then
            begin
                  LBArr := OSCAddress.Split(['/']);
                  if (Length(LBArr) = 0) then exit;
                  if StrtoInt64Def(LBArr[High(LBArr)],-1) <> GenerateAccessToken(GlobalUserIDHash, GlobalPassCodeHash, SALTDATE_MOMENT, True) then exit;

                  // Exclude passcode
                  OSCAddress := String.Join('/', Copy(LBArr, 0, Length(LBArr)-1) );

                  Result := 1; // Is loopback, successs
            end else
            begin
                  Result := 0; // Not loopback
            end;
      end;

begin
      // proxy
      try
            if (FUDPPr.Enable) and (IdUDPClient2.Active) and (IdUDPClient2.Connected) then IdUDPClient2.SendBuffer(AData);
      except

      end;

      p := @AData[0]; pe := @AData[High(AData)];
      BufAddr := ''; BufType := '';

      c := 0;
      for i := 0 to Length(AData) - 1 do
      begin
            if p^ = $0 then
            begin
                  case c of
                        0 :
                        begin
                              if PByte(NativeUInt(p)+1)^ <> $0 then Inc(c);
                              Inc(p); continue;
                        end;
                        1 :
                        begin
                              if (((i+1) mod 4) = 0) then Inc(c);
                              Inc(p); continue;
                        end;
                  end;
            end;

            case c of
                  0: BufAddr := BufAddr + Char(AnsiChar(p^));
                  1: BufType := BufType + Char(AnsiChar(p^));
                  2:
                  begin
                        // string only
                        if BufType = ',s' then
                        begin
                              while (p <> pe) do
                              begin
                                    if p^ <> $0 then
                                    begin
                                          if ProcessLoopBack(BufAddr) <> -1 then
                                          begin
                                                BufString := AnsiString(StrPas(PAnsiChar(p)));
                                                KnownParameters.Values[BufAddr] := String(BufString);
                                                FormOSCLogger.ReceiveDataOSC(KnownParameters);
                                          end;

                                          exit;
                                    end;

                                    Inc(p);
                              end;

                              break;
                        end else
                        begin
                              p := @AData[Length(AData)-4];
                              BufValue := P32bitArray(p)^;
                              break;
                        end;
                  end;
            end;

            Inc(p);
      end;

      EndianConv(@BufValue);

      if ProcessLoopBack(BufAddr) = -1 then exit;

      if BufType = ',f' then
      begin
            KnownParameters.Values[BufAddr] := Format('%f', [PSingle(@BufValue)^]);
      end else if BufType = ',i' then
      begin
            KnownParameters.Values[BufAddr] := InttoStr(PInteger(@BufValue)^);
      end else if BufType = ',T' then
      begin
            KnownParameters.Values[BufAddr] := '1';
      end else if BufType = ',F' then
      begin
            KnownParameters.Values[BufAddr] := '0';
      end else
      begin
            KnownParameters.Values[BufAddr] := BufType + '?' + InttoStr(PInteger(@BufValue)^);
      end;

      FormOSCLogger.ReceiveDataOSC(KnownParameters);
end;

procedure TVTKForm.MenuKeyCandClick(Sender: TObject);
begin
      (Sender as TMenuItem).IsChecked := true;
      PassCodeEdit.Text := (Sender as TMenuItem).TagString;
end;

procedure TVTKForm.MenuCellCopyClick(Sender: TObject);
var
      Cell : TExecutionCell;
begin
      if (PopupMenu2.PopupComponent is TCornerButton) then
      begin
            if CellList.TryGetValue( TCornerButton( PopupMenu2.PopupComponent ).Tag, Cell) then Cell.ButtonCopyClick(nil);
      end;
end;

procedure TVTKForm.MenuCellPasteClick(Sender: TObject);
var
      Cell : TExecutionCell;
begin
      if (PopupMenu2.PopupComponent is TCornerButton) then
      begin
            if CellList.TryGetValue( TCornerButton( PopupMenu2.PopupComponent ).Tag, Cell) then Cell.ButtonPasteClick(nil);
      end;
end;

procedure TVTKForm.MenuItemClick(Sender:TObject);
begin
      if Assigned(PopupMenu1.TagObject) then
      begin
            (PopupMenu1.TagObject as TExecutionCell).PresetMenuClickNotify(Sender as TMenuItem);
      end;
end;

procedure TVTKForm.PassCodeEditChange(Sender: TObject);
begin
      GlobalPassCodeHash := SHA1(PassCodeEdit.Text);
      GlobalPassCodeRaw := StrtoIntDef(PassCodeEdit.Text, 0);
end;

procedure TVTKForm.PassCodeEditKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
      PassCodeEditChange(Sender);
end;

procedure TVTKForm.ReleasePressAll(const ExceptBtn:TControl);
var
      i : Integer;
      Btn : TControl;
begin
      for i := 0 to GridPanelLayout1.ChildrenCount-1 do
      begin
            Btn := TControl(GridPanelLayout1.Children.Items[i]);

            if ExceptBtn <> Btn then
            begin
                  if (Btn is TCornerButton) then
                  begin
                        (Btn as TCornerButton).IsPressed := False;
                        (Btn as TCornerButton).StyleLookup := 'CornerButton1Style1';
                  end;
            end;
      end;
end;

procedure TVTKForm.PlotGrid1Click(Sender: TObject);

begin
      TabControl1.ActiveTab := TabControl1.Tabs[0];

      ReleasePressAll(nil);

      //Button1.SetFocus;
      //GlowButton(-1);
end;

procedure TVTKForm.RunExeCell(const CellNumber: Integer; const RawValue:Single = NaN);
var
      Cell : TExecutionCell;
begin
      if CellList.TryGetValue(CellNumber, Cell) then Cell.ExecuteCell(RawValue);
end;

procedure TVTKForm.RunExeCell(const PassCode: Cardinal; const CellNumber: Integer; const InternalValue:Integer = 0);
var
      Source : SmallInt;
      Value : SmallInt;
      i,j : Integer;
      validtoken : Cardinal;
      Cells : TArray<TPair<Integer, TExecutionCell>>;
      Res : Variant;
      aw1, aw2, aw3 : Word;
      note_cc, vel_val : Byte;
      midimcu : TMIDIProcCode;
      NormalizedValue : Single;
      p, q : Integer;
begin
      validtoken := GenerateAccessToken(GlobalUserIDHash, SHA1(PassCodeEdit.Text));

      if (Length(PassCodeEdit.Text) > 0) and (validtoken <> PassCode) then
      begin
            // invalid, reject
            exit;
      end;

      NormalizedValue := 0.0;
      Source := SmallInt(CellNumber and $FFFF);
      Value := SmallInt((CellNumber shr 16) and $FFFF);

      Cells := CellList.ToArray();

      aw1 := SmallInt(InternalValue and $FF); // channel
      aw2 := SmallInt((InternalValue shr 8) and $FF); // message
      aw3 := SmallInt((InternalValue shr 16) and $FFFF); // additional value

      //LogWrap('CELL', 'Received', Format('S=%d, V=(%d,%d) V2=(%d,%d,%d)', [ Source, Value,  InternalValue, LowerWord(Msg.LParam), UpperWord(Msg.LParam), aw1, aw2, aw3]));

      for i := 0 to High(Cells) do
      begin
            Res := Cells[i].Value.QueryRollActive(Source);
            if VarIsNull(Res) then continue;

            case Source of
                  APPSEND_KeyCode:
                  begin
                        //if CompareValue(Value, Single(Res)) = 0 then Cells[i].Value.ExecuteCell(Value/255);
                        if CompareValue(Value, Single(Res)) = 0 then Cells[i].Value.ExecuteCell;
                  end;

                  APPSEND_WindowMessage:
                  begin
                        if (Cells[i].Key = Value) then
                        begin
                              Cells[i].Value.ExecuteCell(Abs(Value/32767));
                              break;
                        end;
                  end;

                  APPSEND_HTTPDEvent:
                  begin
                        if (Cells[i].Key = Value) then
                        begin
                              Cells[i].Value.ExecuteCell;
                              break;
                        end;
                  end;

                  //  aw1 = ch, aw2 = message, aw3 = additional value
                  APPSEND_MIDIIN:
                  begin
                        p := LowerLong(StrtoUInt64Def(Res,0)); q := UpperLong(StrtoUInt64Def(Res,0));

                        note_cc := (Value and $FF);
                        vel_val := ((Value shr 8) and $FF);
                        NormalizedValue := NaN;

                        // MCU mode
                        if (q and MIDISELFLAG_MCU) > 0 then
                        begin
                              midimcu := MIDIMCUNote2LocalCode(note_cc);

                              if (aw2 = MIDICODE_PITCHBEND) then
                              begin
                                    if (FMCUCode[i] <> TMIDIProcCode.mcNull) then NormalizedValue := aw3 / 32767;
                              end else
                              begin
                                    case aw2 of
                                          MIDICODE_CONTROLCHANGE:
                                          begin
                                                if (Byte(midimcu) = LowerWord(q)) then
                                                begin
                                                      NormalizedValue := vel_val / 127;
                                                end;
                                          end;

                                          MIDICODE_NOTEON, MIDICODE_NOTEOFF:
                                          begin
                                                if (Byte(midimcu) = LowerWord(q)) then
                                                begin
                                                      FMCUCode[i] := TMIDIProcCode(TMIDIProcCode.mcNull);

                                                      // press thr
                                                      if (vel_val < $2) then aw2 := MIDICODE_NOTEOFF;

                                                      case aw2 of
                                                            MIDICODE_NOTEON :
                                                            begin
                                                                  // onkeydown
                                                                  FMCUCode[i] := midimcu;

                                                                  // switch on
                                                                  if (midimcu in ([TMIDIProcCode.mcRecCh1 .. TMIDIProcCode.mcResv14] - MIDIPROC_ANALOG)) then NormalizedValue := 1.0;
                                                            end;

                                                            MIDICODE_NOTEOFF:
                                                            begin
                                                                  // onkeyup
                                                                  //if (midimcu in [TMIDIProcCode.mcRecCh1 .. TMIDIProcCode.mcBEATSLED]) then NormalizedValue := 0.0;
                                                            end;
                                                      end;
                                                end;
                                          end;
                                    end;
                              end;

                        // MIDI Normal
                        end else
                        begin
                              if (aw1 = p) then
                              begin
                                    if (vel_val < $2) then aw2 := MIDICODE_NOTEOFF;

                                    //  aw1 = ch, aw2 = message, aw3 = additional value
                                    case aw2 of
                                          MIDICODE_NOTEON:
                                          begin
                                                if ( (q and MIDISELFLAG_NOTE) > 0) and ( note_cc = LowerWord(q) ) then NormalizedValue := 1.0;
                                          end;

                                          //MIDICODE_NOTEOFF : NormalizedValue := 0.0;

                                          MIDICODE_CONTROLCHANGE:
                                          begin
                                                if ( (q and (MIDISELFLAG_CC or MIDISELFLAG_CC_ML) ) > 0) and ( note_cc = LowerWord(q) ) then
                                                      NormalizedValue := vel_val / High(ShortInt);
                                          end;
                                    end;
                              end;
                        end;

                        if IsNan(NormalizedValue) then continue;

                        Cells[i].Value.ExecuteCell(NormalizedValue);
                  end;

                  APPSEND_GamePad:
                  begin
                        p := (Round(Single(Res)) shr 16) and $FFFF; // subtype
                        q := Round(Single(Res)) and $FFFF; // operand

                        case Value of
                              GP_BUTTONS: if ( Cardinal(InternalValue) and (1 shl q) ) > 0 then Cells[i].Value.ExecuteCell;
                              GP_AXIS_1: ;
                              GP_AXIS_2: ;
                              GP_AXIS_3: ;
                        end;
                  end;
            else
                  Cells[i].Value.ExecuteCell;
            end;
      end;

end;

procedure TVTKForm.LoadUserData(bForce:Boolean = False);
var
      AppData : TAppSettings;
      Json : TStringList;
      DeS : TJsonSerializer;
      FileName : String;
      i : Integer;
begin
      Json := TStringList.Create;
      try
            FileName := ChangeFileExt(ParamStr(0),'.config');

            if FileExists(FileName) then
            begin
                  Json.LoadFromFile(FileName,TEncoding.UTF8);

                  DeS := TJsonSerializer.Create;
                  try
                        try
                              AppData := DeS.Deserialize<TAppSettings>( Json.Text );

                              if (AppData.Verify <> UserDataMakeVerifyCode(AppData)) then
                              begin
                                    ShowMessage(LC('設定ファイルの何かが変わりました？'));

                                    // セーフティ
                                    for i := 0 to High(AppData.RecRecords) do
                                    begin
                                          AppData.RecRecords[i].Operation := otNull;
                                    end;
                              end;

                              if (Length(AppData.EnableFunction) > 0) then CheckBox1.IsChecked := AppData.EnableFunction[0];
                              if (Length(AppData.EnableFunction) > 1) then CheckBox2.IsChecked := AppData.EnableFunction[1];
                              if (Length(AppData.EnableFunction) > 2) then CheckBox4.IsChecked := AppData.EnableFunction[2];
                              if (Length(AppData.EnableFunction) > 3) then CheckBox3.IsChecked := AppData.EnableFunction[3];
                              if (Length(AppData.EnableFunction) > 4) then CheckBox5.IsChecked := AppData.EnableFunction[4];

                              i := ComboMIDIInSelecter.Items.IndexOf(AppData.UseDevices.MIDIIn);
                              if i >= 0 then ComboMIDIInSelecter.ItemIndex := i;

                              PassCodeEdit.Text := RightStr(Format('%.06d', [AppData.CryptedPassCode xor StrtoUInt64Def('$' + Copy(NicMacHash, 1, 15), 0)]),6);

                              RestoreCells(AppData.CellRecords);
                              FormOSCLogger.ReceivedEvent := AppData.RecRecords;

                              ApplyLocalize(AppData.UserLocale, false, true);
                              GlobalLocaleID := AppData.UserLocale;
                        except
                        end;
                  finally
                        Des.Free;
                  end;
            end;
      finally
            Json.Free;
      end;
end;

procedure TVTKForm.RestoreCell(const AData:TCellSettings);
var
      ExeCell : TExecutionCell;
begin
      if CellList.TryGetValue(AData.CellNumber, ExeCell) then
      begin
            ExeCell.InitializeCell(nil,nil,AData,nil,nil,nil);
      end;
end;

procedure TVTKForm.RestoreCells(const ADataArray:TArray<TCellSettings>);
var
      i : Integer;
begin
      for i := Low(ADataArray) to High(ADataArray) do RestoreCell(ADataArray[i]);
end;

function TVTKForm.UserDataMakeVerifyCode(const AppData:TAppSettings):String;
var
      JS : TJsonSerializer;
      s : String;
      Dat : TAppSettings;
begin
      JS := TJsonSerializer.Create;
      try
            Dat := AppData;
            JS.Formatting := TJsonFormatting.None;
            Dat.Verify := ''; s := JS.Serialize(Dat);
            Result := SHA1(s + ':' + GlobalUserIDHash);
      finally
            JS.Free;
      end;
end;

procedure TVTKForm.SaveUserData(bForce:Boolean = False);
const
      SAVEREQ_CYCLE = 10;
var
      i : Integer;
      Cells : TArray<TPair<Integer, TExecutionCell>>;
      CellRecords : TArray<TCellSettings>;
      JS : TJsonSerializer;
      Writer : TStringList;
      AppData : TAppSettings;
begin
      if not FIntialized then exit;

      if not bForce then
      begin
            if (FSaveDataCounter mod SAVEREQ_CYCLE) <> 0 then
            begin
                  Inc(FSaveDataCounter);
                  exit;
            end;

            Inc(FSaveDataCounter);
      end;

      Cells := CellList.ToArray();

      SetLength(CellRecords, Length(Cells));
      for i := 0 to High(Cells) do CellRecords[i] := Cells[i].Value.CellSettings;

      JS := TJsonSerializer.Create;

      Writer := TStringList.Create;
      try
            JS.Formatting := TJsonFormatting.Indented;

            AppData.EnableFunction := [CheckBox1.IsChecked, CheckBox2.IsChecked, CheckBox4.IsChecked, CheckBox3.IsChecked, CheckBox5.IsChecked];
            AppData.CryptedPassCode := StrtoUInt64Def(PassCodeEdit.Text,0) xor StrtoUInt64Def('$' + Copy(NicMacHash, 1, 15), 0);
            AppData.UserLocale := GlobalLocaleID;

            AppData.UseDevices.MIDIIn := 'None';
            if Assigned(ComboMIDIInSelecter) and (Assigned(ComboMIDIInSelecter.Selected)) then AppData.UseDevices.MIDIIn := ComboMIDIInSelecter.Selected.Text;

            AppData.CellRecords := CellRecords;
            AppData.RecRecords := FormOSCLogger.ReceivedEvent;
            AppData.Verify := UserDataMakeVerifyCode(AppData);

            Writer.Text := JS.Serialize(AppData);
            Writer.SaveToFile(ChangeFileExt(ParamStr(0),'.config'),TEncoding.UTF8);
      finally
            JS.Free;
            Writer.Free;
      end;

      FSaveDataCounter := 0;
end;

procedure TVTKForm.SpinUDPRPortChange(Sender: TObject);
begin
      FUDPPr.Port := Round((Sender as TSpinBox).Value);
end;

procedure TVTKForm.Splitter1Resized(Sender: TObject);
var
      i : integer;
      CellWidth, CellHeight : Single;
begin
      GridPanelLayout1.BeginUpdate;
      try
            CellWidth := GridPanelLayout1.Width / GridPanelLayout1.ColumnCollection.Count;
            CellHeight := GridPanelLayout1.Height / GridPanelLayout1.RowCollection.Count;

            for i := 0 to GridPanelLayout1.ColumnCollection.Count-1 do
            begin
                  with GridPanelLayout1.ColumnCollection.Items[i] do
                  begin
                        SizeStyle := TGridPanelLayout.TSizeStyle.Absolute;
                        Value := CellWidth;
                  end;
            end;

            for i := 0 to GridPanelLayout1.RowCollection.Count-1 do
            begin
                  with GridPanelLayout1.RowCollection.Items[i] do
                  begin
                        SizeStyle := TGridPanelLayout.TSizeStyle.Absolute;
                        Value := CellHeight;
                  end;
            end;

      finally
            GridPanelLayout1.EndUpdate;
      end;
end;

procedure TVTKForm.SaveTimerTimer(Sender: TObject);
begin
      SaveUserData(True);
end;

procedure TVTKForm.Button1Click(Sender: TObject);
begin
      if not FormOSCLogger.Visible then FormOSCLogger.Show
      else FormOSCLogger.Close;
end;

procedure TVTKForm.Button3Click(Sender: TObject);
var
      PresetDir, PresetFile : String;
      JS : TJsonSerializer;
      CellRecords : TArray<TCellSettings>;
      PresetData : TStringList;
begin
      PresetDir := IncludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0))) + 'Presets\';
      PresetFile := PresetDir + ComboEditPreset.Text + '.json';

      if not FileExists(PresetFile) then
      begin
            ShowMessage(LC('プリセットが存在しません: ') + PresetFile);
            exit;
      end;

      // Loading
      JS := TJsonSerializer.Create;
      PresetData := TStringList.Create;
      try
            PresetData.LoadFromFile(PresetFile, TEncoding.UTF8);
            CellRecords := JS.Deserialize<TArray<TCellSettings>>(PresetData.Text);
            RestoreCells(CellRecords);
      finally
            JS.Free;
            PresetData.Free;
      end;

end;

procedure TVTKForm.Button4Click(Sender: TObject);
var
      PresetDir, PresetFile : String;
      JS : TJsonSerializer;
      i : Integer;
      Cells : TArray<TPair<Integer, TExecutionCell>>;
      CellRecords : TArray<TCellSettings>;
      PresetData : TStringList;
begin
      PresetDir := IncludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0))) + 'Presets\';
      PresetFile := PresetDir + ComboEditPreset.Text + '.json';

      if not TPath.HasValidPathChars(PresetFile, False) then
      begin
            ShowMessage(LC('ファイル名に使用できない文字が含まれています'));
            exit;
      end;

      if not DirectoryExists(PresetDir) and (not ForceDirectories(PresetDir)) then
      begin
            ShowMessage(LC('プリセットフォルダの用意ができません'));
            exit;
      end;

      // Saving
      JS := TJsonSerializer.Create;
      PresetData := TStringList.Create;
      try
            JS.Formatting := TJsonFormatting.Indented;
            Cells := CellList.ToArray();

            SetLength(CellRecords, Length(Cells));
            for i := 0 to High(Cells) do CellRecords[i] := Cells[i].Value.CellSettings;

            PresetData.Text := JS.Serialize(CellRecords);
            PresetData.SaveToFile(PresetFile, TEncoding.UTF8);
      finally
            JS.Free;
            PresetData.Free;
      end;

      // Scan
      ComboEditPresetEnter(ComboEditPreset);
end;

procedure TVTKForm.CheckBox1Change(Sender: TObject);
begin
      VRC10KEYRECEIVER.EnableKeyCode := (Sender as TCheckBox).IsChecked;
end;

procedure TVTKForm.CheckBox2Change(Sender: TObject);
begin
      VRC10KEYRECEIVER.EnableSendMessage := (Sender as TCheckBox).IsChecked;
end;

procedure TVTKForm.CheckBox3Change(Sender: TObject);
begin
      VRC10KEYRECEIVER.EnableNERV := (Sender as TCheckBox).IsChecked;
end;

procedure TVTKForm.CheckBox4Change(Sender: TObject);
begin
      VRC10KEYRECEIVER.EnableHTTPD := (Sender as TCheckBox).IsChecked;
end;

procedure TVTKForm.CheckBox5Change(Sender: TObject);
begin
      VRC10KEYRECEIVER.EnableMIDI := (Sender as TCheckBox).IsChecked;
      ComboMIDIInSelecter.Items.Assign(VRC10KEYRECEIVER.MIDIDevices);
      ComboMIDIInSelecter.Items.InsertObject(0, 'None', TObject(-1));
      ComboMIDIInSelecter.ItemIndex := 0;
end;

procedure TVTKForm.CheckUDPREnableChange(Sender: TObject);
begin
      if Length(FUDPPr.Host) < 2 then
      begin
            // fallback
            EditUDPRHost.Text := '127.0.0.1';
      end;

      if FUDPPr.Port < 1 then
      begin
            // fallback
            SpinUDPRPort.Value := 8999;
      end;

      FUDPPr.Enable := (Sender as TCheckBox).IsChecked;

      EditUDPRHost.Enabled := not FUDPPr.Enable;
      SpinUDPRPort.Enabled := not FUDPPr.Enable;

      if not IdUDPClient2.Active then
      begin
            IdUDPClient2.Host := FUDPPr.Host;
            IdUDPClient2.Port := FUDPPr.Port;
      end;

      IdUDPClient2.Active := FUDPPr.Enable;

      if (not IdUDPClient2.Active) and (IdUDPClient2.Connected) then IdUDPClient2.Disconnect;

      if IdUDPClient2.Active and (not IdUDPClient2.Connected) then IdUDPClient2.Connect;
end;

procedure TVTKForm.ComboMIDIInSelecterChange(Sender: TObject);
var
      i : Integer;
begin
      with (Sender as TComboBox) do
      begin
            if ItemIndex >= 0 then
            begin
                  i := Integer(Items.Objects[ItemIndex]);
                  if i >= 0 then VRC10KEYRECEIVER.MIDISelected := i
            end else
            begin
                  VRC10KEYRECEIVER.MIDISelected := -1;
            end;
      end;
end;

procedure TVTKForm.ComboEditPresetChange(Sender: TObject);
begin
      //ComboEditPresetEnter(Sender);
end;

procedure TVTKForm.ComboEditPresetEnter(Sender: TObject);
var
      PresetDir : String;
      Files : TArray<String>;
      CurrentSelect : String;
      i,j : Integer;
begin
      PresetDir := IncludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0))) + 'Presets\';

      if DirectoryExists(PresetDir) then
      begin
            Files := TDirectory.GetFiles(PresetDir, '*.json', TSearchOption.soAllDirectories);

            CurrentSelect := (Sender as TComboEdit).Text;

            (Sender as TComboEdit).BeginUpdate;
            try
                  (Sender as TComboEdit).Items.Clear;

                  for i := 0 to High(Files) do
                  begin
                        (Sender as TComboEdit).Items.Add( ExtractFileName(ChangeFileExt(Files[i],'')) );
                  end;

                  j := (Sender as TComboEdit).Items.IndexOf(CurrentSelect);
                  if j >= 0 then (Sender as TComboEdit).ItemIndex := j;
            finally
                  (Sender as TComboEdit).EndUpdate;
            end;

      end;
end;

procedure TVTKForm.ComboEditPresetExit(Sender: TObject);
begin
      //ComboEditPresetEnter(Sender);
end;

procedure TVTKForm.ApplyLocalize(const LocaleCode:String='ja-tokyo'; const ResStringOnly:Boolean=False; const ApplySelectMenu:Boolean = False);
var
      langid : String;
      i,j,k : Integer;
      JV : TJsonValue;
      JA : TJsonArray;
      s : String;
      sr : TArray<String>;
      Ch : TArray<TFmxObject>;
      Chs : TStringList;
      Gen : TStringList;
begin
      if (Length(GlobalLocaleID) = 0) or (LocaleCode <> GlobalLocaleID)  then
      begin
            langid := IfThen( Length(LocaleCode) = 0, 'ja-tokyo', LocaleCode);
            GlobalLocaleID := langid;

            if ApplySelectMenu then
            begin
                  for i := 0 to FLangList.Count-1 do
                  begin
                        if CompareText( FLangList.ValueFromIndex[i], GlobalLocaleID ) = 0 then
                        begin
                              j := ComboLang.Items.IndexOf(FLangList.Names[i]);
                              if j >= 0 then ComboLang.ItemIndex := j;

                              break;
                        end;
                  end;
            end;

            if Assigned(Translater) then
            begin
                  Chs := TStringList.Create;
                  Gen := TStringList.Create;
                  try
                        JV := Translater.FindValue('@resourcestring.' + langid);

                        if Assigned(JV) then
                        begin
                              JA := (JV as TJsonArray);

                              // string resource
                              for j := 0 to JA.Count-1 do
                              begin
                                    s := JA.Items[j].ToString().DeQuotedString('"');

                                    // unescape
                                    s := s.Replace('\/','/').Replace('\"','"').Replace('\\','\').Replace('\n',#13#10);

                                    sr := s.Split(['='],2);
                                    if (Length(sr) = 2) and (Length(Trim(sr[1])) > 0) then TranslateDictionary.AddOrSetValue(langid + ':' + sr[0], sr[1]);
                              end;
                        end;

                        if ResStringOnly then exit;

                        // form resource
                        for i := 0 to FFormList.Count-1 do
                        begin
                              JV := Translater.FindValue(FFormList[i].ClassName + '.' + langid);

                              if Assigned(JV) then
                              begin
                                    JA := (JV as TJsonArray);

                                    Chs.Clear; Ch := [];

                                    if (FFormList[i] is TForm) then
                                    begin
                                          Chs.AddObject(FFormList[i].Name + '=' + FFormList[i].ClassName, TObject(FFormList[i]) );

                                          with TForm(FFormList[i]) do
                                                for j := 0 to ComponentCount-1 do Chs.AddObject(Components[j].Name + '=' + Components[j].ClassName , TObject(Components[j]) );

                                    end else if (FFormList[i] is TFrame) then
                                    begin
                                          with TFrame(FFormList[i]) do
                                                for j := 0 to ComponentCount-1 do Chs.AddObject(Components[j].Name + '=' + Components[j].ClassName , TObject(Components[j]) );
                                    end;

                                    for j := 0 to JA.Count-1 do
                                    begin
                                          s := JA.Items[j].ToString().DeQuotedString('"');

                                          // unescape
                                          s := s.Replace('\/','/').Replace('\"','"').Replace('\\','\');

                                          // Controls
                                          if s.StartsWith('@') then
                                          begin
                                                Delete(s,1,1);
                                                sr := s.Split(['=']);

                                                k := chs.IndexOfName(sr[0]);
                                                if (Length(sr) >= 2) and (k >= 0) then
                                                begin
                                                      if Length(sr) > 2 then
                                                            sr[1] := String.Join('=', Copy(sr, 1, Length(sr)) );

                                                      sr[1] := sr[1].Replace('\n',#13#10);

                                                      case IndexText(chs.ValueFromIndex[k], ['TMemo', 'TExpander', 'TLabel', 'TCheckBox', 'TListBoxItem', 'TPopupColumn']) of
                                                            0: TMemo(chs.Objects[k]).Text := sr[1];
                                                            1: TExpander(chs.Objects[k]).Text := sr[1];
                                                            2: TLabel(chs.Objects[k]).Text := sr[1];
                                                            3: TCheckBox(chs.Objects[k]).Text := sr[1];
                                                            4: TListBoxItem(chs.Objects[k]).Text := sr[1];
                                                            5: TPopupColumn(chs.Objects[k]).Items.Text := sr[1];
                                                      else
                                                            if TFmxObject(chs.Objects[k]) is TForm then TForm(chs.Objects[k]).Caption := sr[1];
                                                      end;
                                                end;
                                          end else
                                          begin
                                                if (Gen.IndexOf(s) < 0) and not(s.EndsWith('=')) then Gen.Add(s);
                                          end;

                                    end;

                              end;

                        end;

                        LoadLangFromStrings(Gen);
                  finally
                        Chs.Free;
                        Gen.Free;
                  end;

            end;
      end;

end;

procedure TVTKForm.ComboLangChange(Sender: TObject);
begin
      if (Sender as TComboBox).ItemIndex >= 0 then
      begin
            ApplyLocalize(FLangList.Values[(Sender as TComboBox).Selected.Text]);
      end;
end;

procedure TVTKForm.ClickInspecterClick(Sender:TObject);
begin
      (Sender as TCornerButton).IsPressed := True;
      (Sender as TCornerButton).StyleLookup := 'CornerButton1Style2';
end;

procedure TVTKForm.ClickInspecterMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
      ClickInspecterButton(Sender);
      ReleasePressAll(Sender as TControl);
      (Sender as TCornerButton).StyleLookup := 'CornerButton1Style2';
end;

procedure TVTKForm.CreatePassCodeButtonClick(Sender: TObject);
begin
      TDialogService.MessageDialog(
            LC('パスコードをランダム値で置き換えます。'),TMsgDlgType.mtConfirmation,mbOKCancel,TMsgDlgBtn.mbCancel,0,
            procedure(const AResult: TModalResult)
            var
                  i32a, i32b : Integer;
            begin
                  if AResult = mrOK then
                  begin
                        i32a := RandomRange(Low(Integer), High(Integer));
                        i32b := RandomRange(0, High(Integer));
                        PassCodeEdit.Text := Copy( Format('%u%.08d', [i32a,i32b]), RandomRange(1,6), 6);

                        ShowMessage(LC('パスコードを置き換えました。'));
                  end;
            end
      );
end;

procedure TVTKForm.EditUDPRHostExit(Sender: TObject);
begin
      FUDPPr.Host := (Sender as TEdit).Text;
end;

procedure TVTKForm.EditUDPRHostKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
      EditUDPRHostExit(Sender);
end;

procedure TVTKForm.ClickInspecterButton(Sender:TObject);
begin
      TabControl1.ActiveTab := TabControl1.Tabs[(Sender as TControl).Tag + 1];
end;

procedure TVTKForm.CellChangeEvent(Sender:TObject);
var
      Cell : TExecutionCell;
      Btn : TCornerButton;
begin
      Cell := (Sender as TExecutionCell) as TExecutionCell;
      Btn := (Cell.TagObject as TCornerButton);

      Btn.Text := Cell.CellSettings.CellLabel;
      TLabel(Btn.TagObject).Text := Cell.RollEdit.Text;
      //;

      GlobalCellLabel[Cell.CellNumber] := Btn.Text;

      SaveUserData();
end;

procedure TVTKForm.CellExecuteAnimationEvent(Sender:TObject);
begin
      TRectangle((Sender as TFloatAnimation).Parent).Visible := False;
end;

procedure TVTKForm.CellExecuteEvent(Sender:TObject; CellIndex:Integer);
var
      Cell : TExecutionCell;
      Btn : TCornerButton;
      Rct : TRectangle;
      FA : TFloatAnimation;
      i : Integer;
      tv : Variant;
begin
      Cell := (Sender as TExecutionCell) as TExecutionCell;
      Btn := (Cell.TagObject as TCornerButton);

      // limit
      tv := (Cell.TagExVar['btn.activesign.time']);
      if (not VarIsNull(tv)) and ((GlobalTimer.ElapsedMilliseconds - Int64(tv)) < 50) then exit;

      // Interactive Create
      Btn.BeginUpdate;
      try
            Rct := nil;
            for i := 0 to Btn.ChildrenCount-1 do
            begin
                  if String(Btn.Children.Items[i].Name).StartsWith('ExecuteOverlay') then
                  begin
                        Rct := TRectangle(Btn.Children.Items[i]);
                  end;
            end;

            if not Assigned(Rct) then
            begin
                  Rct := TRectangle.Create(Btn);
                  Rct.Parent := Btn; Rct.Align := TAlignLayout.Client;
                  Rct.Fill.Color := $FFF3A4D2;
                  Rct.Stroke.Kind := TBrushKind.None;
                  Rct.Name := 'ExecuteOverlay' + CellIndex.ToString();
                  FA := TFloatAnimation.Create(Btn);
                  FA.Enabled := False;
                  FA.Parent := Rct;
                  FA.PropertyName := 'Opacity';
                  FA.StartValue := 0.5;
                  FA.StopValue := 0.0;
                  FA.Duration := 1.0;
                  FA.Loop := False;
                  FA.OnFinish := CellExecuteAnimationEvent;
                  Rct.TagObject := TObject(FA);
            end else
            begin
                  FA := TFloatAnimation(Rct.TagObject);
            end;

            if not FA.Running then
            begin
                  FA.Stop;
                  Rct.Visible := True;
                  Rct.Opacity := FA.StartValue;
            end else
            begin
                  FA.Stop; FA.Start;
            end;

      finally
            Btn.EndUpdate;
            Btn.Repaint;
      end;

      Cell.TagExVar['btn.activesign.time'] := GlobalTimer.ElapsedMilliseconds;
      FA.Start;


end;

procedure TVTKForm.RecChangeEvent(Sender:TObject);
begin
      SaveUserData();
end;

procedure TVTKForm.ClearOSCMenuByDefines();
begin
      PopupMenu1.CloseMenu;
      PopupMenu1.Clear;
end;

procedure TVTKForm.ReScanOSCMenuByDefine(Sender:TObject);
begin
      ClearOSCMenuByDefines();
      ScanOSCDefines();
end;

procedure TVTKForm.OpenOSCDirectoryShell(Sender:TObject);
begin
      if DirectoryExists(GlobalOSCDirectory) then
      begin
            {$IFDEF MSWINDOWS}
            ShellExecute(
                  WindowHandleToPlatform(Handle).Wnd,
                  'open',
                  'explorer',
                  PWideChar('"'+GlobalOSCDirectory+'"'),
                  nil,
                  SW_SHOW
            );
            {$ENDIF}
      end;
end;

procedure TVTKForm.ScanOSCDefines();
var
      OSCDir : String;
      OSCFiles : TArray<String>;
      OSCStdFile : String;
      Json : TStringList;
      JsonRec : TVRCOSCJson;
      Menu, ChildMenu : TMenuItem;
      Dic : TStringList;
      DeS : TJsonSerializer;
      i,j : Integer;
begin
      OSCDir := GlobalOSCDirectory;

      if DirectoryExists(OSCDir) then
      begin
            OSCFiles := TDirectory.GetFiles(OSCDir, '*.json', TSearchOption.soAllDirectories);

            OSCStdFile := IncludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0))) + 'avtr_standard.json';
            if FileExists(OSCStdFile) then SetLength(OSCFiles, Length(OSCFiles)+1);  OSCFiles[High(OSCFiles)] := OSCStdFile;

            // jsonスキャンしてプリセット投入
            Dic := TStringList.Create;
            try
                  Dic.Sorted := true;

                  for i := 0 to High(OSCFiles) do
                  begin
                        if (ExtractFileExt(OSCFiles[i]) = '.json') and (ExtractFileName(OSCFiles[i]).StartsWith('avtr_') ) then
                        begin
                              Json := TStringList.Create;
                              try
                                    Json.LoadFromFile(OSCFiles[i], TEncoding.UTF8);

                                    DeS := TJsonSerializer.Create;
                                    try
                                          try
                                                JsonRec := DeS.Deserialize<TVRCOSCJson>( Json.Text );
                                          except
                                                continue;
                                          end;

                                          j := Dic.IndexOf(JsonRec.id);
                                          if j >= 0 then Menu := TMenuItem(Dic.Objects[j])
                                          else
                                          begin
                                                Menu := TMenuItem.Create(PopupMenu1);
                                                Menu.Parent := PopupMenu1;
                                                Menu.Text := JsonRec.name;
                                                Dic.AddObject(JsonRec.id, Menu);
                                          end;

                                          for j := 0 to High(JsonRec.parameters) do
                                          begin
                                                if Length(JsonRec.parameters[j].input.address) = 0 then continue;

                                                ChildMenu := TMenuItem.Create(PopupMenu1);
                                                ChildMenu.Parent := Menu;

                                                with JsonRec.parameters[j] do
                                                begin
                                                      ChildMenu.Text := name;
                                                      ChildMenu.TagString := String.join(#13#10,['name='+name, 'iaddr='+input.address, 'itype='+input.vartype, 'oaddr='+output.address, 'otype='+output.vartype]);
                                                      ChildMenu.OnClick := MenuItemClick;
                                                end;
                                          end;

                                    finally

                                    end;
                              finally
                                    Json.Free;
                              end;
                        end;
                  end;

                  // Separator
                  Menu := TMenuItem.Create(PopupMenu1);
                  Menu.Parent := PopupMenu1; Menu.Text := '-'; Menu.Enabled := False;

                  Menu := TMenuItem.Create(PopupMenu1);
                  Menu.Parent := PopupMenu1;
                  Menu.TagString := String.join(#13#10,['name='+LC('やまびこアドレス'), 'iaddr='+LOCALLOOPBACK_ROOT+'hogehoge', 'itype=Float', 'oaddr='+LOCALLOOPBACK_ROOT+'hogehoge', 'otype=Float']);
                  Menu.Text := LC('やまびこアドレス');
                  Menu.OnClick := MenuItemClick;

                  // Separator
                  Menu := TMenuItem.Create(PopupMenu1);
                  Menu.Parent := PopupMenu1; Menu.Text := '-'; Menu.Enabled := False;

                  Menu := TMenuItem.Create(PopupMenu1);
                  Menu.Parent := PopupMenu1;
                  Menu.Text := LC('定義を再スキャン');
                  Menu.OnClick := ReScanOSCMenuByDefine;

                  {$IFDEF MSWINDOWS}
                  Menu := TMenuItem.Create(PopupMenu1);
                  Menu.Parent := PopupMenu1;
                  Menu.Text := LC('エクスプローラで開く');
                  Menu.OnClick := OpenOSCDirectoryShell;
                  {$ENDIF}

            finally
                  Dic.Free;
            end;
      end;
end;

procedure TVTKForm.InitLocalize();
var
      i : Integer;
      TranslateJson : TStringList;
      TranslateJsonPath : String;
      JO : TJsonObject;
begin
      TranslateJson := TStringList.Create;
      try
            TranslateJsonPath := ChangeFileExt(ParamStr(0),'.lng.json');
            if FileExists(TranslateJsonPath) then
            begin
                  TranslateJson.LoadFromFile(TranslateJsonPath, TEncoding.UTF8);
                  Translater := TJsonObject.ParseJSONValue(TranslateJson.Text) as TJsonObject;

                  if not Assigned(Translater) or not Assigned(Translater.Values['langlist']) then
                  begin
                        ShowMessage('syntax error at Multi-Language file.');
                        Translater := nil;
                        exit;
                  end;

                  JO := (Translater.Values['langlist'] as TJsonObject);

                  for i := 0 to JO.Count-1 do
                  begin
                        if not JO.Pairs[i].JsonString.Value.StartsWith('#') then
                              FLangList.Add( JO.Pairs[i].JsonValue.Value + '=' + JO.Pairs[i].JsonString.Value );
                  end;

                  Translater := (Translater.Values['translations'] as TJsonObject);
            end;
      finally
            TranslateJson.Free;
      end;
end;

procedure TVTKForm.FormCreate(Sender: TObject);
var
      Cell : TExecutionCell;
      x,y,i : Integer;
      Def_T : Array [1..4, 1..4] of Word;
      MacAddr : TStringList;
      TabItem : TTabItem;
      Btn : TCornerButton;
      DetLabel : TLabel;
begin
      (* Localize Init *)
      Translater := nil;
      FLangList := TStringList.Create;
      FFormList := TList<TFmxObject>.Create;
      FFormList.Add(Self);

      GlobalLocaleID := 'ja-tokyo';
      InitLocalize();
      ApplyLocalize(GlobalLocaleID, true, true);


      (* Global Init *)
      KnownParameters := TStringList.Create;
      CellList := TDictionary<Integer, TExecutionCell>.Create;

      ZeroMemory(@FMCUCode[0], sizeof(FMCUCode));
      SetLength(GlobalCellLabel, 64);

      LabelVersion.Text := VERSIONSTRING;

      {$IFDEF WIN64}
            Self.Caption := Self.Caption + ' (64bit)';
            LabelVersion.Text := LabelVersion.Text + ' (64bit)';
      {$ENDIF}

      (* End of Global Init *)

      // Default key-bind
      Def_T[1][1] := VK_TAB;        Def_T[1][2] := VK_DIVIDE;     Def_T[1][3] := VK_MULTIPLY;   Def_T[1][4] := VK_SUBTRACT;
      Def_T[2][1] := VK_NUMPAD7;    Def_T[2][2] := VK_NUMPAD8;    Def_T[2][3] := VK_NUMPAD9;    Def_T[2][4] := VK_ADD;
      Def_T[3][1] := VK_NUMPAD4;    Def_T[3][2] := VK_NUMPAD5;    Def_T[3][3] := VK_NUMPAD6;    Def_T[3][4] := VK_DECIMAL;
      Def_T[4][1] := VK_NUMPAD1;    Def_T[4][2] := VK_NUMPAD2;    Def_T[4][3] := VK_NUMPAD3;    Def_T[4][4] := VK_NUMPAD0;

      // OSC Dir
      GlobalOSCDirectory := TPath.GetFullPath( IncludeTrailingPathDelimiter(TPath.GetHomePath) + '..\LocalLow\VRChat\VRChat\OSC');

      // get local MAC Addresses and calc Hash, for crypt seed
      MacAddr := TStringList.Create;
      try
            // w/o virutal adapter
            GetLocalMacAddress(MacAddr,['00-05-69','00-0C-29','00-1C-14','00-1C-42','00-50-56','00-15-5D','08-00-27']);
            MacAddr.Sort;
            NicMacHash := SHA1(MacAddr.Text);
      finally
            MacAddr.Free;
      end;

      // make user-ident
      GlobalUserIDHash := SHA1(InttoStr(GetSteamUserIdent()));

      // setup user-passcode
      PassCodeEditChange(PassCodeEdit);

      Randomize;

      i := 0;
      for y := 1 to 4 do
      begin
            for x := 1 to 4 do
            begin
                  TabItem := TabControl1.Add;

                  Cell := TExecutionCell.Create(Self, 'Cell' + i.Tostring());

                  if i = 0 then
                  begin
                        Cell.InitializeCell(
                                    TControl(Self),
                                    TabItem,
                                    i,
                                    [
                                          CellRoll(( APPSEND_MastodonNerv ), Null )
                                    ],
                                    'Preset ' + Chr(Ord('A')+i),
                                    '/avatar/parameters/',
                                    'Int',
                                    ossFixed,
                                    [0,1,1],
                                    IdUDPClient1,
                                    PopupMenu1,
                                    KnownParameters
                        );
                  end else
                  begin
                        Cell.InitializeCell(
                                    TControl(Self),
                                    TabItem,
                                    i,
                                    [
                                          CellRoll(( APPSEND_Keycode ), Def_T[y][x] ),
                                          CellRoll(( APPSEND_HttpdEvent ), Null ),
                                          CellRoll(( APPSEND_MIDIIn ), UInttoStr( MakeUINT64( Byte(TMIDIProcCode.mcSoloCh1) + (i-1), MIDISELFLAG_MCU )))
                                    ],
                                    'Preset ' + Chr(Ord('A')+i),
                                    '/avatar/parameters/',
                                    'Int',
                                    ossFixed,
                                    [0,1,1],
                                    IdUDPClient1,
                                    PopupMenu1,
                                    KnownParameters
                        );
                  end;

                  FFormList.Add(Cell);

                  // make button pad
                  Btn := TCornerButton.Create(Self);
                  Btn.OnClick := ClickInspecterClick;
                  Btn.OnMouseDown := ClickInspecterMouseDown;
                  //Btn.Parent := GridPanelLayout1;

                  Btn.Align := TAlignLayout.Client;
                  Btn.StaysPressed := True;
                  Btn.StyleLookup := 'CornerButton1Style1';
                  Btn.Margins.Left := 5;
                  Btn.Margins.Top := 10;
                  Btn.Margins.Bottom := 10;
                  Btn.Margins.Right := 5;
                  Btn.Tag := i;
                  Btn.PopupMenu := PopupMenu2;
                  //Btn.TagObject := TObject(Cell);

                  // make sublabel in buton
                  DetLabel := TLabel.Create(Self);
                  DetLabel.Parent := Btn;
                  DetLabel.TextSettings.HorzAlign := TTextAlign.Center;
                  DetLabel.Align := TAlignLayout.Bottom;
                  DetLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.FontColor];
                  DetLabel.TextSettings.Font.Size := 9;
                  DetLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
                  DetLabel.Text := '';
                  Btn.TagObject := TObject(DetLabel);

                  // finalize
                  Cell.OnCellChange := CellChangeEvent;
                  Cell.OnCellExecute := CellExecuteEvent;
                  Cell.TagObject := TObject(Btn);
                  Cell.Align := TAlignLayout.Client;
                  Cell.Margins.Left := 5;
                  Cell.Margins.Top := 5;
                  Cell.Margins.Bottom := 5;
                  Cell.Margins.Right := 10;


                  CellList.Add(i, Cell);

                  Btn.Tag := i; Btn.Parent := GridPanelLayout1;
                  GridPanelLayout1.ControlCollection.AddControl(Btn,x-1,y-1);

                  (*with  do
                  begin
                        Control := Btn;
                        Btn.Tag := i;
                        Column := x-1;
                        Row := y-1;
                  end;*)



                  Inc(i);
            end;
      end;

      for i := 0 to FLangList.Count-1 do ComboLang.Items.Add(FLangList.Names[i]);

      TThread.CreateAnonymousThread(procedure()
      begin
            try
                  ScanOSCDefines(); // delayed scan
            except
            end;

            while true do
            begin
                  if (Assigned(FormOSCLogger) and FormOSCLogger.FormReady) then
                  begin
                        if Assigned(VRC10KEYRECEIVER) and VRC10KEYRECEIVER.FormReady then
                        begin
                              Sleep(100);

                              TThread.Queue(nil,procedure()
                              var
                                    Cells : TArray<TPair<Integer, TExecutionCell>>;
                                    Cell : TExecutionCell;
                                    Btn : TCornerButton;
                                    i : Integer;
                              begin
                                    GlobalLocaleID := 'ja-tokyo';
                                    LoadUserData();

                                    CheckBox1Change(CheckBox1);
                                    CheckBox2Change(CheckBox2);
                                    CheckBox3Change(CheckBox3);
                                    CheckBox4Change(CheckBox4);

                                    ComboEditPresetEnter(ComboEditPreset);

                                    PassCodeEditChange(PasscodeEdit);

                                    FormResize(VTKForm);

                                    PlotGrid1.SetFocus;
                                    FormOSCLogger.RecSettingOnChange := RecChangeEvent;


                                    Intialized := True;

                                    Cells := CellList.ToArray();

                                    for i := 0 to High(Cells) do
                                    begin
                                          Cell := Cells[i].Value;
                                          Btn := (Cell.TagObject as TCornerButton);
                                          Btn.Text := Cell.CellSettings.CellLabel;
                                          TLabel(Btn.TagObject).Text := Cell.RollEdit.Text;
                                          GlobalCellLabel[Cell.CellNumber] := Btn.Text;
                                    end;
                              end);

                              exit;
                        end;

                  end;

                  Sleep(100);
            end;
      end).Start;

      FormOSCLogger := TFormOSCLogger.Create(nil);
      VRC10KEYRECEIVER := TVRC10KEYRECEIVER.Create(nil);

      FFormList.Add(FormOSCLogger);
      FFormList.Add(VRC10KEYRECEIVER);

end;

end.
