﻿unit AppInclude;

interface

uses
  {$IFDEF MSWINDOWS} Winapi.Windows, Winapi.Messages, FMX.Platform.Win, Winapi.Nb30, {$ENDIF}
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.TypInfo, System.JSON.Serializers, System.Generics.Collections,
  System.Diagnostics;

const
  VERSIONSTRING = '0.35b';

  //--------------- バージョン履歴メモ -----------------
  // 2022.3. 8    0.20b プレビューリリース
  // 2022.3.25    0.30b 一般向けリリース
  // 2022.4.17    0.35b 一般向けリリース April
  //
  //
  //
  //
  //----------------------------------------------------

  { Unity / VRC Typename assign }
  VRCOSCType_Int = 'Int';
  VRCOSCType_Bool = 'Bool';
  VRCOSCType_Float = 'Float';
  VRCOSCType_None = 'None';

  { Internal Source code }
  APPSEND_Default       =   0;
  APPSEND_Keycode       =   -1;
  APPSEND_HttpdEvent    =   -2;
  APPSEND_WindowMessage =   -3;
  APPSEND_MastodonNerv  =   -4;
  APPSEND_MIDIIN        =   -5;
  APPSEND_GamePad       =   -6;
  APPSEND_Reserved1     =   -7;
  APPSEND_Reserved2     =   -8;
  APPSEND_Reserved3     =   -9;
  APPSEND_Reserved4     =   -10;
  APPSEND_Reserved5     =   -11;

  MIDISELFLAG_NOTE = $00010000;
  MIDISELFLAG_CC = $00020000;
  MIDISELFLAG_CC_ML = $00040000;
  MIDISELFLAG_MCU = $00080000;

  GP_NONE = 0;
  GP_BUTTONS = 1;
  GP_AXIS_1 = 2;
  GP_AXIS_2 = 3;
  GP_AXIS_3 = 4;

  LOCALLOOPBACK_ROOT = '/oscvrc/';
  LOCALLOOPBACK_CELLCHAIN = LOCALLOOPBACK_ROOT + 'cell/';
  LOCALLOOPBACK_KEYPRESS = LOCALLOOPBACK_ROOT + 'key/';

  SALTDATE_RAMUNE  = 'yyyymm';
  SALTDATE_WEAK    = 'yyyymmdd';
  SALTDATE_NORMAL  = 'yyyymmddhh';
  SALTDATE_MEDIUM  = 'yyyymmddhhnn';
  SALTDATE_MOMENT  = 'yyyymmddhhnnss';

  SALTDATE_DEFAULT = SALTDATE_NORMAL;

  { MIDI Extend Messages }
  MIDICODE_NOTEOFF = $8;
  MIDICODE_NOTEON = $9;
  MIDICODE_POLYPHONIC_AFTERTOUCH = $A;
  MIDICODE_CONTROLCHANGE = $B;
  MIDICODE_PROGRAMCHANGE = $C;
  MIDICODE_CHANNEL_AFTERTOUCH = $D;
  MIDICODE_PITCHBEND = $E;
  MIDICODE_SYSEX = $F;

  { MIDI Notes / Ordinal }
  note_Cm1 = 0;
  note_Cdm1 = 1;
  note_Dm1 = 2;
  note_Ddm1 = 3;
  note_Em1 = 4;
  note_Fm1 = 5;
  note_Fdm1 = 6;
  note_Gm1 = 7;
  note_Gdm1 = 8;
  note_Am1 = 9;
  note_Adm1 = 10;
  note_Bm1 = 11;

  note_C_0 = note_Cm1  + 12;
  note_Cd_0 = note_Cdm1 + 12;
  note_D_0 = note_Dm1  + 12;
  note_Dd_0 = note_Ddm1 + 12;
  note_E_0 = note_Em1  + 12;
  note_F_0 = note_Fm1  + 12;
  note_Fd_0 =note_Fdm1 + 12;
  note_G_0 = note_Gm1  + 12;
  note_Gd_0 = note_Gdm1 + 12;
  note_A_0 = note_Am1  + 12;
  note_Ad_0 = note_Adm1 + 12;
  note_B_0 = note_Bm1  + 12;

  note_C_1 = note_C_0  + 12;
  note_Cd_1 = note_Cd_0  + 12;
  note_D_1 = note_D_0  + 12;
  note_Dd_1 = note_Dd_0  + 12;
  note_E_1 = note_E_0  + 12;
  note_F_1 = note_F_0  + 12;
  note_Fd_1 = note_Fd_0  + 12;
  note_G_1 = note_G_0  + 12;
  note_Gd_1 = note_Gd_0  + 12;
  note_A_1 = note_A_0  + 12;
  note_Ad_1 = note_Ad_0  + 12;
  note_B_1 = note_B_0  + 12;

  note_C_2 = note_C_1  + 12;
  note_Cd_2 = note_Cd_1  + 12;
  note_D_2 = note_D_1  + 12;
  note_Dd_2 = note_Dd_1  + 12;
  note_E_2 = note_E_1  + 12;
  note_F_2 = note_F_1  + 12;
  note_Fd_2 = note_Fd_1  + 12;
  note_G_2 = note_G_1  + 12;
  note_Gd_2 = note_Gd_1  + 12;
  note_A_2 = note_A_1  + 12;
  note_Ad_2 = note_Ad_1  + 12;
  note_B_2 = note_B_1  + 12;

  note_C_3 = note_C_2  + 12;
  note_Cd_3 = note_Cd_2  + 12;
  note_D_3 = note_D_2  + 12;
  note_Dd_3 = note_Dd_2  + 12;
  note_E_3 = note_E_2  + 12;
  note_F_3 = note_F_2  + 12;
  note_Fd_3 = note_Fd_2  + 12;
  note_G_3 = note_G_2  + 12;
  note_Gd_3 = note_Gd_2  + 12;
  note_A_3 = note_A_2  + 12;
  note_Ad_3 = note_Ad_2  + 12;
  note_B_3 = note_B_2  + 12;

  note_C_4 = note_C_3  + 12;
  note_Cd_4 = note_Cd_3  + 12;
  note_D_4 = note_D_3  + 12;
  note_Dd_4 = note_Dd_3  + 12;
  note_E_4 = note_E_3  + 12;
  note_F_4 = note_F_3  + 12;
  note_Fd_4 = note_Fd_3  + 12;
  note_G_4 = note_G_3  + 12;
  note_Gd_4 = note_Gd_3  + 12;
  note_A_4 = note_A_3  + 12;
  note_Ad_4 = note_Ad_3  + 12;
  note_B_4 = note_B_3  + 12;

  note_C_5 = note_C_4  + 12;
  note_Cd_5 = note_Cd_4  + 12;
  note_D_5 = note_D_4  + 12;
  note_Dd_5 = note_Dd_4  + 12;
  note_E_5 = note_E_4  + 12;
  note_F_5 = note_F_4  + 12;
  note_Fd_5 = note_Fd_4  + 12;
  note_G_5 = note_G_4  + 12;
  note_Gd_5 = note_Gd_4  + 12;
  note_A_5 = note_A_4  + 12;
  note_Ad_5 = note_Ad_4  + 12;
  note_B_5 = note_B_4  + 12;

  note_C_6 = note_C_5  + 12;
  note_Cd_6 = note_Cd_5  + 12;
  note_D_6 = note_D_5  + 12;
  note_Dd_6 = note_Dd_5  + 12;
  note_E_6 = note_E_5  + 12;
  note_F_6 = note_F_5  + 12;
  note_Fd_6 = note_Fd_5  + 12;
  note_G_6 = note_G_5  + 12;
  note_Gd_6 = note_Gd_5  + 12;
  note_A_6 = note_A_5  + 12;
  note_Ad_6 = note_Ad_5  + 12;
  note_B_6 = note_B_5  + 12;

  note_C_7 = note_C_6  + 12;
  note_Cd_7 = note_Cd_6  + 12;
  note_D_7 = note_D_6  + 12;
  note_Dd_7 = note_Dd_6  + 12;
  note_E_7 = note_E_6  + 12;
  note_F_7 = note_F_6  + 12;
  note_Fd_7 = note_Fd_6  + 12;
  note_G_7 = note_G_6  + 12;
  note_Gd_7 = note_Gd_6  + 12;
  note_A_7 = note_A_6  + 12;
  note_Ad_7 = note_Ad_6  + 12;
  note_B_7 = note_B_6  + 12;

  note_C_8 = note_C_7  + 12;
  note_Cd_8 = note_Cd_7  + 12;
  note_D_8 = note_D_7  + 12;
  note_Dd_8 = note_Dd_7  + 12;
  note_E_8 = note_E_7  + 12;
  note_F_8 = note_F_7  + 12;
  note_Fd_8 = note_Fd_7  + 12;
  note_G_8 = note_G_7  + 12;
  note_Gd_8 = note_Gd_7  + 12;
  note_A_8 = note_A_7  + 12;
  note_Ad_8 = note_Ad_7  + 12;
  note_B_8 = note_B_7  + 12;

type

  TVRCOSCJsonParameterIO = record
      address : String;
      [JsonName('type')]
      vartype : String;
  end;

  TVRCOSCJsonParameter = record
      name : String;
      input : TVRCOSCJsonParameterIO;
      output : TVRCOSCJsonParameterIO;
  end;

  TVRCOSCJson = record
      id : String;
      name : String;
      parameters : TArray<TVRCOSCJsonParameter>
  end;

  { MIDI Notes / Set }
  TMIDINote = (
      mnCm1=note_Cm1,mnCdm1=note_Cdm1,mnDm1=note_Dm1,mnDdm1=note_Ddm1,mnEm1=note_Em1,mnFm1=note_Fm1,mnFdm1=note_Fdm1,mnGm1=note_Gm1,mnGdm1=note_Gdm1,mnAm1=note_Am1,mnAdm1=note_Adm1,mnBm1=note_Bm1,
      mnC_0=note_C_0,mnCd_0=note_Cd_0,mnD_0=note_D_0,mnDd_0=note_Dd_0,mnE_0=note_E_0,mnF_0=note_F_0,mnFd_0=note_Fd_0,mnG_0=note_G_0,mnGd_0=note_Gd_0,mnA_0=note_A_0,mnAd_0=note_Ad_0,mnB_0=note_B_0,
      mnC_1=note_C_1,mnCd_1=note_Cd_1,mnD_1=note_D_1,mnDd_1=note_Dd_1,mnE_1=note_E_1,mnF_1=note_F_1,mnFd_1=note_Fd_1,mnG_1=note_G_1,mnGd_1=note_Gd_1,mnA_1=note_A_1,mnAd_1=note_Ad_1,mnB_1=note_B_1,
      mnC_2=note_C_2,mnCd_2=note_Cd_2,mnD_2=note_D_2,mnDd_2=note_Dd_2,mnE_2=note_E_2,mnF_2=note_F_2,mnFd_2=note_Fd_2,mnG_2=note_G_2,mnGd_2=note_Gd_2,mnA_2=note_A_2,mnAd_2=note_Ad_2,mnB_2=note_B_2,
      mnC_3=note_C_3,mnCd_3=note_Cd_3,mnD_3=note_D_3,mnDd_3=note_Dd_3,mnE_3=note_E_3,mnF_3=note_F_3,mnFd_3=note_Fd_3,mnG_3=note_G_3,mnGd_3=note_Gd_3,mnA_3=note_A_3,mnAd_3=note_Ad_3,mnB_3=note_B_3,
      mnC_4=note_C_4,mnCd_4=note_Cd_4,mnD_4=note_D_4,mnDd_4=note_Dd_4,mnE_4=note_E_4,mnF_4=note_F_4,mnFd_4=note_Fd_4,mnG_4=note_G_4,mnGd_4=note_Gd_4,mnA_4=note_A_4,mnAd_4=note_Ad_4,mnB_4=note_B_4,
      mnC_5=note_C_5,mnCd_5=note_Cd_5,mnD_5=note_D_5,mnDd_5=note_Dd_5,mnE_5=note_E_5,mnF_5=note_F_5,mnFd_5=note_Fd_5,mnG_5=note_G_5,mnGd_5=note_Gd_5,mnA_5=note_A_5,mnAd_5=note_Ad_5,mnB_5=note_B_5,
      mnC_6=note_C_6,mnCd_6=note_Cd_6,mnD_6=note_D_6,mnDd_6=note_Dd_6,mnE_6=note_E_6,mnF_6=note_F_6,mnFd_6=note_Fd_6,mnG_6=note_G_6,mnGd_6=note_Gd_6,mnA_6=note_A_6,mnAd_6=note_Ad_6,mnB_6=note_B_6,
      mnC_7=note_C_7,mnCd_7=note_Cd_7,mnD_7=note_D_7,mnDd_7=note_Dd_7,mnE_7=note_E_7,mnF_7=note_F_7,mnFd_7=note_Fd_7,mnG_7=note_G_7,mnGd_7=note_Gd_7,mnA_7=note_A_7,mnAd_7=note_Ad_7,mnB_7=note_B_7,
      mnC_8=note_C_8,mnCd_8=note_Cd_8,mnD_8=note_D_8,mnDd_8=note_Dd_8,mnE_8=note_E_8,mnF_8=note_F_8,mnFd_8=note_Fd_8,mnG_8=note_G_8,mnGd_8=note_Gd_8,mnA_8=note_A_8,mnAd_8=note_Ad_8,mnB_8=note_B_8
  );
  TMIDINotes = set of TMIDINote;

  { MIDI CC / Set}
  TMIDICtrlChange = (
      ccBankSelect1_ML = 0,
      ccModulationWheel = 1,
      ccBreathController = 2,
      cc3 = 3,
      ccFootController = 4,
      ccPortamentoTime = 5,
      ccDataEntry1_ML = 6,
      ccChannelVolume = 7,
      cc8 = 8,
      cc9 = 9,
      ccPan = 10,
      cc11 = 11,
      cc12 = 12,
      cc13 = 13,
      cc14 = 14,
      cc15 = 15,
      cc16 = 16,
      cc17 = 17,
      cc18 = 18,
      cc19 = 19,
      cc20 = 20,
      cc21 = 21,
      cc22 = 22,
      cc23 = 23,
      cc24 = 24,
      cc25 = 25,
      cc26 = 26,
      cc27 = 27,
      cc28 = 28,
      cc29 = 29,
      cc30 = 30,
      cc31 = 31,
      ccBankSelect2_ML = 32,
      cc33 = 33,
      cc34 = 34,
      cc35 = 35,
      cc36 = 36,
      cc37 = 37,
      ccDataEntry2_ML = 38,
      cc39 = 39,
      cc40 = 40,
      cc41 = 41,
      cc42 = 42,
      cc43 = 43,
      cc44 = 44,
      cc45 = 45,
      cc46 = 46,
      cc47 = 47,
      cc48 = 48,
      cc49 = 49,
      cc50 = 50,
      cc51 = 51,
      cc52 = 52,
      cc53 = 53,
      cc54 = 54,
      cc55 = 55,
      cc56 = 56,
      cc57 = 57,
      cc58 = 58,
      cc59 = 59,
      cc60 = 60,
      cc61 = 61,
      cc62 = 62,
      cc63 = 63,
      ccDamperPedal_TG = 64,
      ccPortamento_TG = 65,
      ccSostenuto_TG = 66,
      ccSoftPedal_TG = 67,
      cc68 = 68,
      cc69 = 69,
      ccSound_Variation = 70,
      ccTimbre_Harmonic_Intensity = 71,
      ccReleaseTime = 72,
      ccAttackTime = 73,
      ccBrightness = 74,
      ccDecayTime = 75,
      ccVibratoRate = 76,
      ccVibratoDepth = 77,
      ccVibratoDelay = 78,
      cc79 = 79,
      cc80 = 80,
      cc81 = 81,
      cc82 = 82,
      cc83 = 83,
      cc84 = 84,
      cc85 = 85,
      cc86 = 86,
      cc87 = 87,
      cc88 = 88,
      cc89 = 89,
      cc90 = 90,
      ccEffect1_Reverb = 91,
      ccEffect2_Tremolo = 92,
      ccEffect3_Chorus = 93,
      ccEffect4_DDetune = 94,
      ccEffect5_Phaser = 95,
      cc96 = 96,
      cc97 = 97,
      cc98 = 98,
      cc99 = 99,
      ccRegistParam1_ML = 100,
      ccRegistParam2_ML = 101,
      cc102 = 102,
      cc103 = 103,
      cc104 = 104,
      cc105 = 105,
      cc106 = 106,
      cc107 = 107,
      cc108 = 108,
      cc109 = 109,
      cc110 = 110,
      cc111 = 111,
      cc112 = 112,
      cc113 = 113,
      cc114 = 114,
      cc115 = 115,
      cc116 = 116,
      cc117 = 117,
      cc118 = 118,
      cc119 = 119,
      cc120 = 120,
      cc121 = 121,
      cc122 = 122,
      cc123 = 123,
      cc124 = 124,
      cc125 = 125,
      cc126 = 126,
      cc127 = 127
  );
  TMIDICtrlChanges = set of TMIDICtrlChange;

  { MIDI Internal MCU Code / Set }
  TMIDIProcCode = (
      mcRecCh1=0,
      mcRecCh2=1,
      mcRecCh3=2,
      mcRecCh4=3,
      mcRecCh5=4,
      mcRecCh6=5,
      mcRecCh7=6,
      mcRecCh8=7,
      mcSoloCh1=8,
      mcSoloCh2=9,
      mcSoloCh3=10,
      mcSoloCh4=11,
      mcSoloCh5=12,
      mcSoloCh6=13,
      mcSoloCh7=14,
      mcSoloCh8=15,
      mcMuteCh1=16,
      mcMuteCh2=17,
      mcMuteCh3=18,
      mcMuteCh4=19,
      mcMuteCh5=20,
      mcMuteCh6=21,
      mcMuteCh7=22,
      mcMuteCh8=23,
      mcSelectCh1=24,
      mcSelectCh2=25,
      mcSelectCh3=26,
      mcSelectCh4=27,
      mcSelectCh5=28,
      mcSelectCh6=29,
      mcSelectCh7=30,
      mcSelectCh8=31,
      mcVPOTSelect1=32,
      mcVPOTSelect2=33,
      mcVPOTSelect3=34,
      mcVPOTSelect4=35,
      mcVPOTSelect5=36,
      mcVPOTSelect6=37,
      mcVPOTSelect7=38,
      mcVPOTSelect8=39,
      mcAssignTrack=40,
      mcAssigneSend=41,
      mcAssignPan=42,
      mcAssignPlugIn=43,
      mcAssignEQ=44,
      mcAssignInstrument=45,
      mcBankLeft=46,
      mcBankRight=47,
      mcChannelLeft=48,
      mcChannelRight=49,
      mcFlip=50,
      mcGlobal=51,
      mcDisplayName=52,
      mcDisplayBeats=53,
      mcF1=54,
      mcF2=55,
      mcF3=56,
      mcF4=57,
      mcF5=58,
      mcF6=59,
      mcF7=60,
      mcF8=61,
      mcMidiTracks=62,
      mcInputs=63,
      mcAudioTracks=64,
      mcAudioInstrument=65,
      mcAux=66,
      mcBus=67,
      mcOutputs=68,
      mcUser=69,
      mcShift=70,
      mcOption=71,
      mcControl=72,
      mcAlt=73,
      mcRead=74,
      mcWrite=75,
      mcTrim=76,
      mcTouch=77,
      mcLatch=78,
      mcGroup=79,
      mcSave=80,
      mcUndo=81,
      mcCancel=82,
      mcEnter=83,
      mcMarkers=84,
      mcNudge=85,
      mcCycle=86,
      mcDrop=87,
      mcReplace=88,
      mcClick=89,
      mcSolo=90,
      mcRwd=91,
      mcFwd=92,
      mcStop=93,
      mcPlay=94,
      mcRecord=95,
      mcUP=96,
      mcDOWN=97,
      mcScrub=98,
      mcZoom=99,
      mcLEFT=100,
      mcRIGHT=101,
      mcSMPTELED=102,
      mcBEATSLED=103,

      mcResv0=104,
      mcResv1=105,
      mcResv2=106,
      mcResv3=107,
      mcResv4=108,
      mcResv5=109,

      mcResvFd7 = 110,
      mcResvG7 = 111,
      mcResvGd7 = 112,
      mcResvA7 = 113,
      mcResvAd7 = 114,
      mcResvB7 = 115,
      mcResvC8 = 116,
      mcResvCd8 = 117,
      mcResvD8 = 118,
      mcResvDd8 = 119,
      mcResvE8 = 120,

      mcResv6=121,
      mcResv7=122,
      mcResv8=123,
      mcResv9=124,
      mcResv10=125,
      mcResv11=126,
      mcResv12=127,
      mcResv13=128,
      mcResv14=129,

      mcSlider1=130,
      mcSlider2=131,
      mcSlider3=132,
      mcSlider4=133,
      mcSlider5=134,
      mcSlider6=135,
      mcSlider7=136,
      mcSlider8=137,

      mcNull=138

  );

  TMIDIProcCodes = set of TMIDIProcCode;

  { Internal Send code }
  TOSCSendType = (ossUndefined=0, ossFixed=1, ossPulse=2, ossInverse=3, ossStep=4, ossAnime=5, ossPass=6);

  { Internal operation code for Received Event }
  TRecOperationType = (otNull=0, otLess=1, otGreater=2, otLessEq=3, otGreaterEq=4, otEqual=5, otNotEqual=6, otContain=7, otNotContain=8, otRegExp=9);

  { Cell Callback }
  TCellExecuteEvent = procedure(Sender:TObject; CellIndex:Integer) of object;

  { Cell Settings }
  TCellRoll = record
      iRoll : Integer;
      iParameter : String;
  end;

  TCellSettings = record
      CellNumber:Integer;
      CellRolls: TArray<TCellRoll>;
      CellLabel:String;
      InputAddress:String;
      InputType:String;
      SendType:TOSCSendType;
      InputValues: TArray<Single>;
  end;

  TReceivedEvent = record
      Address : String;
      Operation : TRecOperationType;
      Value : String;
      Command : String;
      CommandParam : String;
      LastProcessValue : String;
      LastExecution : TDateTime;
      LastExecutionValue : String;
      ExecutionLock : Boolean;
  end;

  { Virtual Keycode / set }
  TVirtualKeys = (
        { Virtual Keys, Standard Set }
        vkNull             = $00,
        vkLButton          = $01,  {   1 }
        vkRButton          = $02,  {   2 }
        vkCancel           = $03,  {   3 }
        vkMButton          = $04,  {   4 }
        vkXButton1         = $05,  {   5 }
        vkXButton2         = $06,  {   6 }
        vk0x07             = $07,
        vkBack             = $08,  {   8 }
        vkTab              = $09,  {   9 }
        vkLineFeed         = $0A,  {  10 }
        vk0x0B             = $0B,
        vkClear            = $0C,  {  12 }
        vkReturn           = $0D,  {  13 }
        vk0x0E             = $0E,
        vk0x0F             = $0F,
        vkShift            = $10,  {  16 }
        vkControl          = $11,  {  17 }
        vkMenu             = $12,  {  18 }
        vkPause            = $13,  {  19 }
        vkCapital          = $14,  {  20 }
        vkKana             = $15,  {  21 }
        vk0x16             = $16,
        vkJunja            = $17,  {  23 }
        vkFinal            = $18,  {  24 }
        vkKanji            = $19,  {  25 }
        vk0x1A             = $1A,
        vkEscape           = $1B,  {  27 }
        vkConvert          = $1C,  {  28 }
        vkNonConvert       = $1D,  {  29 }
        vkAccept           = $1E,  {  30 }
        vkModeChange       = $1F,  {  31 }
        vkSpace            = $20,  {  32 }
        vkPrior            = $21,  {  33 }
        vkNext             = $22,  {  34 }
        vkEnd              = $23,  {  35 }
        vkHome             = $24,  {  36 }
        vkLeft             = $25,  {  37 }
        vkUp               = $26,  {  38 }
        vkRight            = $27,  {  39 }
        vkDown             = $28,  {  40 }
        vkSelect           = $29,  {  41 }
        vkPrint            = $2A,  {  42 }
        vkExecute          = $2B,  {  43 }
        vkSnapshot         = $2C,  {  44 }
        vkInsert           = $2D,  {  45 }
        vkDelete           = $2E,  {  46 }
        vkHelp             = $2F,  {  47 }
        { vk0 thru vk9 are the same as ASCII '0' thru '9' ($30 - $39) }
        vk0                = $30,  {  48 }
        vk1                = $31,  {  49 }
        vk2                = $32,  {  50 }
        vk3                = $33,  {  51 }
        vk4                = $34,  {  52 }
        vk5                = $35,  {  53 }
        vk6                = $36,  {  54 }
        vk7                = $37,  {  55 }
        vk8                = $38,  {  56 }
        vk9                = $39,  {  57 }
        vk0x3A             = $3A,
        vk0x3B             = $3B,
        vk0x3C             = $3C,
        vk0x3D             = $3D,
        vk0x3E             = $3E,
        vk0x3F             = $3F,
        vk0x40             = $40,
        { vkA thru vkZ are the same as ASCII 'A' thru 'Z' ($41 - $5A) }
        vkA                = $41,  {  65 }
        vkB                = $42,  {  66 }
        vkC                = $43,  {  67 }
        vkD                = $44,  {  68 }
        vkE                = $45,  {  69 }
        vkF                = $46,  {  70 }
        vkG                = $47,  {  71 }
        vkH                = $48,  {  72 }
        vkI                = $49,  {  73 }
        vkJ                = $4A,  {  74 }
        vkK                = $4B,  {  75 }
        vkL                = $4C,  {  76 }
        vkM                = $4D,  {  77 }
        vkN                = $4E,  {  78 }
        vkO                = $4F,  {  79 }
        vkP                = $50,  {  80 }
        vkQ                = $51,  {  81 }
        vkR                = $52,  {  82 }
        vkS                = $53,  {  83 }
        vkT                = $54,  {  84 }
        vkU                = $55,  {  85 }
        vkV                = $56,  {  86 }
        vkW                = $57,  {  87 }
        vkX                = $58,  {  88 }
        vkY                = $59,  {  89 }
        vkZ                = $5A,  {  90 }
        vkLWin             = $5B,  {  91 }
        vkRWin             = $5C,  {  92 }
        vkApps             = $5D,  {  93 }
        vk0x5E             = $5E,
        vkSleep            = $5F,  {  95 }
        vkNumpad0          = $60,  {  96 }
        vkNumpad1          = $61,  {  97 }
        vkNumpad2          = $62,  {  98 }
        vkNumpad3          = $63,  {  99 }
        vkNumpad4          = $64,  { 100 }
        vkNumpad5          = $65,  { 101 }
        vkNumpad6          = $66,  { 102 }
        vkNumpad7          = $67,  { 103 }
        vkNumpad8          = $68,  { 104 }
        vkNumpad9          = $69,  { 105 }
        vkMultiply         = $6A,  { 106 }
        vkAdd              = $6B,  { 107 }
        vkSeparator        = $6C,  { 108 }
        vkSubtract         = $6D,  { 109 }
        vkDecimal          = $6E,  { 110 }
        vkDivide           = $6F,  { 111 }
        vkF1               = $70,  { 112 }
        vkF2               = $71,  { 113 }
        vkF3               = $72,  { 114 }
        vkF4               = $73,  { 115 }
        vkF5               = $74,  { 116 }
        vkF6               = $75,  { 117 }
        vkF7               = $76,  { 118 }
        vkF8               = $77,  { 119 }
        vkF9               = $78,  { 120 }
        vkF10              = $79,  { 121 }
        vkF11              = $7A,  { 122 }
        vkF12              = $7B,  { 123 }
        vkF13              = $7C,  { 124 }
        vkF14              = $7D,  { 125 }
        vkF15              = $7E,  { 126 }
        vkF16              = $7F,  { 127 }
        vkF17              = $80,  { 128 }
        vkF18              = $81,  { 129 }
        vkF19              = $82,  { 130 }
        vkF20              = $83,  { 131 }
        vkF21              = $84,  { 132 }
        vkF22              = $85,  { 133 }
        vkF23              = $86,  { 134 }
        vkF24              = $87,  { 135 }

        vkCamera           = $88,  { 136 }
        vkHardwareBack     = $89,  { 137 }

        vk0x8A             = $8A,
        vk0x8B             = $8B,
        vk0x8C             = $8C,
        vk0x8D             = $8D,
        vk0x8E             = $8E,
        vk0x8F             = $8F,

        vkNumLock          = $90,  { 144 }
        vkScroll           = $91,  { 145 }

        vk0x92             = $92,
        vk0x93             = $93,
        vk0x94             = $94,
        vk0x95             = $95,
        vk0x96             = $96,
        vk0x97             = $97,
        vk0x98             = $98,
        vk0x99             = $99,
        vk0x9A             = $9A,
        vk0x9B             = $9B,
        vk0x9C             = $9C,
        vk0x9D             = $9D,
        vk0x9E             = $9E,
        vk0x9F             = $9F,

        vkLShift           = $A0,  { 160 }
        vkRShift           = $A1,  { 161 }
        vkLControl         = $A2,  { 162 }
        vkRControl         = $A3,  { 163 }
        vkLMenu            = $A4,  { 164 }
        vkRMenu            = $A5,  { 165 }

        vkBrowserBack      = $A6,  { 166 }
        vkBrowserForward   = $A7,  { 167 }
        vkBrowserRefresh   = $A8,  { 168 }
        vkBrowserStop      = $A9,  { 169 }
        vkBrowserSearch    = $AA,  { 170 }
        vkBrowserFavorites = $AB,  { 171 }
        vkBrowserHome      = $AC,  { 172 }
        vkVolumeMute       = $AD,  { 173 }
        vkVolumeDown       = $AE,  { 174 }
        vkVolumeUp         = $AF,  { 175 }
        vkMediaNextTrack   = $B0,  { 176 }
        vkMediaPrevTrack   = $B1,  { 177 }
        vkMediaStop        = $B2,  { 178 }
        vkMediaPlayPause   = $B3,  { 179 }
        vkLaunchMail       = $B4,  { 180 }
        vkLaunchMediaSelect= $B5,  { 181 }
        vkLaunchApp1       = $B6,  { 182 }
        vkLaunchApp2       = $B7,  { 183 }

        vk0xB8             = $B8,
        vk0xB9             = $B9,

        vkColon            = $BA,  { 186 }
        vkSemicolon        = $BB,  { 187 }
        vkComma            = $BC,  { 188 }
        vkMinus            = $BD,  { 189 }
        vkPeriod           = $BE,  { 190 }
        vkSlash            = $BF,  { 191 }
        vkOEMAt            = $C0,  { 192 }

        vk0xC1             = $C1,
        vk0xC2             = $C2,
        vk0xC3             = $C3,
        vk0xC4             = $C4,
        vk0xC5             = $C5,
        vk0xC6             = $C6,
        vk0xC7             = $C7,
        vk0xC8             = $C8,
        vk0xC9             = $C9,
        vk0xCA             = $CA,
        vk0xCB             = $CB,
        vk0xCC             = $CC,
        vk0xCD             = $CD,
        vk0xCE             = $CE,
        vk0xCF             = $CF,

        vk0xD0             = $D0,
        vk0xD1             = $D1,
        vk0xD2             = $D2,
        vk0xD3             = $D3,
        vk0xD4             = $D4,
        vk0xD5             = $D5,
        vk0xD6             = $D6,
        vk0xD7             = $D7,
        vk0xD8             = $D8,
        vk0xD9             = $D9,
        vk0xDA             = $DA,

        vkLeftBracket      = $DB,  { 219 }
        vkBackslash        = $DC,  { 220 }
        vkRightBracket     = $DD,  { 221 }
        vkCaret            = $DE,  { 222 }
        vkPara             = $DF,  { 223 }

        vk0xE0             = $E0,
        vk0xE1             = $E1,

        vkOem102           = $E2,  { 226 }
        vkIcoHelp          = $E3,  { 227 }
        vkIco00            = $E4,  { 228 }
        vkProcessKey       = $E5,  { 229 }
        vkIcoClear         = $E6,  { 230 }
        vkPacket           = $E7,  { 231 }

        vk0xE8             = $E8,
        vk0xE9             = $E9,
        vk0xEA             = $EA,
        vk0xEB             = $EB,
        vk0xEC             = $EC,
        vk0xED             = $ED,
        vk0xEE             = $EE,
        vk0xEF             = $EF,
        vkOemAttn          = $F0,
        vkOemFinish        = $F1,
        vkOemCopy          = $F2,
        vkOemAuto          = $F3,
        vkOemEnlw          = $F4,
        vkOemBackTab       = $F5,

        vkAttn             = $F6,  { 246 }
        vkCrsel            = $F7,  { 247 }
        vkExsel            = $F8,  { 248 }
        vkErEof            = $F9,  { 249 }
        vkPlay             = $FA,  { 250 }
        vkZoom             = $FB,  { 251 }
        vkNoname           = $FC,  { 252 }
        vkPA1              = $FD,  { 253 }
        vkOemClear         = $FE,  { 254 }
        vkNone             = $FF  { 255 }
  );

  TVirtualKeySet = set of TVirtualKeys;

  { 32bit Parser }
  P32BitArray = ^T32BitArray;
  T32BitArray = packed record
      b1 : Byte;
      b2 : Byte;
      b3 : Byte;
      b4 : Byte;
  end;

const
  MIDIPROC_ANALOG = [TMIDIProcCode.mcAssignPan, TMIDIProcCode.mcResvGd7 .. TMIDIProcCode.mcResvDd8, TMIDIProcCode.mcSlider1 .. TMIDIProcCode.mcSlider8];

{$IFDEF MSWINDOWS}
const
  WM_INPUT  = $00FF;

  RIDEV_REMOVE                = $00000001;
  RIDEV_EXCLUDE               = $00000010;
  RIDEV_PAGEONLY              = $00000020;
  RIDEV_NOLEGACY              = $00000030;
  RIDEV_INPUTSINK             = $00000100;
  RIDEV_CAPTUREMOUSE          = $00000200;
  RIDEV_NOHOTKEYS             = $00000200;
  RIDEV_APPKEYS               = $00000400;

  RIM_TYPEMOUSE               = $00000000;
  RIM_TYPEKEYBOARD            = $00000001;
  RIM_TYPEHID                 = $00000002;

  RID_INPUT                   = $10000003;
  RID_HEADER                  = $10000005;

  RIDI_PREPARSEDDATA          = $20000005;
  RIDI_DEVICENAME             = $20000007;
  RIDI_DEVICEINFO             = $2000000b;

  APPCOMMAND_BROWSER_BACKWARD                     = 1;
  APPCOMMAND_BROWSER_FORWARD                      = 2;
  APPCOMMAND_BROWSER_REFRESH                      = 3;
  APPCOMMAND_BROWSER_STOP                         = 4;
  APPCOMMAND_BROWSER_SEARCH                       = 5;
  APPCOMMAND_BROWSER_FAVORITES                    = 6;
  APPCOMMAND_BROWSER_HOME                         = 7;
  APPCOMMAND_VOLUME_MUTE                          = 8;
  APPCOMMAND_VOLUME_DOWN                          = 9;
  APPCOMMAND_VOLUME_UP                            = 10;
  APPCOMMAND_MEDIA_NEXTTRACK                      = 11;
  APPCOMMAND_MEDIA_PREVIOUSTRACK                  = 12;
  APPCOMMAND_MEDIA_STOP                           = 13;
  APPCOMMAND_MEDIA_PLAY_PAUSE                     = 14;
  APPCOMMAND_LAUNCH_MAIL                          = 15;
  APPCOMMAND_LAUNCH_MEDIA_SELECT                  = 16;
  APPCOMMAND_LAUNCH_APP1                          = 17;
  APPCOMMAND_LAUNCH_APP2                          = 18;
  APPCOMMAND_BASS_DOWN                            = 19;
  APPCOMMAND_BASS_BOOST                           = 20;
  APPCOMMAND_BASS_UP                              = 21;
  APPCOMMAND_TREBLE_DOWN                          = 22;
  APPCOMMAND_TREBLE_UP                            = 23;

  APPCOMMAND_MICROPHONE_VOLUME_MUTE               = 24;
  APPCOMMAND_MICROPHONE_VOLUME_DOWN               = 25;
  APPCOMMAND_MICROPHONE_VOLUME_UP                 = 26;
  APPCOMMAND_HELP                                 = 27;
  APPCOMMAND_FIND                                 = 28;
  APPCOMMAND_NEW                                  = 29;
  APPCOMMAND_OPEN                                 = 30;
  APPCOMMAND_CLOSE                                = 31;
  APPCOMMAND_SAVE                                 = 32;
  APPCOMMAND_PRINT                                = 33;
  APPCOMMAND_UNDO                                 = 34;
  APPCOMMAND_REDO                                 = 35;
  APPCOMMAND_COPY                                 = 36;
  APPCOMMAND_CUT                                  = 37;
  APPCOMMAND_PASTE                                = 38;
  APPCOMMAND_REPLY_TO_MAIL                        = 39;
  APPCOMMAND_FORWARD_MAIL                         = 40;
  APPCOMMAND_SEND_MAIL                            = 41;
  APPCOMMAND_SPELL_CHECK                          = 42;
  APPCOMMAND_DICTATE_OR_COMMAND_CONTROL_TOGGLE    = 43;
  APPCOMMAND_MIC_ON_OFF_TOGGLE                    = 44;
  APPCOMMAND_CORRECTION_LIST                      = 45;
  APPCOMMAND_MEDIA_PLAY                           = 46;
  APPCOMMAND_MEDIA_PAUSE                          = 47;
  APPCOMMAND_MEDIA_RECORD                         = 48;
  APPCOMMAND_MEDIA_FAST_FORWARD                   = 49;
  APPCOMMAND_MEDIA_REWIND                         = 50;
  APPCOMMAND_MEDIA_CHANNEL_UP                     = 51;
  APPCOMMAND_MEDIA_CHANNEL_DOWN                   = 52;

  APPCOMMAND_DELETE                               = 53;
  APPCOMMAND_DWM_FLIP3D                           = 54;

  APPCOMMAND_MEDIA_PLAY_PAUSE2                    = 4143;
  APPCOMMAND_MEDIA_PLAY2                          = 4142;
  APPCOMMAND_MEDIA_PAUSE2                         = 4143;
  APPCOMMAND_MEDIA_RECORD2                        = 4144;
  APPCOMMAND_MEDIA_FASTFORWARD2                   = 4145;
  APPCOMMAND_MEDIA_REWIND2                        = 4146;
  APPCOMMAND_MEDIA_CHANNEL_UP2                    = 4147;
  APPCOMMAND_MEDIA_CHANNEL_DOWN2                  = 4148;

type
  PAstat = ^TAstat;
  TAstat = packed record
    adapt: TAdapterStatus;
    NameBuff: array [0 .. 29] of TNameBuffer;
  end;

  USHORT = Word;

  TRAWINPUTDEVICE = packed record
    usUsagePage : WORD;
    usUsage     : WORD;
    dwFlags     : Cardinal;
    hwndTarget  : HWND;
  end;
  PRAWINPUTDEVICE = ^TRAWINPUTDEVICE;

  TRAWINPUTHEADER = packed record
    dwType    : Cardinal;
    dwSize    : Cardinal;
    hDevice   : THANDLE;
    wParam    : WPARAM;
  end;
  PRAWINPUTHEADER = ^TRAWINPUTHEADER;

  TRMBUTTONS = packed record
    case Integer of
        0:(ulButtons: ULONG);
        1:(
           usButtonFlags : SHORT;
           usButtonData  : SHORT;
          );
    end;

  TRAWMOUSE = packed record
    usFlags            : SHORT;
    RMButtons          : TRMBUTTONS;
    ulRawButtons       : ULONG;
    lLastX             : LongInt;
    lLastY             : LongInt;
    ulExtraInformation : ULONG;
  end;
  PRAWMOUSE = ^TRAWMOUSE;

  TRAWKEYBOARD = packed record
    MakeCode         : SHORT;
    Flags            : SHORT;
    Reserved         : SHORT;
    VKey             : SHORT;
    Mess             : UINT;
    ExtraInformation : ULONG;
  end;
  PRAWKEYBOARD = ^TRAWKEYBOARD;

  TRAWHID = packed record
    dwSizeHid : Cardinal;
    dwCount   : Cardinal;
    bRawData  : BYTE;
  end;
  PTRAWHID = ^TRAWHID;

  TRAWINPUTDATA = packed record
    case Integer of
      0:(mouse    : TRAWMOUSE   );
      1:(keyboard : TRAWKEYBOARD);
      2:(hid      : TRAWHID     );
    end;

  TRAWINPUT = packed record
      header  : TRAWINPUTHEADER;
      data    : TRAWINPUTDATA;
  end;
  PRAWINPUT = ^TRAWINPUT;

  TRID_DEVICE_INFO_MOUSE = packed record
     dwId               : Cardinal;
     dwNumberOfButtons  : Cardinal;
     dwSampleRate       : Cardinal;
  end;
  PRID_DEVICE_INFO_MOUSE = ^TRID_DEVICE_INFO_MOUSE;

  TRID_DEVICE_INFO_KEYBOARD = packed record
     dwType : Cardinal;
     dwSubType              : Cardinal;
     dwKeyboardMode         : Cardinal;
     dwNumberOfFunctionKeys : Cardinal;
     dwNumberOfIndicators   : Cardinal;
     dwNumberOfKeysTotal    : Cardinal;
  end;
  PRID_DEVICE_INFO_KEYBOARD = ^TRID_DEVICE_INFO_KEYBOARD;

  TRID_DEVICE_INFO_HID  = packed record
     dwVendorId       : Cardinal;
     dwProductId      : Cardinal;
     dwVersionNumber  : Cardinal;
     usUsagePage      : USHORT;
     usUsage          : USHORT;
     end;
  PRID_DEVICE_INFO_HID = ^TRID_DEVICE_INFO_HID;

  TRID_DEVICE_INFO = packed record
      cbSize : Cardinal;
      dwType : Cardinal;
      case Integer of
        0:(mouse    : TRID_DEVICE_INFO_MOUSE   );
        1:(keyboard : TRID_DEVICE_INFO_KEYBOARD);
        2:(hid      : TRID_DEVICE_INFO_HID     );
  end;
  PRID_DEVICE_INFO = ^TRID_DEVICE_INFO;

{$EXTERNALSYM RegisterRawInputDevices}
function RegisterRawInputDevices(pRawInputDevices: PRAWINPUTDEVICE; uiNumDevices:UINT; cbSize: UINT): BOOL; stdcall; external 'user32.dll';
{$EXTERNALSYM GetRawInputData}
function GetRawInputData(hRawInput: Pointer; uiCommand:UINT; pData: Pointer; pcbSize: Pointer; cbSizeHeader: UINT): UINT; stdcall; external 'user32.dll';
{$EXTERNALSYM GetRawInputDeviceInfoA}
function GetRawInputDeviceInfoA(hDevice: THandle; uiCommand:UINT; pData: Pointer; pcbSize: Pointer): UINT; stdcall; external 'user32.dll';
{$EXTERNALSYM GetRawInputDeviceInfoW}
function GetRawInputDeviceInfoW(hDevice: THandle; uiCommand:UINT; pData: Pointer; pcbSize: Pointer): UINT; stdcall; external 'user32.dll';
{$EXTERNALSYM SendARP}
function SendARP (DestIp: ULONG; srcIP: ULONG; pMacAddr: Pointer; PhyAddrLen: Pointer): DWORD; stdcall; external 'iphlpapi.dll';

function MIDIMCUNote2LocalCode(const note:byte):TMIDIProcCode;

{$ENDIF}

{ Global Variables }
var
  GlobalUserIDHash : String;
  GlobalPassCodeHash : String;
  GlobalPassCodeRaw : Cardinal;
  GlobalCellLabel : TArray<String>;
  GlobalOSCDirectory : String;
  GlobalLocaleID : String;
  GlobalTimer : TStopWatch;
  TranslateDictionary : TDictionary<string, string>;

implementation

function MIDIMCUNote2LocalCode(const note:byte):TMIDIProcCode;
begin

      Result := TMIDIProcCode.mcNull;

      case note of
            //0 - 39 : REC-VPOT
            note_Cm1 .. note_Dd_2 : Result := TMIDIProcCode( Byte(TMIDIProcCode.mcRecCh1) + (note - note_Cm1) ); //Result := TMIDIProcCode(note);

            note_E_2: Result := TMIDIProcCode.mcAssignTrack; // 40 E2 : Assign Track
            note_F_2: Result := TMIDIProcCode.mcAssigneSend; // 41 F2 : Assigne Send
            note_Fd_2: Result := TMIDIProcCode.mcassignPan; // 42 F#2 : Assign Pan/Surround
            note_G_2: Result := TMIDIProcCode.mcAssignPlugIn; // 43 G2 : Assign Plug In
            note_Gd_2: Result := TMIDIProcCode.mcassignEQ; // 44 G#2 : Assign EQ
            note_A_2: Result := TMIDIProcCode.mcassignInstrument; // 45 A2 : Assign Instrument
            note_Ad_2: Result := TMIDIProcCode.mcbankLeft; // 46 A#2 :  Bank Left
            note_B_2: Result := TMIDIProcCode.mcbankRight; // 47 B2 : Bank Right
            note_C_3: Result := TMIDIProcCode.mcChannelLeft; // 48 C3 : Channel Left
            note_Cd_3: Result := TMIDIProcCode.mcChannelRight; // 49 C#3 :  Channel Right
            note_D_3: Result := TMIDIProcCode.mcflip; // 50 D3 : Flip
            note_Dd_3: Result := TMIDIProcCode.mcglobal; // 51 D#3 : Global
            note_E_3: Result := TMIDIProcCode.mcDisplayName; // 52 E3 : Display Name/Value
            note_F_3:  Result := TMIDIProcCode.mcDisplayBeats; // 53 F3 : Display SMPTE/Beats

            // 54 - 61
            note_Fd_3 .. note_Cd_4 :
                  Result := TMIDIProcCode( Byte(TMIDIProcCode.mcF1) + (note - note_Fd_3) );

            note_D_4: Result := TMIDIProcCode.mcmidiTracks; // 62 D4 : Midi Tracks
            note_Dd_4: Result := TMIDIProcCode.mcinputs; // 63 D#4 : Inputs
            note_E_4: Result := TMIDIProcCode.mcaudioTracks;  // 64 E4 : AudioTracks
            note_F_4: Result := TMIDIProcCode.mcaudioInstrument;// 65 F4 : Audio Instrument
            note_Fd_4: Result := TMIDIProcCode.mcaux; // 66 F#4 : Aux
            note_G_4: Result := TMIDIProcCode.mcbus; // 67 G4 : Bus
            note_Gd_4: Result := TMIDIProcCode.mcoutputs; // 68 G#4 : Outputs
            note_A_4: Result := TMIDIProcCode.mcuser; // 69 A4 : User
            note_Ad_4: Result := TMIDIProcCode.mcshift; // 70 A#4 : Shift
            note_B_4: Result := TMIDIProcCode.mcoption; // 71 B4 : Option
            note_C_5: Result := TMIDIProcCode.mccontrol; // 72 C5 : Control
            note_Cd_5: Result := TMIDIProcCode.mcalt; // 73 C#5 : Alt
            note_D_5: Result := TMIDIProcCode.mcread; // 74 D5 : Read/Off
            note_Dd_5: Result := TMIDIProcCode.mcwrite; // 75 D#5 : Write
            note_E_5: Result := TMIDIProcCode.mctrim; // 76 E5 : Trim
            note_F_5: Result := TMIDIProcCode.mctouch; // 77 F5 : Touch
            note_Fd_5: Result := TMIDIProcCode.mclatch; // 78 F#5 : Latch
            note_G_5: Result := TMIDIProcCode.mcgroup; // 79 G5 : Group
            note_Gd_5: Result := TMIDIProcCode.mcsave; // 80 G#5 : Save
            note_A_5: Result := TMIDIProcCode.mcundo; // 81 A5 : Undo
            note_Ad_5: Result := TMIDIProcCode.mccancel; // 82 A#5 : Cancel
            note_B_5: Result := TMIDIProcCode.mcenter; // 83 B5 : Enter
            note_C_6: Result := TMIDIProcCode.mcmarkers; // 84 C6 : Markers
            note_Cd_6: Result := TMIDIProcCode.mcnudge; // 85 C#6 : Nudge
            note_D_6: Result := TMIDIProcCode.mccycle; // 86 D6 : Cycle
            note_Dd_6: Result := TMIDIProcCode.mcdrop; // 87 D#6 : Drop
            note_E_6: Result := TMIDIProcCode.mcreplace; // 88 E6 : Replace
            note_F_6: Result := TMIDIProcCode.mcclick; // 89 F6 : Click
            note_Fd_6: Result := TMIDIProcCode.mcsolo; // 90 F#6 : Solo
            note_G_6: Result := TMIDIProcCode.mcRwd; // 91 G6 : Rwd
            note_Gd_6: Result := TMIDIProcCode.mcFwd; // 92 G#6 : Fwd
            note_A_6: Result := TMIDIProcCode.mcstop; // 93 A6 : Stop
            note_Ad_6: Result := TMIDIProcCode.mcplay; // 94 A#6 : Play
            note_B_6: Result := TMIDIProcCode.mcRecord; // 95 B6 : Record
            note_C_7: Result := TMIDIProcCode.mcup; // 96 C7 : UP
            note_Cd_7: Result := TMIDIProcCode.mcdown; // 97 C#7 : DOWN
            note_D_7: Result := TMIDIProcCode.mcscrub; // 98 D7 : Scrub
            note_Dd_7: Result := TMIDIProcCode.mczoom; // 99 D#7 : Zoom
            note_E_7: Result := TMIDIProcCode.mcleft; // 100 E7 : LEFT
            note_F_7: Result := TMIDIProcCode.mcright; // 101 F7 : RIGHT

            note_Fd_7: Result := TMIDIProcCode.mcResvFd7;
            note_G_7: Result := TMIDIProcCode.mcResvG7;
            note_Gd_7: Result := TMIDIProcCode.mcResvGd7;
            note_A_7: Result := TMIDIProcCode.mcResvA7;
            note_Ad_7: Result := TMIDIProcCode.mcResvAd7;
            note_B_7: Result := TMIDIProcCode.mcResvB7;

            note_C_8: Result := TMIDIProcCode.mcResvC8;
            note_Cd_8: Result := TMIDIProcCode.mcResvCd8;
            note_D_8: Result := TMIDIProcCode.mcResvD8;
            note_Dd_8: Result := TMIDIProcCode.mcResvDd8;
            note_E_8: Result := TMIDIProcCode.mcResvE8;

            note_F_8: Result := TMIDIProcCode.mcSMPTELED; // 113 F8 : SMPTE LED
            note_Fd_8: Result := TMIDIProcCode.mcBEATSLED; // 114 F#8 : BEATS LED
      end;
end;


end.
