unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ExtCtrls, FMX.Edit, FMX.EditBox, FMX.ComboTrackBar, FMX.Layouts,
  FMX.Controls.Presentation, FMX.Text, IdUDPClient, IdGlobal, FMX.Menus,
  System.Diagnostics, System.Math, AppInclude, AppUtils, FMX.Objects, FMX.ListBox,
  System.Generics.Collections, Lime.Util.SendKeys;

type

  TExecutionCell = class(TFrame)
    Layout1: TLayout;
    AddressBar: TEdit;
    EllipsesEditButton1: TEllipsesEditButton;
    Timer1: TTimer;
    RoundRect1: TRoundRect;
    PresetNumLabel: TLabel;
    Layout2: TLayout;
    ParamValue1: TComboTrackBar;
    ParamValue2: TComboTrackBar;
    ComboSendStyle: TComboBox;
    ListItemTypeFixed: TListBoxItem;
    ListItemTypeAdd: TListBoxItem;
    ListItemTypeInverse: TListBoxItem;
    ListItemTypeOnce: TListBoxItem;
    ParamValueD: TComboTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    TriggerSettingPopup: TPopup;
    Expander1: TExpander;
    Layout3: TLayout;
    ButtonMemo: TEdit;
    Rectangle1: TRectangle;
    Label8: TLabel;
    Layout4: TLayout;
    Label9: TLabel;
    TypeBox: TPopupBox;
    Label10: TLabel;
    Expander2: TExpander;
    RollEdit: TEdit;
    Expander3: TExpander;
    Layout6: TLayout;
    Image1: TImage;
    Layout7: TLayout;
    ListItemTypeAnime: TListBoxItem;
    TriggerList: TListBox;
    ListTrigKey: TListBoxItem;
    CheckListTrigKey: TCheckBox;
    Label3: TLabel;
    ListTrigKeyOption: TComboBox;
    ListTrigAppMsg: TListBoxItem;
    CheckListTrigAppMsg: TCheckBox;
    Label4: TLabel;
    ListTrigHttpd: TListBoxItem;
    CheckListTrigHttpd: TCheckBox;
    Label5: TLabel;
    ListTrigNerv: TListBoxItem;
    CheckListTrigNerv: TCheckBox;
    Label6: TLabel;
    ListBoxHeader1: TListBoxHeader;
    Label7: TLabel;
    SendButton: TButton;
    KeyNameLabel: TLabel;
    ListBoxItem1: TListBoxItem;
    Label11: TLabel;
    CheckListTrigMIDI: TCheckBox;
    ListTrigMIDIOption2: TComboBox;
    ListTrigMIDIOption1: TComboBox;
    ListItemTypePass: TListBoxItem;
    Layout5: TLayout;
    Lang1: TLang;
    Expander4: TExpander;
    GridPanelLayout1: TGridPanelLayout;
    ButtonCopy: TButton;
    ButtonPaste: TButton;
    procedure TypeBoxChange(Sender: TObject);
    procedure SendButtonClick(Sender: TObject);
    procedure EllipsesEditButton1MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure AddressBarChange(Sender: TObject);
    procedure ParamValuesChange(Sender: TObject);
    procedure ParamValuesKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure SendButtonGesture(Sender: TObject;
      const EventInfo: TGestureEventInfo; var Handled: Boolean);
    procedure SendButtonMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure SendButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure SendButtonMouseLeave(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ComboSendStyleChange(Sender: TObject);
    procedure ButtonMemoExit(Sender: TObject);
    procedure AddressBarExit(Sender: TObject);
    procedure TriggerSetupRenew(Sender: TObject);
    procedure TriggerSetup(Sender: TObject);
    procedure DropDownEditButton1Click(Sender: TObject);
    procedure TriggerElementChange(Sender: TObject);
    procedure ButtonCopyClick(Sender: TObject);
    procedure ButtonPasteClick(Sender: TObject);
  private
    { private �錾 }
    FMasterForm : TControl;
    FCellNumber : Integer;
    FInputAddress : String;
    FInputType : String;
    FSendType : TOSCSendType;
    FInputValues : Array [0..3] of Single;
    FTIdUDPClient : TIdUDPClient;
    FCandidateMenu : TPopupMenu;
    FReceivedParams : TStringList;
    FCellRolls: TArray<TCellRoll>;
    FCellChange : TNotifyEvent;
    FCellExecute : TCellExecuteEvent;
    FTriggerSetupExclusive : Boolean;
    FAnimationAbort : Boolean;
    FRunning : Boolean;
    FSendKeys : TSendKeys;
    FExTagVariant : TDictionary<String, Variant>;
    function GetCellSettings():TCellSettings;
    function SetCellRolls(const CellRolls: Array of TCellRoll):String;
    procedure Changed;
    procedure OnCreate;
    procedure OnDestroy;
    procedure AddKeyBoardItem(const Keyfrom:Byte; const Keyto:Byte); overload;
    procedure AddKeyBoardItem(const Text:String; const Key:TVirtualKeys); overload;
    function ExTagReaderVariant(index:String):Variant;
    procedure ExTagWriterVariant(index:String; Value:Variant);
  public
    { public �錾 }
    constructor Create(AOwner: TComponent; const AName:TComponentName);
    destructor Destroy; override;
    procedure PresetMenuClickNotify(const ASelected:TMenuItem);
    procedure InitializeCell(const MasterForm,ParentObject:TControl; const CellNumber:Integer; const CellRolls:Array of TCellRoll; const CellLabel, InputAddress, InputType:String;  const SendType:TOSCSendType; const InputValues:Array of Single; const UDPClient:TIdUDPClient = nil; const CandidateMenu:TPopupMenu = nil; const KnownParameters:TStrings = nil); overload;
    procedure InitializeCell(const MasterForm,ParentObject:TControl; const CellSettings:TCellSettings; const UDPClient:TIdUDPClient = nil; const CandidateMenu:TPopupMenu = nil; const KnownParameters:TStrings = nil); overload;

    procedure ExecuteCell(const RawValue:Single = NaN);

    function QueryRollActive(const RollType:Integer):Variant;
    procedure ImportCellSettings(const SerializedData:String);
    function ExportCellSettings():String;
    property CellSettings: TCellSettings read GetCellSettings;

    property OnCellChange:TNotifyEvent read FCellChange write FCellChange;
    property OnCellExecute:TCellExecuteEvent read FCellExecute write FCellExecute;
    property Running:Boolean read FRunning;
    property CellNumber:Integer read FCellNumber;
    property InputAddress:String read FInputAddress;
    property InputType:String read FInputType;
    property UDPClient: TIdUDPClient read FTIdUDPClient;
    property CandidateMenu: TPopupMenu read FCandidateMenu;
    property CellRolls:TArray<TCellRoll> read FCellRolls;

    property TagExVar[index:String]:Variant read ExTagReaderVariant write ExTagWriterVariant;

  end;



implementation

{$R *.fmx}

uses Unit1, System.StrUtils, System.JSON.Serializers, System.JSON.Types, FMX.Platform;

procedure TExecutionCell.AddressBarChange(Sender: TObject);
begin
      FInputAddress := AddressBar.Text;
end;

procedure TExecutionCell.ComboSendStyleChange(Sender: TObject);
begin
      if ComboSendStyle.Selected = ListItemTypeFixed then
      begin
            ParamValue1.Enabled := True;
            ParamValue2.Enabled := False;
            ParamValueD.Enabled := False;
            FSendType := ossFixed;
      end else if ComboSendStyle.Selected = ListItemTypeInverse then
      begin
            ParamValue1.Enabled := False;
            ParamValue2.Enabled := False;
            ParamValueD.Enabled := False;
            FSendType := ossInverse;
      end else if ComboSendStyle.Selected = ListItemTypeOnce then
      begin
            ParamValue1.Enabled := True;
            ParamValue2.Enabled := True;
            ParamValueD.Enabled := False;
            FSendType := ossPulse;
      end else if ComboSendStyle.Selected = ListItemTypeAnime then
      begin
            ParamValue1.Enabled := True;
            ParamValue2.Enabled := True;
            ParamValueD.Enabled := True;
            FSendType := ossAnime;
      end else if ComboSendStyle.Selected = ListItemTypePass then
      begin
            ParamValue1.Enabled := True;
            ParamValue2.Enabled := True;
            ParamValueD.Enabled := False;
            FSendType := ossPass;
      end else
      begin
            ParamValue1.Enabled := True;
            ParamValue2.Enabled := True;
            ParamValueD.Enabled := True;

            if ComboSendStyle.Selected = ListItemTypeAdd then FSendType := ossStep;
      end;

      Changed;
end;

constructor TExecutionCell.Create(AOwner: TComponent; const AName:TComponentName);
begin
      inherited Create(AOwner);
      Self.Name := AName;

      OnCreate;
end;

destructor TExecutionCell.Destroy();
begin
      OnDestroy;

      inherited;
end;

function TExecutionCell.ExTagReaderVariant(index:String):Variant;
begin
      if not FExTagVariant.TryGetValue(index, Result) then Result := Null;
end;

procedure TExecutionCell.ExTagWriterVariant(index:String; Value:Variant);
begin
      FExTagVariant.AddOrSetValue(index,Value);
end;

procedure TExecutionCell.DropDownEditButton1Click(Sender: TObject);
begin
      TriggerSettingPopup.Opacity := 0.8;
      TriggerSettingPopup.PlacementTarget := Sender as TControl;
      TriggerSettingPopup.Popup();

end;

procedure TExecutionCell.AddKeyBoardItem(const Keyfrom:Byte; const Keyto:Byte);
var
      i : Integer;
      s : String;
begin
      for i := Keyfrom to Keyto do
      begin
            s := KeyCodeNameMod(GetKeycodeName(i));
            ListTrigKeyOption.Items.AddObject(s,TObject(NativeUINT(i)));
      end;
end;

procedure TExecutionCell.AddKeyBoardItem(const Text:String; const Key:TVirtualKeys);
begin
      ListTrigKeyOption.Items.AddObject(Text,TObject(NativeUINT(Byte(Key))));
end;

procedure TExecutionCell.OnCreate;
const
      JPOnly = ' [JP]';
var
      i : Byte;
      s : String;
      ni : Cardinal;
begin
      FExTagVariant := TDictionary<String,Variant>.Create;
      ListTrigKeyOption.Items.Clear;

      FSendKeys := TSendKeys.Create;

      // Basic 10key
      AddKeyBoardItem(Byte(TVirtualKeys.VKNUMPAD0), Byte(TVirtualKeys.vkDivide));

      // JP Additional 1 [Local]
      AddKeyBoardItem(Byte(TVirtualKeys.vkColon), Byte(TVirtualKeys.vkOEMAt));

      // JP Additional 2 [Local]
      AddKeyBoardItem('\' + JPOnly, TVirtualKeys.vkBackslash);
      AddKeyBoardItem('[' + JPOnly, TVirtualKeys.vkLeftBracket);
      AddKeyBoardItem(']' + JPOnly, TVirtualKeys.vkRightBracket);
      AddKeyBoardItem('^' + JPOnly, TVirtualKeys.vkCaret);
      AddKeyBoardItem('�_' + JPOnly, TVirtualKeys.vkOem102);

      // Well-known Keys, Common
      AddKeyBoardItem(Byte(TVirtualKeys.VKA), Byte(TVirtualKeys.VKZ));
      AddKeyBoardItem(Byte(TVirtualKeys.VK0), Byte(TVirtualKeys.VK9));

      // Well-known Keys, Functional
      AddKeyBoardItem('BackSpace',TVirtualKeys.vkBack);
      AddKeyBoardItem('Delete',TVirtualKeys.VKDELETE);
      AddKeyBoardItem('Space',TVirtualKeys.vkSpace);

      AddKeyBoardItem('Enter',TVirtualKeys.vkReturn);
      AddKeyBoardItem('Tab', TVirtualKeys.vkTab);

      AddKeyBoardItem('��',TVirtualKeys.VKUP);
      AddKeyBoardItem('��',TVirtualKeys.VKDOWN);
      AddKeyBoardItem('��',TVirtualKeys.VKLEFT);
      AddKeyBoardItem('��',TVirtualKeys.VKRIGHT);

      AddKeyBoardItem('��PgUp',TVirtualKeys.VKPRIOR);
      AddKeyBoardItem('��PgDown',TVirtualKeys.VKNEXT);
      AddKeyBoardItem('��Home',TVirtualKeys.VKHOME);
      AddKeyBoardItem('��End',TVirtualKeys.VKEND);

      // Experiments (common)
      AddKeyBoardItem('Shift',TVirtualKeys.vkShift);
      AddKeyBoardItem('Apps',TVirtualKeys.vkApps);
      AddKeyBoardItem('Pause',TVirtualKeys.vkPause);

      // Experiments (jp)
      AddKeyBoardItem(LC('���ϊ�') + JPOnly, TVirtualKeys.vkNonConvert);
      AddKeyBoardItem(LC('�ϊ�') + JPOnly,TVirtualKeys.vkConvert);

      // Experiments (functional)
      //AddKeyBoardItem(Byte(TVirtualKeys.vkVolumeMute), Byte(TVirtualKeys.vkLaunchApp2));

      // MIDI Note
      for i := 0 to Integer(High(TMIDINote)) do
      begin
            ListTrigMIDIOption2.Items.AddObject( MIDIGetNoteName(i).Replace('_',''), TObject( i or MIDISELFLAG_NOTE ) );
      end;

      // MIDI CC
      for i := 0 to Integer(High(TMIDICtrlChange)) do
      begin
            ni := i or MIDISELFLAG_CC;

            s := MIDIGetCCName(TMIDICtrlChange(i));
            if s.EndsWith('_ML') then s := s.Replace('_ML','');

            ListTrigMIDIOption2.Items.AddObject('CC ' + s , TObject( ni ) );
      end;

      // MIDI MCU
      for i := 0 to Integer(High(TMIDIProcCode)) do
      begin
            ListTrigMIDIOption2.Items.AddObject('MCU ' + MIDIGetMCULocalCodeName(TMIDIProcCode(i)) , TObject( i or MIDISELFLAG_MCU ) );
      end;

      ListTrigMIDIOption2.ItemIndex := 0;
end;

procedure TExecutionCell.OnDestroy();
begin
      FExTagVariant.Free;
      FSendKeys.Free;
end;

procedure TExecutionCell.EllipsesEditButton1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
      Coord : TPointF;
      Margin : TPointF;
      PF : TForm;
begin
      Coord := (Sender as TControl).LocalToAbsolute(PointF(0,0));

      PF := (Parent.Root as TForm);
      Margin.X := (PF.Width - PF.ClientWidth) / 2;
      Margin.Y := (PF.Height - PF.ClientHeight) - Margin.X;

      FCandidateMenu.TagObject := (Self as TControl);
      try
            FCandidateMenu.Popup(
                  PF.Left + Margin.X + Coord.X+X,
                  PF.Top + Margin.Y + Coord.Y+Y
            );
      finally

      end;
end;

procedure TExecutionCell.ParamValuesChange(Sender: TObject);
begin
      FInputValues[(Sender as TComboTrackBar).Tag] := (Sender as TComboTrackBar).Value;
      Changed;
end;

procedure TExecutionCell.ParamValuesKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
      ParamValuesChange(Sender);
end;

procedure TExecutionCell.PresetMenuClickNotify(const ASelected:TMenuItem);
var
      Data : TStringList;
      i : Integer;
begin
      Data := TStringList.Create;
      try
            Data.Text := ASelected.TagString;
            AddressBar.Text := Data.Values['iaddr'];

            i := TypeBox.Items.IndexOf(Data.Values['itype']);

            if i >= 0 then TypeBox.ItemIndex := i
            else TypeBox.ItemIndex := TypeBox.Items.IndexOf('None');

            ButtonMemo.Text := ExtractFileName(Data.Values['iaddr'].Replace('/','\'));

            ParamValue1.Value := 0.0;
            ParamValue2.Value := 0.0;
            ParamValueD.Value := 1.0;

      finally
            Data.Free;
      end;
end;

function TExecutionCell.SetCellRolls(const CellRolls:Array of TCellRoll):String;
var
      s : String;
      i,p, q : Integer;
begin
      s := '';
      SetLength(FCellRolls, Length(CellRolls));
      for i := Low(CellRolls) to High(CellRolls) do
      begin
            case CellRolls[i].iRoll of
                  APPSEND_Keycode: s := s + 'KEY ' + KeyCodeNameMod(GetKeycodeName(StrtoIntDef(CellRolls[i].iParameter,0))) + #09;
                  APPSEND_HTTPDEvent : s := s + 'HTTP' + #09;
                  APPSEND_WindowMessage: s := s + 'MSG' + #09;
                  APPSEND_MastodonNerv: s := s + 'NERV' + #09;
                  APPSEND_MIDIIN:
                  begin
                        p := LowerLong(StrtoUInt64Def(CellRolls[i].iParameter,0)) + 1; // 1 based (track)
                        q := UpperLong(StrtoUInt64Def(CellRolls[i].iParameter,0));

                        if (q and MIDISELFLAG_NOTE) > 0 then
                        begin
                              s := s + 'M#' + p.ToString() + ' ' + MIDIGetNoteName(LowerWord(q)).Replace('_','') + #09;

                        end else if ((q and MIDISELFLAG_CC) > 0) or ((q and MIDISELFLAG_CC_ML) > 0) then
                        begin
                              s := s + 'M#' + p.ToString() + ' CC:' + MIDIGetCCName(TMIDICtrlChange(LowerWord(q))) + #09;

                        end else if (q and MIDISELFLAG_MCU) > 0 then
                        begin
                              s := s + 'M#' + p.ToString() + ' MCU:' + MIDIGetMCULocalCodeName(TMIDIProcCode(LowerWord(q))) + #09;
                        end;

                        //s := s + 'MIDI' + #09;
                  end;
            end;

            FCellRolls[i].iRoll := CellRolls[i].iRoll;
            FCellRolls[i].iParameter := CellRolls[i].iParameter;
      end;

      Result := s;

      TriggerSetup(nil);
end;

procedure TExecutionCell.InitializeCell(const MasterForm, ParentObject:TControl; const CellNumber:Integer; const CellRolls:Array of TCellRoll; const CellLabel, InputAddress, InputType:String;
 const SendType:TOSCSendType; const InputValues:Array of Single; const UDPClient:TIdUDPClient = nil; const CandidateMenu:TPopupMenu = nil; const KnownParameters:TStrings = nil);
var
      i : Integer;
      s : String;
begin
      if Assigned(MasterForm) then FMasterForm := MasterForm;
      if Assigned(ParentObject) then Parent := ParentObject;

      if CellNumber >= 0 then
      begin
            PresetNumLabel.Text := CellNumber.ToString;
            FCellNumber := CellNumber;
      end;

      s := SetCellRolls(CellRolls);
      RollEdit.Text := Trim(s).Replace(#09,' | ');

      //if CellLabel1 <> #0 then KeyNameLabel.Text := CellLabel1 + ' ';
      if CellLabel <> #0 then ButtonMemo.Text := CellLabel;

      if InputAddress <> #0 then FInputAddress := InputAddress;
      if InputType <> #0 then FInputType := InputType;

      AddressBar.Text := FInputAddress;
      TypeBox.ItemIndex := TypeBox.Items.IndexOf(FInputType);


      for i := 0 to ListTrigMIDIOption1.Items.Count-1 do ListTrigMIDIOption1.Items.Objects[i] := TObject(i);

      for i := 0 to 2 do
      begin
            if Length(InputValues) > i then
            begin
                  if (not System.Math.IsInfinite(InputValues[i]) and not System.Math.IsNan(InputValues[i])) then FInputValues[i] := InputValues[i];
            end;
      end;

      case SendType of
            ossFixed: ComboSendStyle.ItemIndex := ListItemTypeFixed.Index;
            ossPulse: ComboSendStyle.ItemIndex := ListItemTypeOnce.Index;
            ossInverse: ComboSendStyle.ItemIndex := ListItemTypeInverse.Index;
            ossStep: ComboSendStyle.ItemIndex := ListItemTypeAdd.Index;
            ossAnime: ComboSendStyle.ItemIndex := ListItemTypeAnime.Index;
            ossPass: ComboSendStyle.ItemIndex := ListItemTypePass.Index;
      end;

      ComboSendStyleChange(ComboSendStyle);

      ParamValue1.Value := FInputValues[0];
      ParamValue2.Value := FInputValues[1];
      ParamValueD.Value := FInputValues[2];

      if Assigned(UDPClient) then FTIdUDPClient := UDPClient;
      if Assigned(CandidateMenu) then FCandidateMenu := CandidateMenu;
      if Assigned(KnownParameters) then FReceivedParams := TStringList(KnownParameters);

end;

procedure TExecutionCell.InitializeCell(const MasterForm,ParentObject:TControl; const CellSettings:TCellSettings; const UDPClient:TIdUDPClient = nil; const CandidateMenu:TPopupMenu = nil; const KnownParameters:TStrings = nil);
begin
      InitializeCell(
            MasterForm,
            ParentObject,
            CellSettings.CellNumber,
            CellSettings.CellRolls,
            CellSettings.CellLabel,
            CellSettings.InputAddress,
            CellSettings.InputType,
            CellSettings.SendType,
            CellSettings.InputValues,
            UDPClient,
            CandidateMenu,
            KnownParameters
      );
end;

function TExecutionCell.GetCellSettings():TCellSettings;
var
      i : Integer;
begin
      Result.CellNumber := FCellNumber;
      Result.CellRolls := FCellRolls;
      Result.CellLabel := ButtonMemo.Text;
      Result.InputAddress := FInputAddress;
      Result.InputType := FInputType;
      Result.SendType := FSendType;

      SetLength(Result.InputValues, Length(FInputValues));
      for i := Low(FInputValues) to High(FInputValues) do Result.InputValues[i] := FInputValues[i];
end;

function TExecutionCell.QueryRollActive(const RollType:Integer):Variant;
var
      i : Integer;
begin
      Result := Null;

      for i := 0 to High(FCellRolls) do
      begin
            if ( FCellRolls[i].iRoll = RollType ) then
            begin
                  Result := String(FCellRolls[i].iParameter);

                  if IsDecimal(FCellRolls[i].iParameter) then Result := Int64(StrtoInt64(FCellRolls[i].iParameter))
                  else if IsNumeric(FCellRolls[i].iParameter) then Result := Single(StrtoFloat(FCellRolls[i].iParameter));

                  break;
            end;
      end;
end;

procedure TExecutionCell.ImportCellSettings(const SerializedData:String);
var
      JS : TJsonSerializer;
      CS : TCellSettings;
begin
      JS := TJsonSerializer.Create;
      try
            CS := JS.Deserialize<TCellSettings>(SerializedData);
            CS.CellNumber := CellNumber;
            InitializeCell(nil, nil, CS);
      finally
            JS.Free;
      end;
end;

function TExecutionCell.ExportCellSettings():String;
var
      JS : TJsonSerializer;
begin
      JS := TJsonSerializer.Create;
      try
            JS.Formatting := TJsonFormatting.Indented;
            Result := JS.Serialize(CellSettings);
      finally
            JS.Free;
      end;
end;

procedure TExecutionCell.ExecuteCell(const RawValue:Single = NaN);
begin
      // Simulation
      SendButton.TagFloat := RawValue;
      SendButtonClick(SendButton);
end;

procedure TExecutionCell.AddressBarExit(Sender: TObject);
begin
      Changed;
end;

procedure TExecutionCell.ButtonCopyClick(Sender: TObject);
var
      Clipboard: IFMXClipboardService;
      Obj : string;
begin
      Obj := Self.ExportCellSettings;
      if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, Clipboard) then
      begin
            Clipboard.SetClipboard(Obj);
      end;
end;

procedure TExecutionCell.ButtonMemoExit(Sender: TObject);
begin
      Changed;
end;

procedure TExecutionCell.ButtonPasteClick(Sender: TObject);
var
      Clipboard: IFMXClipboardService;
      Obj : string;
begin
      if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, Clipboard) then
      begin
            Obj := Clipboard.GetClipboard.AsString;
            ImportCellSettings(Obj);
      end;
end;

procedure TExecutionCell.Changed;
begin
      if Assigned(FCellChange) then FCellChange(Self);
      //
end;

procedure TExecutionCell.TriggerElementChange(Sender: TObject);
begin
      if not FTriggerSetupExclusive then
      begin
            TriggerSetupRenew(Sender);
            Changed;
      end;
end;

procedure TExecutionCell.Timer1Timer(Sender: TObject);
begin
      Timer1.Enabled := False;
      SendButton.Enabled := False;
      try
            SendButton.IsPressed := False;
            SendButton.StaysPressed := False;

            SendButton.Repaint;

            //ShowMessage('LongTap')
      finally
            SendButton.Enabled := True;
            SendButton.Repaint;
      end;
end;

procedure TExecutionCell.TriggerSetupRenew(Sender: TObject);
var
      CR : TArray<TCellRoll>;
      i : Integer;
      s : String;
begin
      SetLength(CR, 10); i := 0;

      if CheckListTrigKey.IsChecked then
      begin
            CR[i].iRoll := APPSEND_KEYCODE;
            CR[i].iParameter := InttoStr(NativeUINT(ListTrigKeyOption.Items.Objects[ListTrigKeyOption.ItemIndex]));
            Inc(i);
      end;

      if CheckListTrigHttpd.IsChecked then
      begin
            CR[i].iRoll := APPSEND_HTTPDEvent;
            CR[i].iParameter := '';
            Inc(i);
      end;

      if CheckListTrigAppMsg.IsChecked then
      begin
            CR[i].iRoll := APPSEND_WindowMessage;
            CR[i].iParameter := '';
            Inc(i);
      end;

      if CheckListTrigNerv.IsChecked then
      begin
            CR[i].iRoll := APPSEND_MastodonNerv;
            CR[i].iParameter := '';
            Inc(i);
      end;

      if CheckListTrigMIDI.IsChecked then
      begin
            CR[i].iRoll := APPSEND_MIDIIn;
            CR[i].iParameter := UInttoStr(MakeUINT64(
                  Cardinal(NativeINT(ListTrigMIDIOption1.Items.Objects[Max(ListTrigMIDIOption1.ItemIndex,0)])),
                  Cardinal(NativeINT(ListTrigMIDIOption2.Items.Objects[Max(ListTrigMIDIOption2.ItemIndex,0)]))
            ));
                  //InttoStr(NativeINT(ListTrigMIDIOption1.Items.Objects[ListTrigMIDIOption1.ItemIndex])) + ':' +
                  //InttoStr(NativeINT(ListTrigMIDIOption2.Items.Objects[ListTrigMIDIOption2.ItemIndex]));
            Inc(i);
      end;

      SetLength(CR, i);

      s := SetCellRolls(CR);
      RollEdit.Text := Trim(s).Replace(#09,' | ');
end;

procedure TExecutionCell.TriggerSetup(Sender: TObject);
var
      CR : TArray<TCellRoll>;
      i,j : Integer;
      p,q : Integer;
begin
      FTriggerSetupExclusive := True;
      try
            CR := Self.FCellRolls;

            CheckListTrigKey.IsChecked := False;
            CheckListTrigHttpd.IsChecked := False;
            CheckListTrigAppMsg.IsChecked := False;
            CheckListTrigNerv.IsChecked := False;
            CheckListTrigMIDI.IsChecked := False;

            ListTrigKeyOption.ItemIndex := 0;

            for i := 0 to High(CR) do
            begin
                  case CR[i].iRoll of
                        APPSEND_KEYCODE:
                        begin
                              CheckListTrigKey.IsChecked := True;
                              j := ListTrigKeyOption.Items.IndexOfObject(TObject(StrtoIntDef(CR[i].iParameter,0)));
                              if j >= 0 then ListTrigKeyOption.ItemIndex := j;
                        end;
                        APPSEND_HTTPDEvent:
                              CheckListTrigHttpd.IsChecked := True;

                        APPSEND_WindowMessage:
                              CheckListTrigAppMsg.IsChecked := True;

                        APPSEND_MastodonNerv:
                              CheckListTrigNerv.IsChecked := True;

                        APPSEND_MIDIIN:
                        begin
                              CheckListTrigMIDI.IsChecked := True;

                              p := LowerLong(StrtoUInt64Def(CR[i].iParameter,0));
                              q := UpperLong(StrtoUInt64Def(CR[i].iParameter,0));

                              j := ListTrigMIDIOption1.Items.IndexOfObject(TObject(p));
                              if j >= 0 then ListTrigMIDIOption1.ItemIndex := j;

                              j := ListTrigMIDIOption2.Items.IndexOfObject(TObject(q));
                              if j >= 0 then ListTrigMIDIOption2.ItemIndex := j;

                        end;
                  end;
            end;
      finally
            FTriggerSetupExclusive := False;
      end;
end;

procedure TExecutionCell.TypeBoxChange(Sender:TObject);
var
      ParamValueUI : Array [0..2] of TComboTrackBar;
      i : Integer;
begin
      ParamValueUI[0] := ParamValue1; ParamValueUI[1] := ParamValue2; ParamValueUI[2] := ParamValueD;

      for i := Low(ParamValueUI) to High(ParamValueUI) do
      begin
            case IndexText(TypeBox.Text, [VRCOSCType_Int, VRCOSCType_Bool, VRCOSCType_Float]) of
            0:
                  begin
                        ParamValueUI[i].Min := -255;
                        ParamValueUI[i].Max := 255;
                        ParamValueUI[i].Value := 0;
                        ParamValueUI[i].ValueType := TNumValueType.Integer;
                  end;
            1:
                  begin
                        ParamValueUI[i].Min := 0;
                        ParamValueUI[i].Max := 1;
                        ParamValueUI[i].Value := 0;
                        ParamValueUI[i].ValueType := TNumValueType.Integer;
                  end;
            2:
                  begin
                        ParamValueUI[i].Min := -2;
                        ParamValueUI[i].Max := 2;
                        ParamValueUI[i].Value := 0;
                        ParamValueUI[i].ValueType := TNumValueType.Float;
                  end;
            else
                        ParamValueUI[i].Min := 0;
                        ParamValueUI[i].Max := 1;
                        ParamValueUI[i].Value := 0;
                        ParamValueUI[i].ValueType := TNumValueType.Integer;
            end;
      end;

      FInputType := TypeBox.Text;
      Changed;
end;


procedure TExecutionCell.SendButtonClick(Sender: TObject);
var
      Buffer : TMemoryStream;
      LastBuffer : TIdBytes;
      InputValue : Single;
      CurrentValue : Single;
      LastValue : Variant;
      StartValue, EndValue : Single;
      i,j,n : Integer;
      UDPHost : String;
      UDPPort : Word;
      OSCAddr  : String;
      InputAddr : String;
      InputArr, Arr : TArray<String>;

procedure SendBuffer(const AStream:TMemoryStream);
begin
      SetLength(LastBuffer, AStream.Position);
      AStream.Seek(0,0); Buffer.Read(LastBuffer[0], Length(LastBuffer));
      FTIdUDPClient.SendBuffer(LastBuffer);
      AStream.Clear;
end;

begin
      if Assigned(FCellExecute) then FCellExecute(Self, FCellNumber);

      if Assigned(FTIdUDPClient) then
      begin
            if (Self.Running) and (FSendType = ossAnime) then
            begin
                  Self.FAnimationAbort := True;
                  exit;
            end;

            InputArr := FInputAddress.Split(['|']);

            for n := 0 to High(InputArr) do
            begin
                  InputAddr := Trim(InputArr[n]);

                  UDPHost := FTIdUDPClient.Host;
                  UDPPort := FTIdUDPClient.Port;
                  //OSCAddr := FInputAddress;
                  OSCAddr := InputAddr;
                  try
                        // Override (CellChain)
                        if InputAddr.StartsWith(LOCALLOOPBACK_CELLCHAIN) then
                        begin
                              InputValue :=  NaN;
                              if not IsNan((Sender as TControl).TagFloat) then
                              begin
                                    StartValue := Min(FInputValues[0],  FInputValues[1]); EndValue := Max(FInputValues[0],  FInputValues[1]);
                                    InputValue := StartValue + (EndValue - StartValue) * (Sender as TControl).TagFloat;
                              end;

                              VTKForm.RunExeCell( StrtoIntDef(EzMatch(LOCALLOOPBACK_CELLCHAIN + '%d', InputAddr) ,-1 ), InputValue );
                              continue;
                        end;

                        // Override (CellChain)
                        if InputAddr.StartsWith(LOCALLOOPBACK_KEYPRESS) then
                        begin
                              Arr := EzMatch(LOCALLOOPBACK_KEYPRESS+'*', InputAddr).Split(['/'], TStringSplitOptions.ExcludeEmpty);

                              case Length(Arr) of
                                    0 : exit;
                                    1 : FSendKeys.SendKeys(Arr[0]);
                                    2 : begin
                                          FSendKeys.AppActivate(PWideChar(Arr[0]));
                                          FSendKeys.SendKeys(Arr[1]);
                                    end;
                              else
                                    FSendKeys.AppActivate(PWideChar(Arr[0]));
                                    Delete(Arr, 0, 1);
                                    FSendKeys.SendKeys( String.Join('/', Arr) );
                              end;

                              continue;
                        end;

                        // Override (Loopback)
                        if InputAddr.StartsWith(LOCALLOOPBACK_ROOT) then
                        begin
                              FTIdUDPClient.Host := '127.0.0.1';
                              FTIdUDPClient.Port := 9001;
                              OSCAddr := (* FInputAddress *) InputAddr + '/' + InttoStr(GenerateAccessToken(GlobalUserIDHash, GlobalPassCodeHash, SALTDATE_MOMENT, True));
                        end;

                        FTIdUDPClient.Connect;
                        Buffer := TMemoryStream.Create;
                        FRunning := True;
                        try
                              CurrentValue := 0.0;
                              InputValue := 0.0;

                              // read current value
                              if Assigned(FReceivedParams) and ( FReceivedParams.IndexOfName(InputAddr) >= 0 ) then
                                    CurrentValue := StrtoFloatDef(FReceivedParams.Values[InputAddr], NaN);

                              Buffer.Clear;

                              case FSendType of
                                    ossFixed:
                                    begin
                                          InputValue := FInputValues[0];
                                          BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);
                                          SendBuffer(Buffer);
                                    end;

                                    ossInverse:
                                    begin
                                          // not op
                                          if IsNan(CurrentValue) then CurrentValue := 1.0;
                                          InputValue := IfThen( IsZero(CurrentValue), 1.0, 0.0);

                                          BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);
                                          SendBuffer(Buffer);
                                    end;

                                    ossPulse:
                                    begin
                                          for i := 0 to 2 do
                                          begin
                                                InputValue := FInputValues[i mod 2];
                                                BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);

                                                SendBuffer(Buffer);

                                                // 50ms x n
                                                for j := 1 to 1 do
                                                begin
                                                      Application.ProcessMessages;
                                                      Sleep(50);
                                                end;
                                          end;
                                    end;

                                    ossStep:
                                    begin
                                          if (FInputValues[2] >= 0) then
                                          begin
                                                StartValue := Min(FInputValues[0],  FInputValues[1]);
                                                EndValue := Max(FInputValues[0],  FInputValues[1]);
                                          end else
                                          begin
                                                EndValue := Min(FInputValues[0],  FInputValues[1]);
                                                StartValue := Max(FInputValues[0],  FInputValues[1]);
                                          end;

                                          if IsNan(CurrentValue) then CurrentValue := StartValue - FInputValues[2];
                                          InputValue := CurrentValue + FInputValues[2];

                                          if (FInputValues[2] >= 0) then
                                          begin
                                                if (InputValue > EndValue) then InputValue := StartValue
                                                //else if (InputValue < StartValue) then InputValue := EndValue;
                                          end else
                                          begin
                                                if (InputValue < EndValue) then InputValue := StartValue
                                                //else if (InputValue < StartValue) then InputValue := EndValue;
                                          end;

                                          BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);
                                          SendBuffer(Buffer);
                                    end;

                                    ossAnime:
                                    begin
                                          FAnimationAbort := False;

                                          if not IsZero(FInputValues[2]) then
                                          begin
                                                if (FInputValues[2] >= 0) then
                                                begin
                                                      StartValue := Min(FInputValues[0],  FInputValues[1]);
                                                      EndValue := Max(FInputValues[0],  FInputValues[1]);
                                                end else
                                                begin
                                                      EndValue := Min(FInputValues[0],  FInputValues[1]);
                                                      StartValue := Max(FInputValues[0],  FInputValues[1]);
                                                end;

                                                InputValue := StartValue;

                                                while True do
                                                begin
                                                      BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);
                                                      SendBuffer(Buffer);

                                                      if (FInputValues[2] >= 0) then
                                                      begin
                                                            if (EndValue <= InputValue) then break;
                                                      end else
                                                      begin
                                                            if (InputValue <= EndValue) then break;
                                                      end;

                                                      InputValue := InputValue + FInputValues[2];

                                                      // 50ms
                                                      for i := 0 to 10 do
                                                      begin
                                                            Sleep(5);
                                                            Application.ProcessMessages;
                                                      end;

                                                      if FAnimationAbort then break;

                                                end;

                                                FAnimationAbort := False;
                                          end;
                                    end;

                                    ossPass:
                                    begin
                                          if not IsNan((Sender as TControl).TagFloat) then
                                          begin
                                                StartValue := Min(FInputValues[0],  FInputValues[1]);
                                                EndValue := Max(FInputValues[0],  FInputValues[1]);

                                                InputValue := StartValue + (EndValue - StartValue) * (Sender as TControl).TagFloat;
                                                BuildOSCPacket(Buffer, OSCAddr, FInputType, InputValue, LastValue);
                                                SendBuffer(Buffer);
                                          end;
                                    end;

                              end;

                              //FInputValues[0] := InputValue;
                              if Assigned(FReceivedParams) and ( (not VarIsEmpty(LastValue)) and (not VarIsNull(LastValue)) ) then
                                    //FReceivedParams.Values[FInputAddress] := FloatToStr(InputValue);
                                    FReceivedParams.Values[InputAddr] := String(LastValue);

                        finally
                              Buffer.Free;
                              FTIdUDPClient.Disconnect;
                              FRunning := False;
                        end;
                  finally
                        FTIdUDPClient.Host := UDPHost;
                        FTIdUDPClient.Port := UDPPort;
                  end;
            end;
      end;
end;

procedure TExecutionCell.SendButtonGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
      ShowMessage('');
end;

procedure TExecutionCell.SendButtonMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
      Timer1.Enabled := True;
end;

procedure TExecutionCell.SendButtonMouseLeave(Sender: TObject);
begin
      Timer1.Enabled := False;
end;

procedure TExecutionCell.SendButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
      Timer1.Enabled := False;
end;

end.



