//----------------------------------------------------------------------------//
//  OSCさんといっしょ for VRChat といっしょ                       Version 1.00
//    Author : NAGATO Mirai <hayate@zuh.jp> / @MiraiNagato at Twitter
//    Website : https://mtshop.booth.pm/ / https://mirainagato.geo.jp/
//----------------------------------------------------------------------------//
//  <!> This application is only available in Japanese.
//----------------------------------------------------------------------------//

　　『OSCさんといっしょ for VRChat』受信トリガーと連携して使用できる
　　小物アプリケーションの寄せ集めです。
　　
　　コマンドプロンプトや他アプリから使用することもできます。

　　◎MediaPlayer.exe "[必須]ファイルパス" "音量0～100"
　　
　　　　☆ 動作要件: Windows10 以降 / .NET Framework 4.0 が必要
　　
	音声ファイルを再生します。全て再生し終わると自動終了します。
	<!> 途中で止めるときはタスクバーから終わらせてください。
　　
　　◎PressKey.exe "[必須]キー指定" "アクティヴにするウインドウ名"

	☆ 動作要件: Windows7 以降

	キーボード入力を1回だけ行います。キー指定はVBA SendKeysに準拠します。
	https://docs.microsoft.com/ja-jp/office/vba/language/reference/user-interface-help/sendkeys-statement
　　　
	PressKey.exe "`JP" "メモ帳" とすると、開いているメモ帳が手前に
	来て「JP」が入力されます。"`^V" とすれば、貼り付けもできます。
	基本的な書き方は上記URLを参照してください。
	
	◇仕様
	SendInput API による、スキャンコード送信での実装です。
	仮想キーコード送信に切り替えるときは後述の「`」指令を使用します。
	Ctrl,Alt,Shift,Win各キーは左右あるうちの左を押すものとして処理します。

	◇独自の拡張指令
	> (大なり記号)       直後の文字を即時入力しない
	                     ^〈Ctrl〉%〈Alt〉+〈Shift〉@〈左Win〉は暗黙に適用
	` (バッククォート)   次のバッククォートまで仮想キーコード指定で送信する
	
	◇独自の拡張構文
	{LESS}               小なり記号 < を入力
	{GREATER}            大なり記号 > を入力
	{BQT}                バッククォート ` を入力
	{VKEY i}             仮想キーコード i を入力
	{BEEP f n}           nミリ秒・f Hzの高さでビープ音
	{DELAY n}            nミリ秒のウエイト
	{APPACTIVATE s}      s を含むウインドウを表示、最前面に
	                     コマンドラインオプションと併せて使用可
	{APP s}              {APPACTIVATE s} の別名（エイリアス）
	
	◇よくあるキー指定（テンプレ）
	`^C                            コピー
	`^V                            ペースト
	`{Enter}                       エンターキー
	`{Win}                         スタートメニュー
	`@{Tab}                        タスクビュー
	{APP VRChat}{F12}              VRChat/スクリーンショット
	{APP VRChat}^{F12}             VRChat/4Kスクリーンショット
	{APP VRChat}{ESC}              VRChat/LaunchPad
	{APP VRChat}r                  VRChat/Expressionメニュー
	{APP VRChat}v                  VRChat/マイクミュート
	{APP VRChat}+{ESC}             VRChat/セーフモード
	{APP Discord}^+m{APP VRChat}   PC版Discord/マイクミュート
	{APP Discord}^+d{APP VRChat}   PC版Discord/スピーカーミュート
	{APP Skype}^m{APP VRChat}      PC版Skype/マイクミュート
	
	〇 アプリによっては「`」を付けて、仮想キーコード指定にする必要があります。
	〇 修飾キーとしての右Shift、右Ctrl、右Alt、右Winキーは現在利用できません。
	〇 機能拡張に伴って、書式を予告なく改訂することがあります。
	
	● キーボード入力を模倣する特性上、一部のアンチウイルスソフトウェアによって
	   コンピューターウイルスと誤認識されることがあります。スキャン例外に登録す
	   るなどして回避してください。損害を与えうるコードは当然ながら、一切含みま
	   せん。
	
　　◎Say.exe "[必須]読み上げ内容" "ソフト種別(VOICEROID2 or VOICEVOX or SAPI)" "話者"
	
	☆ 動作要件: Windows10 以降 / .NET Framework 4.8 が必要
	             SAPI以外の読み上げには VOICEROID2, VOICEVOX が必要
	
	指定内容を読み上げます。読み上げ内容を "" (空白) とすると、
	時刻を喋ります。
	
	デフォルトのソフトウェア/話者はWindows付属のSAPI-日本語です。
	
	〇ソフト種別 = "VOICEROID2"
	　VOICEROID2で読み上げます。VOICEROID2が起動している必要があります。
	　話者に "結月ゆかり" などと指定すると、ゆかりさんが喋ります。
	　読み上げ時にウインドウフォーカスが移るため、注意してください。
	
	〇ソフト種別 = "VOICEVOX"
	　VOICEVOXで読み上げます。VOICEVOXが起動している必要があります。
	　話者に "九州そら:ささやき" などと指定すると、そらさんが囁いてきます。
	　
	〇ソフト種別 = "SAPI"
	　SAPIで読み上げます。何も考えず使用できます。話者指定はできません。

	<!> 途中で止めるときはタスクバーから終わらせてください。

//----------------------------------------------------------------------------//
// NAGATO Mirai <hayate.zuh.jp>                                               //
//----------------------------------------------------------------------------//
